//
//last updated@16 Jul,2024
//Z.Chen
//


#include <TFile.h>
#include <TChain.h>
#include <TROOT.h>
#include <TApplication.h>
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string>

#include "./inc/users/zchen/userA.hh"



int main(int argc, char**argv){

	TString file1 = "./input/preLISA_1pF1M_241Am_Eris.root";
	//TString file2 = "test3.root";
	TString file3 = "settings_Febex_export.set";
	//TString func = "anaFebex4_traces";
	//Int_t func_convert = -1;
	Int_t ffebch = -1;
	ULong64_t fjentry = 0;

	if(argc<2){
		std::cout<<"[Error INFO] Invalid command format. ***"<<std::endl;
		std::cout<<"argc = "<<argc<<std::endl;
		std::cout<<"argv = "<<argv[0]<<std::endl;
		std::cout<<"Usage: ./users -FLAG CONTENT -FLAG CONTENT ..."<<std::endl;
		return -1;
	}

	for(int i = 0;i<argc;i++){
		if(strcmp(argv[i],"-h")==0){
			std::cout<<"Usage: ./users -FLAG CONTENT -FLAG CONTENT ..."<<std::endl;
			return -1;
		}
		//input
		if(strcmp(argv[i],"-i")==0){
			file1 = argv[i+1];
		}
		//setting
		if(strcmp(argv[i],"-s")==0){
			file3 = argv[i+1];
		}
		//febex channel
		if(strcmp(argv[i],"-ch")==0){
			ffebch = atoi(argv[i+1]);
		}
		//febex entry
		if(strcmp(argv[i],"-en")==0){
			fjentry = atoll(argv[i+1]);
		}
	}

	std::cout<<"...oooOO0OOooo......oooOO0OOooo...... anaTraces ......oooOO0OOooo......oooOO0OOooo"<<std::endl;
	std::cout<<"[Print INFO] input file = "<<file1<<std::endl;
	std::cout<<"[Print INFO] setting file = "<<file3<<std::endl;
	std::cout<<"[Print INFO] febex channel = "<<ffebch<<std::endl;
	std::cout<<"[Print INFO] febex entry = "<<fjentry<<std::endl;
	std::cout<<"...oooOO0OOooo......oooOO0OOooo...... anaTraces ......oooOO0OOooo......oooOO0OOooo"<<std::endl;

	//***  set input/output here ******//
	TString iptFileName = file1;

	TFile *iptfile;
	TTree *ipttree;
	userA_class *ipt;


	//...oooOO0OOooo...//
	iptfile = new TFile(iptFileName);
	ipttree = (TTree*)iptfile->Get("h101");

	ipt = new userA_class(ipttree);
	std::cout<<"[Print INFO] declared an userA_class."<<std::endl;

	ipt->Init(ipttree);
	std::cout<<"[Print INFO] Init done"<<std::endl;

	ipt->loadSettings(file3);//from class lisa_settings
	std::cout<<"[Print INFO] load settings done"<<std::endl;

	ipt->setJentry(fjentry);
	ipt->setFebexCh(ffebch); 

	// for display //
	int appc=2;
	char* appv[2];
	appv[0]= (char*)"./users";
	appv[1]= (char*)"-x";
	TApplication theApp("App",&appc, appv);//for display

    // put your applications here //
    ipt->anaPileUp(ipt);

	theApp.Run();

	std::cout<<"[Print INFO] The end."<<std::endl;
	return 0;
}

