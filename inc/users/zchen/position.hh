
#ifndef __POSITION_HH__
#define __POSITION_HH__

//*** defination ***//
//*** ---***--- *** //
//*** |0|***|1| *** //
//*** ---*O*--- *** //
//*** |3|***|2| *** //
//*** ---***--- *** //
//**X--->********** //
//******^********** //
//**Y**/ \********* //
//******|********** //
//********* x1  y1       x2   y2 */
//ch1,9: b_l(-4.5,0.5),t_r(-0.5,4.5)
//center@(-2.5,2.5)
//
//ch2,10: b_l( 0.5,0.5),t_r( 4.5,4.5)
//center@(2.5,2.5)
//
//ch5,13: b_l( 0.5,-4.5),t_r(4.5,-0.5)
//center@(2.5,-2.5)
//
//ch6,14: b_l(-4.5,-4.5),t_r(-0.5,-0.5)
//center@(-2.5,-2.5)
//***************** //

#include <iostream>
#include "TRandom.h"

const int N2x2LISACh=4; //total channel number in single layer
const int positionZ_eris = -5;//mm
const int positionZ_sparrow = 5;//mm
const int positionZ_degrader = 0;//mm

float coor_LISA_center_x[N2x2LISACh];
float coor_LISA_center_y[N2x2LISACh];

//float coor_LISA_fakeCenter_x[N2x2LISACh];//simulated with random values
//float coor_LISA_fakeCenter_y[N2x2LISACh];

float coor_LISA_x1[N2x2LISACh];//bottom left
float coor_LISA_y1[N2x2LISACh];//
float coor_LISA_x2[N2x2LISACh];//top right
float coor_LISA_y2[N2x2LISACh];


void loadPositionInfo();
void getMapping(UInt_t fch, char* fname);
void getPosition(UInt_t fch, UInt_t *fpos);
void getFakeCenter(double *x1, double *y1, UInt_t fch);

void loadPositionInfo(){

	coor_LISA_x1[0] = -4.5;//bottom left
	coor_LISA_y1[0] =  0.5;//
	coor_LISA_x2[0] = -0.5;//top right
	coor_LISA_y2[0] =  4.5;
	coor_LISA_center_x[0] = -2.5;
	coor_LISA_center_y[0] =  2.5;

	coor_LISA_x1[1] =  0.5;
	coor_LISA_y1[1] =  0.5;
	coor_LISA_x2[1] =  4.5;
	coor_LISA_y2[1] =  4.5;
	coor_LISA_center_x[1] =  2.5;
	coor_LISA_center_y[1] =  2.5;

	coor_LISA_x1[2] =  0.5;
	coor_LISA_y1[2] = -4.5;
	coor_LISA_x2[2] =  4.5;
	coor_LISA_y2[2] = -0.5;
	coor_LISA_center_x[2] =  2.5;
	coor_LISA_center_y[2] = -2.5;

	coor_LISA_x1[3] = -4.5;
	coor_LISA_y1[3] = -4.5;
	coor_LISA_x2[3] = -0.5;
	coor_LISA_y2[3] = -0.5;
	coor_LISA_center_x[3] = -2.5;
	coor_LISA_center_y[3] = -2.5;

}

void getMapping(UInt_t fch, char* fname){
	switch(fch){
		case 2:
			strcpy(fname, "Caracas");
			break;
		case 1:
			strcpy(fname, "Quito");
			break;
		case 6:
			strcpy(fname, "Amsterdam");
			break;
		case 5:
			strcpy(fname, "Reykjavik");
			break;
		case 9:
			strcpy(fname, "Novi Sad");
			break;
		case 10:
			strcpy(fname, "Havana");
			break;
		case 13:
			strcpy(fname, "Dublin");
			break;
		case 14:
			strcpy(fname, "Sucre");
			break;
		default:
			std::cout<<"Error!!! Channel num is not valid. Please check."<<std::endl;
			return;
	}
}

void getPosition(UInt_t fch, UInt_t *fpos){

	switch(fch){
		case 1:
            fpos[0] = 0;
            break;
		case 2:
            fpos[0] = 1;
            break;
		case 5:
            fpos[0] = 3;
            break;
		case 6:
            fpos[0] = 2;
            break;
		case 9:
            fpos[0] = 0;
            break;
		case 10:
            fpos[0] = 1;
            break;
		case 13:
            fpos[0] = 3;
            break;
		case 14:
            fpos[0] = 2;
            break;
            default:
            std::cout<<"Error!!! Channel num is not valid. Please check."<<std::endl;
            return;
    }   
}

void getFakeCenter(double *x1, double *y1, UInt_t fch){
	x1[0] = gRandom->Uniform(coor_LISA_x1[fch],coor_LISA_x2[fch]);
	y1[0] = gRandom->Uniform(coor_LISA_y1[fch],coor_LISA_x2[fch]);
}

#endif
