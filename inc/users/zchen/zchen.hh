


#ifndef _ZCHEN_hh_
#define _ZCHEN_hh_


#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TH2.h>
#include <TLine.h>
#include <TCanvas.h>
#include <TStyle.h>
#include <TF1.h>
#include "LISA.hh"
//============================================================================================================================================//
// change display mode here //
const bool IF_DEBUG = 0;
const int SPEED = 100;  //the larger the speed value you set, the lower the refresh frequency you'll get. e.g. 100, refresh every 100 events.
const bool USE_CENTER = true;
const bool USE_FAKE = false;
const bool SINGLE_MODE = false;
const bool HOLDON_MODE = true; 
//============================================================================================================================================//
//
//

class zchen {


        //*** display ***//
        TLine *p_eris1;
        TLine *p_eris2;
        TLine *p_sparrow1;
        TLine *p_sparrow2;
        TLine *pzline;

        TH2D *offline_hx;
        TH2D *offline_hy;
        TH2D *offline_hpz;

        TCanvas *offline_c1;
        TCanvas *offline_c2;
        TCanvas *c_hit_pattern;

        TH2F *h_eris;
        TH2F *h_sparrow;

        LISA_FRAME hit_eris;
        LISA_FRAME hit_sparrow;

		zchen();
        virtual ~zchen();
        virtual void hitPattern();
        virtual void offlineLoop(UInt_t fhit, Int_t* channel);
        virtual void filltrack(double x1,double y1,double z1,double x2, double y2,double z2, double pz,TH2D *offline_hx,TH2D *offline_hy,TH2D *offline_hpz);

        virtual void setEnt();
        virtual void loadPosition();
        virtual void setDisplay();

        virtual void setFileName(TString sIptFileName = "ch0-7_random.root"){f_iptFile = sIptFileName;};
        virtual TString getFileName(){ return f_iptFile;};

    private:
        //TString f_iptFile = "../run_0181_0001_ana.root";
        TString f_iptFile = "anaroot/run_0181_0004_ana.root";

};


#endif
