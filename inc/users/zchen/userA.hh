
//last updated@16 Jul,2024
//Z.Chen
//

#ifndef _USERA_HH_
#define _USERA_HH_

#include "../../lisa_febex.hh"

class userA_class: public lisa_febex{
    public :
        userA_class(TTree *tree);
        userA_class();
        virtual ~userA_class();

        virtual void     anaPileUp(lisa_febex *ipt);//

        ClassDef(userA_class,1)
};





#endif
