#ifndef __LISA_HH__
#define __LISA_HH__

#include <TBox.h>
#include <iostream>
#include <vector>

struct LISA_FRAME {

	int ID;
	std::vector<float>fx1;
	std::vector<float>fy1;
	std::vector<float>fx2;
	std::vector<float>fy2;
	std::vector<TBox*> fbox;

	LISA_FRAME():ID(0) {}
	void clear() {
		ID = 0;
		fx1.clear();
		fy1.clear();
		fx2.clear();
		fy2.clear();
		fbox.clear();
	}
};

#endif
