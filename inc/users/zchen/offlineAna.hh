//
//Error4xxx
//

#ifndef offlineAna_hh
#define offlineAna_hh

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TH2.h>
#include <TLine.h>
#include <TCanvas.h>
#include <TStyle.h>
#include <TF1.h>
//#include "position.hh"
#include "LISA.hh"
//============================================================================================================================================//
// change display mode here //
const bool IF_DEBUG = 0;
const int SPEED = 100;  //the larger the speed value you set, the lower the refresh frequency you'll get. e.g. 100, refresh every 100 events.
const bool USE_CENTER = true;
const bool USE_FAKE = false;
const bool SINGLE_MODE = false;
const bool HOLDON_MODE = true; 
//============================================================================================================================================//

class offlineAna {
    public:
        TFile *fiptFile;
        TChain *fiptChain;
        TTree *iptTree;

        //*** root file structure ***//
        // Declaration of leaf types
        Int_t          TRIGGER_c4;
        Int_t          EVENTNO_c4;
        UInt_t          hit;
        Int_t           febCh[16];   //[hit]
        Int_t           entry;
        Int_t           entry_empty[16];   //[hit]
        Long64_t        ts[16];   //[hit]
        Long64_t        tsS[16];   //[hit]
        Double_t        Eraw[16];   //[hit]
        Double_t        Ecal[16];   //[hit]
        Double_t        T_cfd[16];   //[hit]
        Int_t           if_overflow[16];   //[hit]
        UInt_t          traceLength[16];   //[hit]
        Double_t        tracesX[16][3000];   //[hit]
        Double_t        tracesY[16][3000];   //[hit]

        // List of branches
        TBranch        *b_TRIGGER_c4;   //!
        TBranch        *b_EVEVTNO_c4;   //!
        TBranch        *b_hit;   //!
        TBranch        *b_febCh;   //!
        TBranch        *b_entry;   //!
        TBranch        *b_entry_empty;   //!
        TBranch        *b_ts;   //!
        TBranch        *b_tsS;   //!
        TBranch        *b_Eraw;   //!
        TBranch        *b_Ecal;   //!
        TBranch        *b_T_cfd;   //!
        TBranch        *b_if_overflow;   //!
        TBranch        *b_traceLength;   //!
        TBranch        *b_tracesX;   //!
        TBranch        *b_tracesY;   //!

        //*** display ***//
        TLine *p_eris1;
        TLine *p_eris2;
        TLine *p_sparrow1;
        TLine *p_sparrow2;
        TLine *pzline;

        TH2D *offline_hx;
        TH2D *offline_hy;
        TH2D *offline_hpz;

        TCanvas *offline_c1;
        TCanvas *offline_c2;
        TCanvas *c_hit_pattern;

        TH2F *h_eris;
        TH2F *h_sparrow;

        LISA_FRAME hit_eris;
        LISA_FRAME hit_sparrow;

        offlineAna();
        //offlineAna(TString sIptFileName = "himac_anaroot_run6_all_thre200_energycali.root");
        //offlineAna(TString sIptFileName = "himac_anaroot_run6_all_thre200.root");
        virtual ~offlineAna();
        virtual void filltrack(double x1,double y1,double z1,double x2, double y2,double z2, double pz,TH2D *offline_hx,TH2D *offline_hy,TH2D *offline_hpz);
        virtual void InitRoot(TTree *iptTree);
        virtual void hitPattern();
        virtual void hitPattern_2();
        virtual void getPID(int fch1, int fch2);
        virtual void energyCalib();
        virtual void singleFit(TH1F *fhh, double *famp, double *fmean, double *fsigma);
        virtual void setEnt();
        virtual void loadPosition();
        virtual void setDisplay();
        virtual void convert(float fk1, float fb1, float fk2, float fb2);
        virtual void offlineLoop(UInt_t fhit,Long64_t jentry, Long64_t nentries);
        virtual void setFileName(TString sIptFileName = "ch0-7_random.root"){f_iptFile = sIptFileName;};
        virtual TString getFileName(){ return f_iptFile;};

    private:
        //TString f_iptFile = "../run_0181_0001_ana.root";
        TString f_iptFile = "anaroot/run_0181_0004_ana.root";
};


#endif
