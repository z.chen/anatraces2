#include "TCanvas.h"
#include "TGraph.h"
#include "TF1.h"
#include "TMath.h"

Double_t start_point = 3800;
Double_t length = 8500;

Double_t fitFunction(Double_t *x, Double_t *par) {
    // Skip fitting in the range [2400, 2600]
    /*
    if (x[0] >= 2400 && x[0] <= start_point) {
        return 0; // Return a very large value to effectively ignore this range
    }
    if (x[0] >= start_point + length) {
        return 0; // Return a very large value to effectively ignore this range
    }
    */
    if (x[0] < par[2]) {
        // Constant part
        return par[0];
    } else {
        // Exponential decay part
        return par[1] * TMath::Exp(par[3] * (x[0] - par[2]));
    }
}

void simulate_curve_with_fit2() {
    // Number of points
    const int n = 2000;
    //double x[n], y[n];
    vector <double> sim_x;
    vector <double> sim_y;
    sim_x.clear();
    sim_y.clear();
    
    // Define the constants for the functions
    double constant_value = 0.0;
    double exp_constant = 300.0;
    double exp_decay_rate = -1/2050.0;
    double sim_uncertainty = 0.2;

    // Generate the data points
    int cnt=0;
    for (int i = 0; i < n; i++) {
        if (i < 55) {
            // Left part - constant
            sim_x.push_back(2000 + i*10);//sim_x[i] = 2000 + i*10;
            sim_y.push_back(gRandom->Gaus(constant_value, sim_uncertainty));//sim_y[i] = constant_value;
        } else {
            // Right part - exponential decay
            sim_x.push_back(2010 + i*10);//sim_x[i] = 2020 + i*10;
            double ytem = exp_constant * TMath::Exp(exp_decay_rate * (sim_x.at(i) - 2010));
            sim_y.push_back(gRandom->Gaus(ytem, sim_uncertainty));//sim_y[i] = gRandom->Gaus(ytem, 0.01);
        }
        cnt++;
    }

    // Create a TGraph
    TGraph *graph = new TGraph(sim_x.size(), sim_x.data(), sim_y.data());
    graph->SetMarkerStyle(8);
    graph->SetMarkerSize(0.5);
    graph->SetMarkerColor(kBlue);

    /*
    TGraph *graph = new TGraph();
    for (int i = 0; i < n; ++i) {
        if ((sim_x[i] >= 2000 && sim_x[i] <= 2400) || (sim_x[i] >= 4000 && sim_x[i] <= 4500)) {
            graph->SetPoint(graph->GetN(), sim_x[i], sim_y[i]);
        }
    }
*/
    // Create a canvas to draw the graph
    TCanvas *c1 = new TCanvas("c1", "Simulated Curve with Fit", 800, 600);
    graph->Draw("A*");

    // Set axis labels
    graph->GetXaxis()->SetTitle("X");
    graph->GetYaxis()->SetTitle("Y");

    // Define the fit function
    TF1 *fitFunc = new TF1("fitFunc", fitFunction, 2000, 7000, 4);
    fitFunc->SetParameters(0, 100, 2550, -1/2000.);
    //fitFunc->SetParameters(constant_value, exp_constant, 2550, exp_decay_rate);
    fitFunc->SetParLimits(0, 0, 1);
    fitFunc->SetParLimits(1, 200, 400);
    //fitFunc->FixParameter(2, 2550);
    //fitFunc->SetParLimits(3, 1/3000., 1/1000.);
    fitFunc->SetParNames("Constant Value", "Exp Constant", "Transition X", "Exp Decay Rate");

    // Fit the function to the graph
    graph->Fit(fitFunc, "qR");
    graph->GetYaxis()->SetRangeUser(-50, 350);
    graph->GetXaxis()->SetRangeUser(2000, 7500);

    // Draw the fit function
    fitFunc->Draw("same");
    fitFunc->SetLineColor(kRed);
    Double_t pars[4];

    fitFunc->GetParameters(pars);
    cout << "Constant Value = " << pars[0] << endl;
    cout << "Exp Constant = " << pars[1] << endl;
    cout << "Transition X = " << pars[2] << endl;
    cout << "Exp tau = " << -1.0/pars[3] << endl;

    fitFunc->SetParameters(pars);
    fitFunc->SetParLimits(0, 0, 1);
    fitFunc->SetParLimits(1, 200, 400);
    fitFunc->SetParLimits(2, pars[2]-pars[2]*0.3, pars[2]+pars[2]*0.3);
    fitFunc->SetParLimits(3, pars[3]-pars[3]*0.3, pars[3]+pars[3]*0.3);
    graph->Fit(fitFunc, "Rq");
    fitFunc->GetParameters(pars);
    cout << "Constant Value = " << pars[0] << endl;
    cout << "Exp Constant = " << pars[1] << endl;
    cout << "Transition X = " << pars[2] << endl;
    cout << "Exp tau = " << -1.0/pars[3] << endl;

    fitFunc->SetParameters(pars);
    fitFunc->SetParLimits(0, 0, 1);
    fitFunc->SetParLimits(1, 200, 400);
    //fitFunc->FixParameter(2, 2550);
    fitFunc->SetParLimits(3, pars[3]-pars[3]*0.3, pars[3]+pars[3]*0.3);
    graph->Fit(fitFunc, "qR");
    fitFunc->GetParameters(pars);
    cout << "Constant Value = " << pars[0] << endl;
    cout << "Exp Constant = " << pars[1] << endl;
    cout << "Transition X = " << pars[2] << endl;
    cout << "Exp tau = " << -1.0/pars[3] << endl;
    // Save the canvas as a png file
    //c1->SaveAs("simulated_curve_with_fit.png");
}

