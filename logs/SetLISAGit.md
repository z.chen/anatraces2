# \######## manual \#######

* download from "zchen" branch

1. git clone https://git.gsi.de/lisa/daq_analysis.git
2. cd daq_analysis
3. git fetch
(git branch -a) //check all the existing branches
4. git checkout zchen


* remove files from local
1. git checkout zchen
2. git rm -r *
3. git commit -m "Removed all files from zchen branch"
4. git push origin zchen



