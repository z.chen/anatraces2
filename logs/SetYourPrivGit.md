# \######## manual \#######

** always setup your ssh key before start to do anything

* if this is an empty folder:
1. $ git init #always init first before download anything
2. $ git pull git@git.gsi.de:z.chen/anatraces2.git #before this step, make sure you have added you ssh-key to this git account
3. $ git config pull.rebase true ##if the folder is not empty, you need to rebase the folder

* if this is your first time, before submit your updated files
1. $ git add .
2. $ git commit -m YOUR_COMMENTS
3. $ git remote add origin git@git.gsi.de:z.chen/anatraces2.git
4. $ git branch -M main
5. $ git push -uf origin main

* if it's not the first time to upload files from this folder'
1. $ git add .
2. $ git commit -m YOUR_COMMENTS
3. $ git push

* to sync from github
1. git pull origin
