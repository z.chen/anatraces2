# \######## UPDATE logs \#######

### 17 Jul, 2024
### Z.Chen

* change function name:
1. getSingleTrace   -> loadSingleTrace
2. getCorrectTrace  -> calcCorrectTrace
3. getMWDTrace      -> calcMWDTrace
4. getCFDTrace      -> calcCFDTrace

* change type:
1. in lisa_febex.hh: float -> Double_t
2. in lisa_febex.cc: float -> Double_t
3. in lisa_Evt.hh: float -> Double_t
4. in lisa_OutputManager.cc: float -> Double_t
5. in lisa_vis.cc: float -> Double_t

* add new functions in lisa_febex.hh/.cc
1. findKink() // for pileup analysis
2. setKink()
3. getKink()

* add new switch
1. Enable.DMi
2. Enable.MWDi
3. Read.from.Hit

* add new TBranch into output root file
1. MWDtracesX and MWDtracesY
2. hit_s //multihit in a single febex channel; for pileup analysis
3. hit  -> hit_m //multihit in a febex card(16 channels); ATTENTION: hit_s is not included in the calculation of hit_m; MAX of hit_m is 16.

### 25 Jul, 2024
### Z.Chen

* change the way to set resutls in lisa_febex.cc:

1. febex_data_temp.fCorrTrace_x = getCorrectTraceI();//return a vector
2. tracesX[hit_m][i] = fopt.fCorrTrace_x[i];
3. tracesY[hit_m][i] = fopt.fCorrTrace_y[i];
4. MWDtracesX[hit_m][i] = fopt.fMWDTrace_x[i];
5. MWDtracesY[hit_m][i] = fopt.fMWDTrace_y[i];

* add switch in setting file 

1. Enable.calc.energy.via.range: 0 //calculate energy via assigned range
2. Enable.calc.energy.via.kink:  1 //calculate energy via kink
3. Decaytime.ch.0:              2050 #index here is febch, start from 0
4. Undershoot.threshold:        -100 # for undershoot detection

* "Undershoot"
1. ![Square](https://docs.google.com/drawings/d/e/2PACX-1vTco7x2BqXygG1th-l5QI839qBCxZEQXD2CTg8Tojpd-gg4OveCYwsJegRnedSUFmaL4mA2uP1eNQYV/pub?w=480&h=360)


* change type:
1. in lisa_Setting.hh: Double_t fDecaytime  -> Double_t fDecaytime[16]

### 05 Aug, 2024
### Z.Chen

* new feature in the code: multi-threads calculation for MWD
* add switch in the setting file

1. Set.MWD.calculation.threads:          8 #num of threads you plan to use for MWD calculation
