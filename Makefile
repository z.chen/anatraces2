# Compiler and linker
CXX = g++
## gcc4.8 -std=c++11
## gcc8.4 -std=c++17
CXXFLAGS = -O2 -Wall -Wextra -std=c++11 $(shell root-config --cflags) -Iinc -fopenmp -pthread
LDFLAGS = $(shell root-config --libs) -lSpectrum -ltbb -fopenmp

# Project files
SRCS = src/lisa_febex.cc src/lisa_OutputManager.cc src/lisa_settings.cc src/lisa_ucesb.cc src/lisa_vis.cc src/lisa_scope.cc src/lisa_Env.cc src/users/zchen/userA.cc

HDRS = inc/lisa_febex.hh inc/lisa_OutputManager.hh inc/lisa_settings.hh inc/lisa_ucesb.hh inc/lisa_vis.hh inc/lisa_scope.hh inc/lisa_Env.hh inc/users/zchen/userA.hh

# Dictionary files
BUILD = build
DICT_SRCS = $(BUILD)/dict.cc
DICT_HDRS = inc/dict.hh inc/dictLinkDef.h
DICT_O = $(BUILD)/dict.o
DICT_PCM = $(BUILD)/dict_rdict.pcm

# Object files
OBJS = $(SRCS:%.cc=build/%.o) $(DICT_O)

# Executables
EXEC1 = anaTraces2
EXEC2 = users

# Default target
all: create_build_dir $(EXEC1) $(EXEC2) setup_rdict

# Rule to build executables
$(EXEC1): $(OBJS) build/anaTraces2.o
	$(CXX) -o $@ $^ $(LDFLAGS)

$(EXEC2): $(OBJS) build/users.o
	$(CXX) -o $@ $^ $(LDFLAGS)

# Rule to build object files for main executables
build/anaTraces2.o: anaTraces2.cc $(HDRS)
	$(CXX) $(CXXFLAGS) -c $< -o $@

build/users.o: users.cc $(HDRS)
	$(CXX) $(CXXFLAGS) -c $< -o $@

# Rule to build object files for source files
build/%.o: %.cc $(HDRS)
	$(CXX) $(CXXFLAGS) -c $< -o $@

build/%.o: src/%.cc inc/%.hh
	$(CXX) $(CXXFLAGS) -c $< -o $@

# Rule to generate dictionary files
build/dict.cc: $(DICT_HDRS)
	rootcling -f $@ -c inc/dict.hh inc/dictLinkDef.h

# Rule to build dictionary object file
build/dict.o: build/dict.cc
	$(CXX) $(CXXFLAGS) -c $< -o $@

# Ensure build directory exists
create_build_dir:
	@if [ ! -d "$(BUILD)" ]; then \
		echo "[makefile INFO]Folder $(BUILD) doesn't exist, create a new one."; \
		mkdir -p build ;\
			else \
		echo "[makefile INFO]Folder $(BUILD) already exist. continue."; \
		fi

setup_rdict:
	@if [ ! -e "$(DICT_PCM)" ]; then \
		echo "[makefile INFO]$(DICT_PCM) doesn't exist, continue."; \
			else \
			ln -sf $(DICT_PCM) ;\
		echo "[makefile INFO]build link to $(DICT_PCM)"; \
		fi

# Phony targets
.PHONY: all clean create_build_dir

# Clean rule
clean:
	rm -f build/*.o $(EXEC1) $(EXEC2) build/src/*.o build/dict.cc build/dict_rdict.pcm

