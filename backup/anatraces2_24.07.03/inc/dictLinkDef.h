
#ifdef __CINT__
#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class lisa_febex+;
#pragma link C++ class lisa_optManager+;
#pragma link C++ class lisa_settings+;
#pragma link C++ class lisa_ucesb+;
#pragma link C++ class lisa_vis+;
#pragma link C++ class lisa_scope+;

#endif

