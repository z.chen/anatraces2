
#include <vector>
#include <TFile.h>
#include <TTree.h>
#include <TBranch.h>

static constexpr Int_t kMaxcbmout_0_Event_EventHeader = 1;
static constexpr Int_t kMaxcbmout_0_FatimaTwinpeaksCalDataFolder_FatimaTwinpeaksCalData = 24;
static constexpr Int_t kMaxcbmout_0_FatimaTimeMachineDataFolder_FatimaTimeMachineData = 8;
static constexpr Int_t kMaxcbmout_0_FatimaVmeCalDataFolder_FatimaVmeCalData = 1;
static constexpr Int_t kMaxcbmout_0_FatimaVmeTimeMachineDataFolder_FatimaVmeTimeMachineData = 8;
static constexpr Int_t kMaxcbmout_0_GermaniumCalData_GermaniumCalData = 1;
static constexpr Int_t kMaxcbmout_0_TimeMachineData_GermaniumTimeMachineData = 1;

//EventHeader     *EventHeader_;
//EventHeader     *EventHeader;
ULong_t    event_no;
Int_t      EventHeader_fTrigger;
ULong_t    EventHeader_fTimeStamp;
Bool_t     EventHeader_fSpillFlag;

// fatima twinpeaks //
Int_t      FatimaTwinpeaksCalData_;
UInt_t     FatimaTwinpeaksCalData_fUniqueID[kMaxcbmout_0_FatimaTwinpeaksCalDataFolder_FatimaTwinpeaksCalData];
UInt_t     FatimaTwinpeaksCalData_fBits[kMaxcbmout_0_FatimaTwinpeaksCalDataFolder_FatimaTwinpeaksCalData];
UShort_t   FatimaTwinpeaksCalData_fboard_id[kMaxcbmout_0_FatimaTwinpeaksCalDataFolder_FatimaTwinpeaksCalData];
UShort_t   FatimaTwinpeaksCalData_fch_ID[kMaxcbmout_0_FatimaTwinpeaksCalDataFolder_FatimaTwinpeaksCalData];
UShort_t   FatimaTwinpeaksCalData_fdetector_id[kMaxcbmout_0_FatimaTwinpeaksCalDataFolder_FatimaTwinpeaksCalData];
Double_t   FatimaTwinpeaksCalData_fslow_lead_time[kMaxcbmout_0_FatimaTwinpeaksCalDataFolder_FatimaTwinpeaksCalData];
Double_t   FatimaTwinpeaksCalData_fslow_trail_time[kMaxcbmout_0_FatimaTwinpeaksCalDataFolder_FatimaTwinpeaksCalData];
Double_t   FatimaTwinpeaksCalData_ffast_lead_time[kMaxcbmout_0_FatimaTwinpeaksCalDataFolder_FatimaTwinpeaksCalData];
Double_t   FatimaTwinpeaksCalData_ffast_trail_time[kMaxcbmout_0_FatimaTwinpeaksCalDataFolder_FatimaTwinpeaksCalData];
Double_t   FatimaTwinpeaksCalData_ffast_ToT[kMaxcbmout_0_FatimaTwinpeaksCalDataFolder_FatimaTwinpeaksCalData];
Double_t   FatimaTwinpeaksCalData_fslow_ToT[kMaxcbmout_0_FatimaTwinpeaksCalDataFolder_FatimaTwinpeaksCalData];
Double_t   FatimaTwinpeaksCalData_fenergy[kMaxcbmout_0_FatimaTwinpeaksCalDataFolder_FatimaTwinpeaksCalData];
UShort_t   FatimaTwinpeaksCalData_fwr_subsystem_id[kMaxcbmout_0_FatimaTwinpeaksCalDataFolder_FatimaTwinpeaksCalData];
ULong_t    FatimaTwinpeaksCalData_fwr_t[kMaxcbmout_0_FatimaTwinpeaksCalDataFolder_FatimaTwinpeaksCalData];
Long_t     FatimaTwinpeaksCalData_fabsolute_event_time[kMaxcbmout_0_FatimaTwinpeaksCalDataFolder_FatimaTwinpeaksCalData];

// fatima timemachine //
Int_t      FatimaTimeMachineData_;
UInt_t     FatimaTimeMachineData_fUniqueID[kMaxcbmout_0_FatimaTimeMachineDataFolder_FatimaTimeMachineData];
UInt_t     FatimaTimeMachineData_fBits[kMaxcbmout_0_FatimaTimeMachineDataFolder_FatimaTimeMachineData];
Double_t   FatimaTimeMachineData_fundelayed_time[kMaxcbmout_0_FatimaTimeMachineDataFolder_FatimaTimeMachineData];
Double_t   FatimaTimeMachineData_fdelayed_time[kMaxcbmout_0_FatimaTimeMachineDataFolder_FatimaTimeMachineData];
UInt_t     FatimaTimeMachineData_fwr_subsystem_id[kMaxcbmout_0_FatimaTimeMachineDataFolder_FatimaTimeMachineData];
ULong_t    FatimaTimeMachineData_fwr_t[kMaxcbmout_0_FatimaTimeMachineDataFolder_FatimaTimeMachineData];

// fatima vme //
Int_t      FatimaVmeCalData_;
UInt_t     FatimaVmeCalData_fUniqueID[kMaxcbmout_0_FatimaVmeCalDataFolder_FatimaVmeCalData];
UInt_t     FatimaVmeCalData_fBits[kMaxcbmout_0_FatimaVmeCalDataFolder_FatimaVmeCalData];
ULong_t    FatimaVmeCalData_fwr_t[kMaxcbmout_0_FatimaVmeCalDataFolder_FatimaVmeCalData];
//std::vector<unsigned int> fsingles_qdc_id[1];
//std::vector<unsigned int> fsingles_e_raw[1];
vector<unsigned int>  FatimaVmeCalData_fsingles_e[kMaxcbmout_0_FatimaVmeCalDataFolder_FatimaVmeCalData];
vector<unsigned int>  FatimaVmeCalData_fsingles_e_raw[kMaxcbmout_0_FatimaVmeCalDataFolder_FatimaVmeCalData];
vector<unsigned int>  FatimaVmeCalData_fsingles_qdc_id[kMaxcbmout_0_FatimaVmeCalDataFolder_FatimaVmeCalData];
vector<unsigned int>  FatimaVmeCalData_fsingles_coarse_time[kMaxcbmout_0_FatimaVmeCalDataFolder_FatimaVmeCalData];
vector<unsigned long> FatimaVmeCalData_fsingles_fine_time[kMaxcbmout_0_FatimaVmeCalDataFolder_FatimaVmeCalData];
vector<unsigned int>  FatimaVmeCalData_fsingles_tdc_timestamp[kMaxcbmout_0_FatimaVmeCalDataFolder_FatimaVmeCalData];
vector<unsigned int>  FatimaVmeCalData_fsingles_tdc_timestamp_raw[kMaxcbmout_0_FatimaVmeCalDataFolder_FatimaVmeCalData];
vector<unsigned int>  FatimaVmeCalData_fsingles_tdc_id[kMaxcbmout_0_FatimaVmeCalDataFolder_FatimaVmeCalData];
vector<unsigned int>  FatimaVmeCalData_fsc41l_hits[kMaxcbmout_0_FatimaVmeCalDataFolder_FatimaVmeCalData];
vector<unsigned int>  FatimaVmeCalData_fsc41r_hits[kMaxcbmout_0_FatimaVmeCalDataFolder_FatimaVmeCalData];
vector<unsigned int>  FatimaVmeCalData_ftm_undelayed_hits[kMaxcbmout_0_FatimaVmeCalDataFolder_FatimaVmeCalData];
vector<unsigned int>  FatimaVmeCalData_ftm_delayed_hits[kMaxcbmout_0_FatimaVmeCalDataFolder_FatimaVmeCalData];
vector<unsigned int>  FatimaVmeCalData_fsc41l_e_hits[kMaxcbmout_0_FatimaVmeCalDataFolder_FatimaVmeCalData];
vector<unsigned int>  FatimaVmeCalData_fsc41r_e_hits[kMaxcbmout_0_FatimaVmeCalDataFolder_FatimaVmeCalData];
vector<unsigned int>  FatimaVmeCalData_ftm_undelayed_e_hits[kMaxcbmout_0_FatimaVmeCalDataFolder_FatimaVmeCalData];
vector<unsigned int>  FatimaVmeCalData_ftm_delayed_e_hits[kMaxcbmout_0_FatimaVmeCalDataFolder_FatimaVmeCalData];
Int_t                 FatimaVmeCalData_ffatvme_mult[kMaxcbmout_0_FatimaVmeCalDataFolder_FatimaVmeCalData];
vector<unsigned int>  FatimaVmeCalData_fqdc_id[kMaxcbmout_0_FatimaVmeCalDataFolder_FatimaVmeCalData];
vector<unsigned int>  FatimaVmeCalData_fqdc_e[kMaxcbmout_0_FatimaVmeCalDataFolder_FatimaVmeCalData];
vector<unsigned int>  FatimaVmeCalData_fqdc_e_raw[kMaxcbmout_0_FatimaVmeCalDataFolder_FatimaVmeCalData];
vector<unsigned int>  FatimaVmeCalData_fqdc_t_coarse[kMaxcbmout_0_FatimaVmeCalDataFolder_FatimaVmeCalData];
vector<unsigned long> FatimaVmeCalData_fqdc_t_fine[kMaxcbmout_0_FatimaVmeCalDataFolder_FatimaVmeCalData];
vector<unsigned int>  FatimaVmeCalData_ftdc_id[kMaxcbmout_0_FatimaVmeCalDataFolder_FatimaVmeCalData];
vector<unsigned int>  FatimaVmeCalData_ftdc_time[kMaxcbmout_0_FatimaVmeCalDataFolder_FatimaVmeCalData];
vector<unsigned int>  FatimaVmeCalData_ftdc_time_raw[kMaxcbmout_0_FatimaVmeCalDataFolder_FatimaVmeCalData];


void init_setaddr(TTree *fChain){

	// Set branch addresses //
	// event //
	//fChain->SetBranchAddress("EventHeader.", &EventHeader);
	//fChain->SetBranchAddress("EventHeader", &EventHeader);
	fChain->SetBranchAddress("EventHeader.fEventno", &event_no);
	fChain->SetBranchAddress("EventHeader.fTrigger", &EventHeader_fTrigger);
	fChain->SetBranchAddress("EventHeader.fTimeStamp", &EventHeader_fTimeStamp);
	fChain->SetBranchAddress("EventHeader.fSpillFlag", &EventHeader_fSpillFlag);
	// fatima twinpeaks //
	fChain->SetBranchAddress("FatimaTwinpeaksCalData", &FatimaTwinpeaksCalData_);
	fChain->SetBranchAddress("FatimaTwinpeaksCalData.fUniqueID", FatimaTwinpeaksCalData_fUniqueID);
	fChain->SetBranchAddress("FatimaTwinpeaksCalData.fBits", FatimaTwinpeaksCalData_fBits);
	fChain->SetBranchAddress("FatimaTwinpeaksCalData.fboard_id", FatimaTwinpeaksCalData_fboard_id);
	fChain->SetBranchAddress("FatimaTwinpeaksCalData.fch_ID", FatimaTwinpeaksCalData_fch_ID);
	fChain->SetBranchAddress("FatimaTwinpeaksCalData.fdetector_id", FatimaTwinpeaksCalData_fdetector_id);
	fChain->SetBranchAddress("FatimaTwinpeaksCalData.fslow_lead_time", FatimaTwinpeaksCalData_fslow_lead_time);
	fChain->SetBranchAddress("FatimaTwinpeaksCalData.fslow_trail_time", FatimaTwinpeaksCalData_fslow_trail_time);
	fChain->SetBranchAddress("FatimaTwinpeaksCalData.ffast_lead_time", FatimaTwinpeaksCalData_ffast_lead_time);
	fChain->SetBranchAddress("FatimaTwinpeaksCalData.ffast_trail_time", FatimaTwinpeaksCalData_ffast_trail_time);
	fChain->SetBranchAddress("FatimaTwinpeaksCalData.ffast_ToT", FatimaTwinpeaksCalData_ffast_ToT);
	fChain->SetBranchAddress("FatimaTwinpeaksCalData.fslow_ToT", FatimaTwinpeaksCalData_fslow_ToT);
	fChain->SetBranchAddress("FatimaTwinpeaksCalData.fenergy", FatimaTwinpeaksCalData_fenergy);
	fChain->SetBranchAddress("FatimaTwinpeaksCalData.fwr_subsystem_id", FatimaTwinpeaksCalData_fwr_subsystem_id);
	fChain->SetBranchAddress("FatimaTwinpeaksCalData.fwr_t", FatimaTwinpeaksCalData_fwr_t);
	fChain->SetBranchAddress("FatimaTwinpeaksCalData.fabsolute_event_time", FatimaTwinpeaksCalData_fabsolute_event_time);
	// fatima timemachine //
	fChain->SetBranchAddress("FatimaTimeMachineData", &FatimaTimeMachineData_);
	fChain->SetBranchAddress("FatimaTimeMachineData.fUniqueID", FatimaTimeMachineData_fUniqueID);
	fChain->SetBranchAddress("FatimaTimeMachineData.fBits", FatimaTimeMachineData_fBits);
	fChain->SetBranchAddress("FatimaTimeMachineData.fundelayed_time", FatimaTimeMachineData_fundelayed_time);
	fChain->SetBranchAddress("FatimaTimeMachineData.fdelayed_time", FatimaTimeMachineData_fdelayed_time);
	fChain->SetBranchAddress("FatimaTimeMachineData.fwr_subsystem_id", FatimaTimeMachineData_fwr_subsystem_id);
	fChain->SetBranchAddress("FatimaTimeMachineData.fwr_t", FatimaTimeMachineData_fwr_t);
	// fatima vme cal data //
	//fChain->SetBranchAddress("FatimaVmeCalData.fsingles_qdc_id", fsingles_qdc_id);
	//fChain->SetBranchAddress("FatimaVmeCalData.fsingles_e_raw", fsingles_e_raw);
	fChain->SetBranchAddress("FatimaVmeCalData", &FatimaVmeCalData_);
	fChain->SetBranchAddress("FatimaVmeCalData.fUniqueID", FatimaVmeCalData_fUniqueID);
	fChain->SetBranchAddress("FatimaVmeCalData.fBits", FatimaVmeCalData_fBits);
	fChain->SetBranchAddress("FatimaVmeCalData.fwr_t", FatimaVmeCalData_fwr_t);
	fChain->SetBranchAddress("FatimaVmeCalData.fsingles_e", FatimaVmeCalData_fsingles_e);
	fChain->SetBranchAddress("FatimaVmeCalData.fsingles_e_raw", FatimaVmeCalData_fsingles_e_raw);
	fChain->SetBranchAddress("FatimaVmeCalData.fsingles_qdc_id", FatimaVmeCalData_fsingles_qdc_id);
	fChain->SetBranchAddress("FatimaVmeCalData.fsingles_coarse_time", FatimaVmeCalData_fsingles_coarse_time);
	fChain->SetBranchAddress("FatimaVmeCalData.fsingles_fine_time", FatimaVmeCalData_fsingles_fine_time);
	fChain->SetBranchAddress("FatimaVmeCalData.fsingles_tdc_timestamp", FatimaVmeCalData_fsingles_tdc_timestamp);
	fChain->SetBranchAddress("FatimaVmeCalData.fsingles_tdc_timestamp_raw", FatimaVmeCalData_fsingles_tdc_timestamp_raw);
	fChain->SetBranchAddress("FatimaVmeCalData.fsingles_tdc_id", FatimaVmeCalData_fsingles_tdc_id);
	fChain->SetBranchAddress("FatimaVmeCalData.fsc41l_hits", FatimaVmeCalData_fsc41l_hits);
	fChain->SetBranchAddress("FatimaVmeCalData.fsc41r_hits", FatimaVmeCalData_fsc41r_hits);
	fChain->SetBranchAddress("FatimaVmeCalData.ftm_undelayed_hits", FatimaVmeCalData_ftm_undelayed_hits);
	fChain->SetBranchAddress("FatimaVmeCalData.ftm_delayed_hits", FatimaVmeCalData_ftm_delayed_hits);
	fChain->SetBranchAddress("FatimaVmeCalData.fsc41l_e_hits", FatimaVmeCalData_fsc41l_e_hits);
	fChain->SetBranchAddress("FatimaVmeCalData.fsc41r_e_hits", FatimaVmeCalData_fsc41r_e_hits);
	fChain->SetBranchAddress("FatimaVmeCalData.ftm_undelayed_e_hits", FatimaVmeCalData_ftm_undelayed_e_hits);
	fChain->SetBranchAddress("FatimaVmeCalData.ftm_delayed_e_hits", FatimaVmeCalData_ftm_delayed_e_hits);
	fChain->SetBranchAddress("FatimaVmeCalData.ffatvme_mult", FatimaVmeCalData_ffatvme_mult);
	fChain->SetBranchAddress("FatimaVmeCalData.fqdc_id", FatimaVmeCalData_fqdc_id);
	fChain->SetBranchAddress("FatimaVmeCalData.fqdc_e", FatimaVmeCalData_fqdc_e);
	fChain->SetBranchAddress("FatimaVmeCalData.fqdc_e_raw", FatimaVmeCalData_fqdc_e_raw);
	fChain->SetBranchAddress("FatimaVmeCalData.fqdc_t_coarse", FatimaVmeCalData_fqdc_t_coarse);
	fChain->SetBranchAddress("FatimaVmeCalData.fqdc_t_fine", FatimaVmeCalData_fqdc_t_fine);
	fChain->SetBranchAddress("FatimaVmeCalData.ftdc_id", FatimaVmeCalData_ftdc_id);
	fChain->SetBranchAddress("FatimaVmeCalData.ftdc_time", FatimaVmeCalData_ftdc_time);
	fChain->SetBranchAddress("FatimaVmeCalData.ftdc_time_raw", FatimaVmeCalData_ftdc_time_raw);

}
