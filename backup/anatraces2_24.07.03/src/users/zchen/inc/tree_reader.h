

#include <TTree.h>


Int_t fverbos = 0;

void addElementsToMap(std::map<TString, uintptr_t>& myMap) {
	// Add elements to the map
	// for lisa_offline //
	
	myMap["LisaCalData.trace"] = 0x176f0;//branch and offset
	
}

void read_leaf(TTree *tree, Long64_t entry, Int_t lenmax)
{

	if(fverbos==1)std::cout<<"---> Welcome to myShow()"<<std::endl;
	if (entry != -1) {
		Int_t ret = tree->LoadTree(entry);
		if (ret == -2) {
			std::cout<<"Cannot read entry(entry does not exist)"<<std::endl;
			return;
		} else if (ret == -1) {
			std::cout<<"Cannot read entry(I/O error)"<<std::endl;;
			return;
		}
		ret = tree->GetEntry(entry);
		if (ret == -1) {
			std::cout<<"Cannot read entry(I/O error)"<<std::endl;
			return;
		} else if (ret == 0) {
			std::cout<<"Cannot read entry(no data read)"<<std::endl;;
			return;
		}
	}
	std::map<TString, uintptr_t> offsetMap;
	addElementsToMap(offsetMap);
	TObjArray* leaves  = tree->GetListOfLeaves();
	Int_t nleaves = leaves->GetEntriesFast();
	if(fverbos==1)std::cout<<"--->nleaves = "<<nleaves<<std::endl;
	for (Int_t i = 0; i < nleaves; i++) {
		TLeaf* leaf = (TLeaf*) leaves->UncheckedAt(i);
		Int_t len = leaf->GetLen();
		if (len <= 0) {
			continue;
		}
		len = TMath::Min(len, lenmax);
		if (leaf->IsA() == TLeafElement::Class()) {

			TString lfullname = (TString)leaf->GetFullName();
			TString lname = (TString)leaf->GetName();
			//bool fIs_qdc_id = (lfullname == "FatimaVmeCalData.fsingles_qdc_id");
			//bool fIs_e_raw = (lfullname == "FatimaVmeCalData.fsingles_e_raw");
			//bool fIs_e = (lfullname == "FatimaVmeCalData.fsingles_e");
			//bool fIs_found = fIs_qdc_id || fIs_e_raw || fIs_e;
			bool fIs_found = (offsetMap.find(lfullname) != offsetMap.end());
			if( !fIs_found )continue;

			auto lpointer_type = leaf->GetTypeName();
			Int_t lcnt = leaf->GetLen(); 
			if(fverbos == 1){
				std::cout<<"full name = "<<lfullname<<std::endl;
				std::cout<<"leaf name = "<<lname<<std::endl;
				std::cout<<"value type = "<<lpointer_type<<std::endl;
				std::cout<<"getLen = "<<lcnt<<std::endl;
				std::cout << "Leaf type: " << typeid(*leaf).name() << std::endl;
			}

			// Check if the leaf is of type TLeafElement
			TLeafElement* leafElement = dynamic_cast<TLeafElement*>(leaf);

			// Get the associated TBranchElement
			TBranchElement* branchElement = dynamic_cast<TBranchElement*>(leafElement->GetBranch());

			TString fbran_name = branchElement->GetFullName();
			if(fverbos == 1)std::cout<<"branch: full name "<<fbran_name<<std::endl;
			Int_t fbran_ndata = branchElement->GetNdata();
			if(fverbos == 1)std::cout<<"branch: Ndata() "<<fbran_ndata<<std::endl;
			// Get the object associated with the branch
			char* object = branchElement->GetObject();
			if(fverbos ==1)std::cout << "Object address: " << static_cast<void*>(object) << std::endl;
			if(fverbos ==1)leaf->PrintValue(len);
			//correct the addr
			uintptr_t address1 = reinterpret_cast<uintptr_t>(object);
			uintptr_t offset;
			//if(fIs_e)offset = 0x4e0;
			//if(fIs_e_raw)offset = 0x4f8;
			//if(fIs_qdc_id)offset = 0x510;
			auto it = offsetMap.find(lfullname);
			offset = it->second;
			uintptr_t addr_corr = address1 + offset;
			if(fverbos){
				std::cout<<"old addr(from object) = "<< std::hex << address1<<std::endl;
				std::cout<<"offset(read from addr map) = "<< std::hex << offset<<std::endl;
				std::cout<<"correct addr = "<< std::hex << addr_corr <<std::endl;
			}
			char* ptr = reinterpret_cast<char*>(addr_corr);

			// Assuming the object is a vector<unsigned int>
			std::vector<unsigned int>* vec = reinterpret_cast<std::vector<unsigned int>*>(ptr);
			if(fverbos==1)std::cout<<"addr of vec = "<<vec<<std::endl;
			if (vec) {
				if(fverbos == 1)std::cout << "Vector size: " << vec->size() << std::endl;
				for (size_t i = 0; i < vec->size(); ++i) {
					std::cout << "Element "<<i<<" :** "<< std::dec << vec->at(i) << std::endl;
				}
			} else {
				std::cerr << "Failed to cast object to vector<unsigned int>*." << std::endl;
			}
			//continue;
		}
	}//end of nleaves
	if(fverbos==1)std::cout<<"---> End of myShow()"<<std::endl;
}


void setLeafAddress(TTree *tree, Long64_t entry, std::map<TString, uintptr_t>& myMap, Int_t *flen )
{

	if(fverbos==1)std::cout<<"---> Welcome to myShow()"<<std::endl;
	if (entry != -1) {
		Int_t ret = tree->LoadTree(entry);
		if (ret == -2) {
			std::cout<<"Cannot read entry(entry does not exist)"<<std::endl;
			return;
		} else if (ret == -1) {
			std::cout<<"Cannot read entry(I/O error)"<<std::endl;;
			return;
		}
		ret = tree->GetEntry(entry);
		if (ret == -1) {
			std::cout<<"Cannot read entry(I/O error)"<<std::endl;
			return;
		} else if (ret == 0) {
			std::cout<<"Cannot read entry(no data read)"<<std::endl;;
			return;
		}
	}
	std::map<TString, uintptr_t> offsetMap;
	addElementsToMap(offsetMap);
	TObjArray* leaves  = tree->GetListOfLeaves();
	Int_t nleaves = leaves->GetEntriesFast();
	if(fverbos==1)std::cout<<"--->nleaves = "<<nleaves<<std::endl;
	for (Int_t i = 0; i < nleaves; i++) {
		TLeaf* leaf = (TLeaf*) leaves->UncheckedAt(i);
		Int_t len = leaf->GetLen();
		if (len <= 0) {
			continue;
		}
		if (leaf->IsA() == TLeafElement::Class()) {

			TString lfullname = (TString)leaf->GetFullName();
			TString lname = (TString)leaf->GetName();
			//bool fIs_qdc_id = (lfullname == "FatimaVmeCalData.fsingles_qdc_id");
			//bool fIs_e_raw = (lfullname == "FatimaVmeCalData.fsingles_e_raw");
			//bool fIs_e = (lfullname == "FatimaVmeCalData.fsingles_e");
			//bool fIs_found = fIs_qdc_id || fIs_e_raw || fIs_e;
			bool fIs_found = (offsetMap.find(lfullname) != offsetMap.end());
			if( !fIs_found )continue;

			auto lpointer_type = leaf->GetTypeName();
			Int_t lcnt = leaf->GetLen(); 
			flen[0] = lcnt;
			if(fverbos == 1){
				std::cout<<"full name = "<<lfullname<<std::endl;
				std::cout<<"leaf name = "<<lname<<std::endl;
				std::cout<<"value type = "<<lpointer_type<<std::endl;
				std::cout<<"getLen = "<<lcnt<<std::endl;
				std::cout << "Leaf type: " << typeid(*leaf).name() << std::endl;
			}

			// Check if the leaf is of type TLeafElement
			TLeafElement* leafElement = dynamic_cast<TLeafElement*>(leaf);

			// Get the associated TBranchElement
			TBranchElement* branchElement = dynamic_cast<TBranchElement*>(leafElement->GetBranch());

			TString fbran_name = branchElement->GetFullName();
			if(fverbos == 1)std::cout<<"branch: full name "<<fbran_name<<std::endl;
			Int_t fbran_ndata = branchElement->GetNdata();
			if(fverbos == 1)std::cout<<"branch: Ndata() "<<fbran_ndata<<std::endl;
			// Get the object associated with the branch
			char* object = branchElement->GetObject();
			if(fverbos ==1)std::cout << "Object address: " << static_cast<void*>(object) << std::endl;
			if(fverbos ==1)leaf->PrintValue(len);
			//correct the addr
			uintptr_t address1 = reinterpret_cast<uintptr_t>(object);
			uintptr_t offset;
			//if(fIs_e)offset = 0x4e0;
			//if(fIs_e_raw)offset = 0x4f8;
			//if(fIs_qdc_id)offset = 0x510;
			auto it = offsetMap.find(lfullname);
			offset = it->second;
			uintptr_t addr_corr = address1 + offset;
			if(fverbos == 1){
				std::cout<<"old addr = "<< std::hex << address1<<std::endl;
				std::cout<<"offset = "<< std::hex << offset<<std::endl;
				std::cout<<"correct addr = "<< std::hex << addr_corr <<std::endl;
			}
			myMap.emplace(lfullname, addr_corr);
		}
	}//end of nleaves
}

