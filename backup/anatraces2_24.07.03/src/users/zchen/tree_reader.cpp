#include <iostream>
#include <vector>
#include <TFile.h>
#include <TTree.h>
#include <TBranch.h>
#include "inc/tree_reader.h"

void tree_reader() {
	// Open the ROOT file
	TFile *file = TFile::Open("run_0022_test.root");
	if (!file || file->IsZombie()) {
		std::cerr << "Error opening file!" << std::endl;
		return;
	}

	// Get the TTree
	TTree *tree = nullptr;
	file->GetObject("evt", tree);
	if (!tree) {
		std::cerr << "Tree 'evt' not found!" << std::endl;
		return;
	}

	std::map<TString, uintptr_t> addrMap;//

	vector<Int_t> ftracesY[10];//multi hit
	vector<Int_t> ftracesX[10];
	//vector<Int_t> ftracesY;//multi hit
	//vector<Int_t> ftracesX;
	Int_t flen[1];//length of leaf
	//tree->Show(0);
	for (Long64_t i = 0; i < 1; i++) {// loop from jentry to ientry
		std::cout<<"--->Entry = "<< i <<std::endl;

		//read_leaf(tree,i,20);//for debug	

		setLeafAddress(tree,i,addrMap, flen);//get addr of leaves; (tree, Jentry, addr container)

		// *** for display *** //
		// Use an iterator to access the element
		std::cout<<"*** Size of map : "<<addrMap.size()<<std::endl;

		// Create a map to store TString keys and pointers to vectors
		std::map<TString, std::vector<unsigned short>*> v_FatimaVmeData;

		// Initialize the map with the vectors from addrMap
		for (const auto& pair : addrMap) {
			v_FatimaVmeData[pair.first] = reinterpret_cast<std::vector<unsigned short>*>(pair.second);
		}

		// Initialize a counter for leaf numbers
		Int_t jj = 0;
		//...oooOO0OOooo......oooOO0OOooo......oooOO0OOooo......oooOO0OOooo......oooOO0OOooo......oooOO0OOooo......oooOO0OOooo...//
		//*** Now we successfully save FatimaVME data into a map, where the first element is Leaf's name and second one is value. ***// 
		//...oooOO0OOooo......oooOO0OOooo......oooOO0OOooo......oooOO0OOooo......oooOO0OOooo......oooOO0OOooo......oooOO0OOooo...//
		// Print names and values of the vectors
		for (const auto& pair : v_FatimaVmeData) {
			std::cout << "*** Leaf " << jj << ": Name : " << pair.first << std::endl;
			if (pair.second != nullptr) {
				std::cout<<"             size of vector:"<<pair.second->size()<<std::endl;
				std::cout << "*** Leaf " << jj << std::endl;
				std::cout << " cnt of Leaf = "<<flen[0]<<std::endl;
				if(pair.second->size() != 2000 )return;
				int cnt=0;
				for (const auto& val : *(pair.second)) {
					//std::cout << "*** cnt " << cnt << ": Value: " << val << std::endl;
					ftracesY[0].push_back(val);
					ftracesX[0].push_back(cnt);
					cnt++;
				}
			} else {
				std::cerr << "Invalid pointer for " << pair.first << std::endl;
			}
			++jj;
		}
		TCanvas *mycan = new TCanvas("mycan","mycan",10,10,800,800);

		TGraph *my = new TGraph(ftracesY[0].size(),ftracesX[0].data(),ftracesY[0].data());
		//TGraph *my = new TGraph(ftracesY[0].size(),ftracesX[0].data(),ftracesY[0].data());
		my->Draw("AP*");

	}//end of loop

	// Clean up
	file->Close();
}

int main() {
	tree_reader();
	return 0;
}


void addrOffsetCalculator( uintptr_t addr1, uintptr_t addr2 ){

	uintptr_t offset = addr1 > addr2 ? addr1 - addr2 : addr2 - addr1;

	// Print the addresses and the offset
	std::cout<<"...oooOO0OOooo......oooOO0OOooo......oooOO0OOooo......oooOO0OOooo..."<<std::endl;
	std::cout << "Address 1: 0x" << std::hex << addr1 << std::endl;
	std::cout << "Address 2: 0x" << std::hex << addr2 << std::endl;
	std::cout << "Offset: 0x" << std::hex << offset << " (" << std::dec << offset << " bytes)" << std::endl;

}
