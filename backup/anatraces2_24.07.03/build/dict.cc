// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME builddIdict
#define R__NO_DEPRECATION

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// The generated code does not explicitly qualifies STL entities
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "inc/dict.hh"

// Header files passed via #pragma extra_include

namespace ROOT {
   static void *new_lisa_ucesb(void *p = 0);
   static void *newArray_lisa_ucesb(Long_t size, void *p);
   static void delete_lisa_ucesb(void *p);
   static void deleteArray_lisa_ucesb(void *p);
   static void destruct_lisa_ucesb(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::lisa_ucesb*)
   {
      ::lisa_ucesb *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::lisa_ucesb >(0);
      static ::ROOT::TGenericClassInfo 
         instance("lisa_ucesb", ::lisa_ucesb::Class_Version(), "inc/lisa_ucesb.hh", 17,
                  typeid(::lisa_ucesb), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::lisa_ucesb::Dictionary, isa_proxy, 4,
                  sizeof(::lisa_ucesb) );
      instance.SetNew(&new_lisa_ucesb);
      instance.SetNewArray(&newArray_lisa_ucesb);
      instance.SetDelete(&delete_lisa_ucesb);
      instance.SetDeleteArray(&deleteArray_lisa_ucesb);
      instance.SetDestructor(&destruct_lisa_ucesb);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::lisa_ucesb*)
   {
      return GenerateInitInstanceLocal((::lisa_ucesb*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::lisa_ucesb*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_lisa_settings(void *p = 0);
   static void *newArray_lisa_settings(Long_t size, void *p);
   static void delete_lisa_settings(void *p);
   static void deleteArray_lisa_settings(void *p);
   static void destruct_lisa_settings(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::lisa_settings*)
   {
      ::lisa_settings *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::lisa_settings >(0);
      static ::ROOT::TGenericClassInfo 
         instance("lisa_settings", ::lisa_settings::Class_Version(), "inc/lisa_settings.hh", 15,
                  typeid(::lisa_settings), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::lisa_settings::Dictionary, isa_proxy, 4,
                  sizeof(::lisa_settings) );
      instance.SetNew(&new_lisa_settings);
      instance.SetNewArray(&newArray_lisa_settings);
      instance.SetDelete(&delete_lisa_settings);
      instance.SetDeleteArray(&deleteArray_lisa_settings);
      instance.SetDestructor(&destruct_lisa_settings);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::lisa_settings*)
   {
      return GenerateInitInstanceLocal((::lisa_settings*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::lisa_settings*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_lisa_febex(void *p = 0);
   static void *newArray_lisa_febex(Long_t size, void *p);
   static void delete_lisa_febex(void *p);
   static void deleteArray_lisa_febex(void *p);
   static void destruct_lisa_febex(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::lisa_febex*)
   {
      ::lisa_febex *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::lisa_febex >(0);
      static ::ROOT::TGenericClassInfo 
         instance("lisa_febex", ::lisa_febex::Class_Version(), "inc/lisa_febex.hh", 20,
                  typeid(::lisa_febex), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::lisa_febex::Dictionary, isa_proxy, 4,
                  sizeof(::lisa_febex) );
      instance.SetNew(&new_lisa_febex);
      instance.SetNewArray(&newArray_lisa_febex);
      instance.SetDelete(&delete_lisa_febex);
      instance.SetDeleteArray(&deleteArray_lisa_febex);
      instance.SetDestructor(&destruct_lisa_febex);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::lisa_febex*)
   {
      return GenerateInitInstanceLocal((::lisa_febex*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::lisa_febex*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_lisa_optManager(void *p = 0);
   static void *newArray_lisa_optManager(Long_t size, void *p);
   static void delete_lisa_optManager(void *p);
   static void deleteArray_lisa_optManager(void *p);
   static void destruct_lisa_optManager(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::lisa_optManager*)
   {
      ::lisa_optManager *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::lisa_optManager >(0);
      static ::ROOT::TGenericClassInfo 
         instance("lisa_optManager", ::lisa_optManager::Class_Version(), "inc/lisa_OutputManager.hh", 13,
                  typeid(::lisa_optManager), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::lisa_optManager::Dictionary, isa_proxy, 4,
                  sizeof(::lisa_optManager) );
      instance.SetNew(&new_lisa_optManager);
      instance.SetNewArray(&newArray_lisa_optManager);
      instance.SetDelete(&delete_lisa_optManager);
      instance.SetDeleteArray(&deleteArray_lisa_optManager);
      instance.SetDestructor(&destruct_lisa_optManager);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::lisa_optManager*)
   {
      return GenerateInitInstanceLocal((::lisa_optManager*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::lisa_optManager*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_lisa_scope(void *p = 0);
   static void *newArray_lisa_scope(Long_t size, void *p);
   static void delete_lisa_scope(void *p);
   static void deleteArray_lisa_scope(void *p);
   static void destruct_lisa_scope(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::lisa_scope*)
   {
      ::lisa_scope *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::lisa_scope >(0);
      static ::ROOT::TGenericClassInfo 
         instance("lisa_scope", ::lisa_scope::Class_Version(), "inc/lisa_scope.hh", 22,
                  typeid(::lisa_scope), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::lisa_scope::Dictionary, isa_proxy, 4,
                  sizeof(::lisa_scope) );
      instance.SetNew(&new_lisa_scope);
      instance.SetNewArray(&newArray_lisa_scope);
      instance.SetDelete(&delete_lisa_scope);
      instance.SetDeleteArray(&deleteArray_lisa_scope);
      instance.SetDestructor(&destruct_lisa_scope);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::lisa_scope*)
   {
      return GenerateInitInstanceLocal((::lisa_scope*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::lisa_scope*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_lisa_vis(void *p = 0);
   static void *newArray_lisa_vis(Long_t size, void *p);
   static void delete_lisa_vis(void *p);
   static void deleteArray_lisa_vis(void *p);
   static void destruct_lisa_vis(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::lisa_vis*)
   {
      ::lisa_vis *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::lisa_vis >(0);
      static ::ROOT::TGenericClassInfo 
         instance("lisa_vis", ::lisa_vis::Class_Version(), "inc/lisa_vis.hh", 19,
                  typeid(::lisa_vis), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::lisa_vis::Dictionary, isa_proxy, 4,
                  sizeof(::lisa_vis) );
      instance.SetNew(&new_lisa_vis);
      instance.SetNewArray(&newArray_lisa_vis);
      instance.SetDelete(&delete_lisa_vis);
      instance.SetDeleteArray(&deleteArray_lisa_vis);
      instance.SetDestructor(&destruct_lisa_vis);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::lisa_vis*)
   {
      return GenerateInitInstanceLocal((::lisa_vis*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::lisa_vis*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

//______________________________________________________________________________
atomic_TClass_ptr lisa_ucesb::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *lisa_ucesb::Class_Name()
{
   return "lisa_ucesb";
}

//______________________________________________________________________________
const char *lisa_ucesb::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::lisa_ucesb*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int lisa_ucesb::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::lisa_ucesb*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *lisa_ucesb::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::lisa_ucesb*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *lisa_ucesb::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::lisa_ucesb*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr lisa_settings::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *lisa_settings::Class_Name()
{
   return "lisa_settings";
}

//______________________________________________________________________________
const char *lisa_settings::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::lisa_settings*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int lisa_settings::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::lisa_settings*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *lisa_settings::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::lisa_settings*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *lisa_settings::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::lisa_settings*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr lisa_febex::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *lisa_febex::Class_Name()
{
   return "lisa_febex";
}

//______________________________________________________________________________
const char *lisa_febex::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::lisa_febex*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int lisa_febex::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::lisa_febex*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *lisa_febex::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::lisa_febex*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *lisa_febex::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::lisa_febex*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr lisa_optManager::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *lisa_optManager::Class_Name()
{
   return "lisa_optManager";
}

//______________________________________________________________________________
const char *lisa_optManager::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::lisa_optManager*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int lisa_optManager::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::lisa_optManager*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *lisa_optManager::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::lisa_optManager*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *lisa_optManager::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::lisa_optManager*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr lisa_scope::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *lisa_scope::Class_Name()
{
   return "lisa_scope";
}

//______________________________________________________________________________
const char *lisa_scope::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::lisa_scope*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int lisa_scope::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::lisa_scope*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *lisa_scope::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::lisa_scope*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *lisa_scope::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::lisa_scope*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr lisa_vis::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *lisa_vis::Class_Name()
{
   return "lisa_vis";
}

//______________________________________________________________________________
const char *lisa_vis::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::lisa_vis*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int lisa_vis::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::lisa_vis*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *lisa_vis::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::lisa_vis*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *lisa_vis::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::lisa_vis*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
void lisa_ucesb::Streamer(TBuffer &R__b)
{
   // Stream an object of class lisa_ucesb.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(lisa_ucesb::Class(),this);
   } else {
      R__b.WriteClassBuffer(lisa_ucesb::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_lisa_ucesb(void *p) {
      return  p ? new(p) ::lisa_ucesb : new ::lisa_ucesb;
   }
   static void *newArray_lisa_ucesb(Long_t nElements, void *p) {
      return p ? new(p) ::lisa_ucesb[nElements] : new ::lisa_ucesb[nElements];
   }
   // Wrapper around operator delete
   static void delete_lisa_ucesb(void *p) {
      delete ((::lisa_ucesb*)p);
   }
   static void deleteArray_lisa_ucesb(void *p) {
      delete [] ((::lisa_ucesb*)p);
   }
   static void destruct_lisa_ucesb(void *p) {
      typedef ::lisa_ucesb current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::lisa_ucesb

//______________________________________________________________________________
void lisa_settings::Streamer(TBuffer &R__b)
{
   // Stream an object of class lisa_settings.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(lisa_settings::Class(),this);
   } else {
      R__b.WriteClassBuffer(lisa_settings::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_lisa_settings(void *p) {
      return  p ? new(p) ::lisa_settings : new ::lisa_settings;
   }
   static void *newArray_lisa_settings(Long_t nElements, void *p) {
      return p ? new(p) ::lisa_settings[nElements] : new ::lisa_settings[nElements];
   }
   // Wrapper around operator delete
   static void delete_lisa_settings(void *p) {
      delete ((::lisa_settings*)p);
   }
   static void deleteArray_lisa_settings(void *p) {
      delete [] ((::lisa_settings*)p);
   }
   static void destruct_lisa_settings(void *p) {
      typedef ::lisa_settings current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::lisa_settings

//______________________________________________________________________________
void lisa_febex::Streamer(TBuffer &R__b)
{
   // Stream an object of class lisa_febex.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(lisa_febex::Class(),this);
   } else {
      R__b.WriteClassBuffer(lisa_febex::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_lisa_febex(void *p) {
      return  p ? new(p) ::lisa_febex : new ::lisa_febex;
   }
   static void *newArray_lisa_febex(Long_t nElements, void *p) {
      return p ? new(p) ::lisa_febex[nElements] : new ::lisa_febex[nElements];
   }
   // Wrapper around operator delete
   static void delete_lisa_febex(void *p) {
      delete ((::lisa_febex*)p);
   }
   static void deleteArray_lisa_febex(void *p) {
      delete [] ((::lisa_febex*)p);
   }
   static void destruct_lisa_febex(void *p) {
      typedef ::lisa_febex current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::lisa_febex

//______________________________________________________________________________
void lisa_optManager::Streamer(TBuffer &R__b)
{
   // Stream an object of class lisa_optManager.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(lisa_optManager::Class(),this);
   } else {
      R__b.WriteClassBuffer(lisa_optManager::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_lisa_optManager(void *p) {
      return  p ? new(p) ::lisa_optManager : new ::lisa_optManager;
   }
   static void *newArray_lisa_optManager(Long_t nElements, void *p) {
      return p ? new(p) ::lisa_optManager[nElements] : new ::lisa_optManager[nElements];
   }
   // Wrapper around operator delete
   static void delete_lisa_optManager(void *p) {
      delete ((::lisa_optManager*)p);
   }
   static void deleteArray_lisa_optManager(void *p) {
      delete [] ((::lisa_optManager*)p);
   }
   static void destruct_lisa_optManager(void *p) {
      typedef ::lisa_optManager current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::lisa_optManager

//______________________________________________________________________________
void lisa_scope::Streamer(TBuffer &R__b)
{
   // Stream an object of class lisa_scope.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(lisa_scope::Class(),this);
   } else {
      R__b.WriteClassBuffer(lisa_scope::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_lisa_scope(void *p) {
      return  p ? new(p) ::lisa_scope : new ::lisa_scope;
   }
   static void *newArray_lisa_scope(Long_t nElements, void *p) {
      return p ? new(p) ::lisa_scope[nElements] : new ::lisa_scope[nElements];
   }
   // Wrapper around operator delete
   static void delete_lisa_scope(void *p) {
      delete ((::lisa_scope*)p);
   }
   static void deleteArray_lisa_scope(void *p) {
      delete [] ((::lisa_scope*)p);
   }
   static void destruct_lisa_scope(void *p) {
      typedef ::lisa_scope current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::lisa_scope

//______________________________________________________________________________
void lisa_vis::Streamer(TBuffer &R__b)
{
   // Stream an object of class lisa_vis.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(lisa_vis::Class(),this);
   } else {
      R__b.WriteClassBuffer(lisa_vis::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_lisa_vis(void *p) {
      return  p ? new(p) ::lisa_vis : new ::lisa_vis;
   }
   static void *newArray_lisa_vis(Long_t nElements, void *p) {
      return p ? new(p) ::lisa_vis[nElements] : new ::lisa_vis[nElements];
   }
   // Wrapper around operator delete
   static void delete_lisa_vis(void *p) {
      delete ((::lisa_vis*)p);
   }
   static void deleteArray_lisa_vis(void *p) {
      delete [] ((::lisa_vis*)p);
   }
   static void destruct_lisa_vis(void *p) {
      typedef ::lisa_vis current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::lisa_vis

namespace ROOT {
   static TClass *vectorlEscope_datagR_Dictionary();
   static void vectorlEscope_datagR_TClassManip(TClass*);
   static void *new_vectorlEscope_datagR(void *p = 0);
   static void *newArray_vectorlEscope_datagR(Long_t size, void *p);
   static void delete_vectorlEscope_datagR(void *p);
   static void deleteArray_vectorlEscope_datagR(void *p);
   static void destruct_vectorlEscope_datagR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<scope_data>*)
   {
      vector<scope_data> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<scope_data>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<scope_data>", -2, "vector", 339,
                  typeid(vector<scope_data>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEscope_datagR_Dictionary, isa_proxy, 0,
                  sizeof(vector<scope_data>) );
      instance.SetNew(&new_vectorlEscope_datagR);
      instance.SetNewArray(&newArray_vectorlEscope_datagR);
      instance.SetDelete(&delete_vectorlEscope_datagR);
      instance.SetDeleteArray(&deleteArray_vectorlEscope_datagR);
      instance.SetDestructor(&destruct_vectorlEscope_datagR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<scope_data> >()));

      ::ROOT::AddClassAlternate("vector<scope_data>","std::vector<scope_data, std::allocator<scope_data> >");
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<scope_data>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEscope_datagR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<scope_data>*)0x0)->GetClass();
      vectorlEscope_datagR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEscope_datagR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEscope_datagR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<scope_data> : new vector<scope_data>;
   }
   static void *newArray_vectorlEscope_datagR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<scope_data>[nElements] : new vector<scope_data>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEscope_datagR(void *p) {
      delete ((vector<scope_data>*)p);
   }
   static void deleteArray_vectorlEscope_datagR(void *p) {
      delete [] ((vector<scope_data>*)p);
   }
   static void destruct_vectorlEscope_datagR(void *p) {
      typedef vector<scope_data> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<scope_data>

namespace ROOT {
   static TClass *vectorlEintgR_Dictionary();
   static void vectorlEintgR_TClassManip(TClass*);
   static void *new_vectorlEintgR(void *p = 0);
   static void *newArray_vectorlEintgR(Long_t size, void *p);
   static void delete_vectorlEintgR(void *p);
   static void deleteArray_vectorlEintgR(void *p);
   static void destruct_vectorlEintgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<int>*)
   {
      vector<int> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<int>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<int>", -2, "vector", 339,
                  typeid(vector<int>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEintgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<int>) );
      instance.SetNew(&new_vectorlEintgR);
      instance.SetNewArray(&newArray_vectorlEintgR);
      instance.SetDelete(&delete_vectorlEintgR);
      instance.SetDeleteArray(&deleteArray_vectorlEintgR);
      instance.SetDestructor(&destruct_vectorlEintgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<int> >()));

      ::ROOT::AddClassAlternate("vector<int>","std::vector<int, std::allocator<int> >");
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<int>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEintgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<int>*)0x0)->GetClass();
      vectorlEintgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEintgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEintgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<int> : new vector<int>;
   }
   static void *newArray_vectorlEintgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<int>[nElements] : new vector<int>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEintgR(void *p) {
      delete ((vector<int>*)p);
   }
   static void deleteArray_vectorlEintgR(void *p) {
      delete [] ((vector<int>*)p);
   }
   static void destruct_vectorlEintgR(void *p) {
      typedef vector<int> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<int>

namespace ROOT {
   static TClass *vectorlEfloatgR_Dictionary();
   static void vectorlEfloatgR_TClassManip(TClass*);
   static void *new_vectorlEfloatgR(void *p = 0);
   static void *newArray_vectorlEfloatgR(Long_t size, void *p);
   static void delete_vectorlEfloatgR(void *p);
   static void deleteArray_vectorlEfloatgR(void *p);
   static void destruct_vectorlEfloatgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<float>*)
   {
      vector<float> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<float>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<float>", -2, "vector", 339,
                  typeid(vector<float>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEfloatgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<float>) );
      instance.SetNew(&new_vectorlEfloatgR);
      instance.SetNewArray(&newArray_vectorlEfloatgR);
      instance.SetDelete(&delete_vectorlEfloatgR);
      instance.SetDeleteArray(&deleteArray_vectorlEfloatgR);
      instance.SetDestructor(&destruct_vectorlEfloatgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<float> >()));

      ::ROOT::AddClassAlternate("vector<float>","std::vector<float, std::allocator<float> >");
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<float>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEfloatgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<float>*)0x0)->GetClass();
      vectorlEfloatgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEfloatgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEfloatgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<float> : new vector<float>;
   }
   static void *newArray_vectorlEfloatgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<float>[nElements] : new vector<float>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEfloatgR(void *p) {
      delete ((vector<float>*)p);
   }
   static void deleteArray_vectorlEfloatgR(void *p) {
      delete [] ((vector<float>*)p);
   }
   static void destruct_vectorlEfloatgR(void *p) {
      typedef vector<float> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<float>

namespace ROOT {
   static TClass *vectorlEfebex_datagR_Dictionary();
   static void vectorlEfebex_datagR_TClassManip(TClass*);
   static void *new_vectorlEfebex_datagR(void *p = 0);
   static void *newArray_vectorlEfebex_datagR(Long_t size, void *p);
   static void delete_vectorlEfebex_datagR(void *p);
   static void deleteArray_vectorlEfebex_datagR(void *p);
   static void destruct_vectorlEfebex_datagR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<febex_data>*)
   {
      vector<febex_data> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<febex_data>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<febex_data>", -2, "vector", 339,
                  typeid(vector<febex_data>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEfebex_datagR_Dictionary, isa_proxy, 0,
                  sizeof(vector<febex_data>) );
      instance.SetNew(&new_vectorlEfebex_datagR);
      instance.SetNewArray(&newArray_vectorlEfebex_datagR);
      instance.SetDelete(&delete_vectorlEfebex_datagR);
      instance.SetDeleteArray(&deleteArray_vectorlEfebex_datagR);
      instance.SetDestructor(&destruct_vectorlEfebex_datagR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<febex_data> >()));

      ::ROOT::AddClassAlternate("vector<febex_data>","std::vector<febex_data, std::allocator<febex_data> >");
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<febex_data>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEfebex_datagR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<febex_data>*)0x0)->GetClass();
      vectorlEfebex_datagR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEfebex_datagR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEfebex_datagR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<febex_data> : new vector<febex_data>;
   }
   static void *newArray_vectorlEfebex_datagR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<febex_data>[nElements] : new vector<febex_data>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEfebex_datagR(void *p) {
      delete ((vector<febex_data>*)p);
   }
   static void deleteArray_vectorlEfebex_datagR(void *p) {
      delete [] ((vector<febex_data>*)p);
   }
   static void destruct_vectorlEfebex_datagR(void *p) {
      typedef vector<febex_data> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<febex_data>

namespace ROOT {
   static TClass *vectorlEdoublegR_Dictionary();
   static void vectorlEdoublegR_TClassManip(TClass*);
   static void *new_vectorlEdoublegR(void *p = 0);
   static void *newArray_vectorlEdoublegR(Long_t size, void *p);
   static void delete_vectorlEdoublegR(void *p);
   static void deleteArray_vectorlEdoublegR(void *p);
   static void destruct_vectorlEdoublegR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<double>*)
   {
      vector<double> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<double>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<double>", -2, "vector", 339,
                  typeid(vector<double>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEdoublegR_Dictionary, isa_proxy, 0,
                  sizeof(vector<double>) );
      instance.SetNew(&new_vectorlEdoublegR);
      instance.SetNewArray(&newArray_vectorlEdoublegR);
      instance.SetDelete(&delete_vectorlEdoublegR);
      instance.SetDeleteArray(&deleteArray_vectorlEdoublegR);
      instance.SetDestructor(&destruct_vectorlEdoublegR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<double> >()));

      ::ROOT::AddClassAlternate("vector<double>","std::vector<double, std::allocator<double> >");
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<double>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEdoublegR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<double>*)0x0)->GetClass();
      vectorlEdoublegR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEdoublegR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEdoublegR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<double> : new vector<double>;
   }
   static void *newArray_vectorlEdoublegR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<double>[nElements] : new vector<double>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEdoublegR(void *p) {
      delete ((vector<double>*)p);
   }
   static void deleteArray_vectorlEdoublegR(void *p) {
      delete [] ((vector<double>*)p);
   }
   static void destruct_vectorlEdoublegR(void *p) {
      typedef vector<double> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<double>

namespace {
  void TriggerDictionaryInitialization_dict_Impl() {
    static const char* headers[] = {
"inc/dict.hh",
0
    };
    static const char* includePaths[] = {
"/cvmfs/eel.gsi.de/debian10-x86_64/root/622-08/include/",
"/u/zchen/Pareeksha_ana/code/backup/anatraces2_24.07.03/",
0
    };
    static const char* fwdDeclCode = R"DICTFWDDCLS(
#line 1 "dict dictionary forward declarations' payload"
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_AutoLoading_Map;
class __attribute__((annotate("$clingAutoload$inc/dict.hh")))  lisa_ucesb;
class __attribute__((annotate("$clingAutoload$inc/dict.hh")))  lisa_settings;
class __attribute__((annotate("$clingAutoload$inc/dict.hh")))  lisa_febex;
class __attribute__((annotate("$clingAutoload$inc/dict.hh")))  lisa_optManager;
class __attribute__((annotate("$clingAutoload$inc/dict.hh")))  lisa_scope;
class __attribute__((annotate("$clingAutoload$inc/dict.hh")))  lisa_vis;
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(
#line 1 "dict dictionary payload"


#define _BACKWARD_BACKWARD_WARNING_H
// Inline headers
#include "inc/dict.hh"

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[] = {
"lisa_febex", payloadCode, "@",
"lisa_optManager", payloadCode, "@",
"lisa_scope", payloadCode, "@",
"lisa_settings", payloadCode, "@",
"lisa_ucesb", payloadCode, "@",
"lisa_vis", payloadCode, "@",
nullptr
};
    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("dict",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_dict_Impl, {}, classesHeaders, /*hasCxxModule*/false);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_dict_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_dict() {
  TriggerDictionaryInitialization_dict_Impl();
}
