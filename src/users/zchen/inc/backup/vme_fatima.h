//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Wed Jun  5 18:35:59 2024 by ROOT version 6.26/10
// from TTree evt//cbmout_0
// found on file: Eu152_10kbq_0003_0001_ts_tree.root
//////////////////////////////////////////////////////////

#ifndef vme_fatima_h
#define vme_fatima_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "EventHeader.h"
#include "TClonesArray.h"
#include "TObject.h"
#include "FatimaTwinpeaksCalData.h"
#include "TimeMachineData.h"
#include "FatimaVmeCalData.h"
#include "GermaniumCalData.h"

class vme_fatima {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.
   static constexpr Int_t kMaxcbmout_0_Event_EventHeader = 1;
   static constexpr Int_t kMaxcbmout_0_FatimaTwinpeaksCalDataFolder_FatimaTwinpeaksCalData = 24;
   static constexpr Int_t kMaxcbmout_0_FatimaTimeMachineDataFolder_FatimaTimeMachineData = 8;
   static constexpr Int_t kMaxcbmout_0_FatimaVmeCalDataFolder_FatimaVmeCalData = 1;
   static constexpr Int_t kMaxcbmout_0_FatimaVmeTimeMachineDataFolder_FatimaVmeTimeMachineData = 8;
   static constexpr Int_t kMaxcbmout_0_GermaniumCalData_GermaniumCalData = 1;
   static constexpr Int_t kMaxcbmout_0_Time Machine Data_GermaniumTimeMachineData = 1;

   // Declaration of leaf types
   EventHeader     *EventHeader_;
   EventHeader     *EventHeader;
   ULong_t         EventHeader_fEventno;
   Int_t           EventHeader_fTrigger;
   ULong_t         EventHeader_fTimeStamp;
   Bool_t          EventHeader_fSpillFlag;

   Int_t           FatimaTwinpeaksCalData_;
   UInt_t          FatimaTwinpeaksCalData_fUniqueID[kMaxcbmout_0_FatimaTwinpeaksCalDataFolder_FatimaTwinpeaksCalData];   //[cbmout_0.FatimaTwinpeaksCalDataFolder.FatimaTwinpeaksCalData_]
   UInt_t          FatimaTwinpeaksCalData_fBits[kMaxcbmout_0_FatimaTwinpeaksCalDataFolder_FatimaTwinpeaksCalData];   //[cbmout_0.FatimaTwinpeaksCalDataFolder.FatimaTwinpeaksCalData_]
   UShort_t        FatimaTwinpeaksCalData_fboard_id[kMaxcbmout_0_FatimaTwinpeaksCalDataFolder_FatimaTwinpeaksCalData];   //[cbmout_0.FatimaTwinpeaksCalDataFolder.FatimaTwinpeaksCalData_]
   UShort_t        FatimaTwinpeaksCalData_fch_ID[kMaxcbmout_0_FatimaTwinpeaksCalDataFolder_FatimaTwinpeaksCalData];   //[cbmout_0.FatimaTwinpeaksCalDataFolder.FatimaTwinpeaksCalData_]
   UShort_t        FatimaTwinpeaksCalData_fdetector_id[kMaxcbmout_0_FatimaTwinpeaksCalDataFolder_FatimaTwinpeaksCalData];   //[cbmout_0.FatimaTwinpeaksCalDataFolder.FatimaTwinpeaksCalData_]
   Double_t        FatimaTwinpeaksCalData_fslow_lead_time[kMaxcbmout_0_FatimaTwinpeaksCalDataFolder_FatimaTwinpeaksCalData];   //[cbmout_0.FatimaTwinpeaksCalDataFolder.FatimaTwinpeaksCalData_]
   Double_t        FatimaTwinpeaksCalData_fslow_trail_time[kMaxcbmout_0_FatimaTwinpeaksCalDataFolder_FatimaTwinpeaksCalData];   //[cbmout_0.FatimaTwinpeaksCalDataFolder.FatimaTwinpeaksCalData_]
   Double_t        FatimaTwinpeaksCalData_ffast_lead_time[kMaxcbmout_0_FatimaTwinpeaksCalDataFolder_FatimaTwinpeaksCalData];   //[cbmout_0.FatimaTwinpeaksCalDataFolder.FatimaTwinpeaksCalData_]
   Double_t        FatimaTwinpeaksCalData_ffast_trail_time[kMaxcbmout_0_FatimaTwinpeaksCalDataFolder_FatimaTwinpeaksCalData];   //[cbmout_0.FatimaTwinpeaksCalDataFolder.FatimaTwinpeaksCalData_]
   Double_t        FatimaTwinpeaksCalData_ffast_ToT[kMaxcbmout_0_FatimaTwinpeaksCalDataFolder_FatimaTwinpeaksCalData];   //[cbmout_0.FatimaTwinpeaksCalDataFolder.FatimaTwinpeaksCalData_]
   Double_t        FatimaTwinpeaksCalData_fslow_ToT[kMaxcbmout_0_FatimaTwinpeaksCalDataFolder_FatimaTwinpeaksCalData];   //[cbmout_0.FatimaTwinpeaksCalDataFolder.FatimaTwinpeaksCalData_]
   Double_t        FatimaTwinpeaksCalData_fenergy[kMaxcbmout_0_FatimaTwinpeaksCalDataFolder_FatimaTwinpeaksCalData];   //[cbmout_0.FatimaTwinpeaksCalDataFolder.FatimaTwinpeaksCalData_]
   UShort_t        FatimaTwinpeaksCalData_fwr_subsystem_id[kMaxcbmout_0_FatimaTwinpeaksCalDataFolder_FatimaTwinpeaksCalData];   //[cbmout_0.FatimaTwinpeaksCalDataFolder.FatimaTwinpeaksCalData_]
   ULong_t         FatimaTwinpeaksCalData_fwr_t[kMaxcbmout_0_FatimaTwinpeaksCalDataFolder_FatimaTwinpeaksCalData];   //[cbmout_0.FatimaTwinpeaksCalDataFolder.FatimaTwinpeaksCalData_]
   Long_t          FatimaTwinpeaksCalData_fabsolute_event_time[kMaxcbmout_0_FatimaTwinpeaksCalDataFolder_FatimaTwinpeaksCalData];   //[cbmout_0.FatimaTwinpeaksCalDataFolder.FatimaTwinpeaksCalData_]

   Int_t           FatimaTimeMachineData_;
   UInt_t          FatimaTimeMachineData_fUniqueID[kMaxcbmout_0_FatimaTimeMachineDataFolder_FatimaTimeMachineData];   //[cbmout_0.FatimaTimeMachineDataFolder.FatimaTimeMachineData_]
   UInt_t          FatimaTimeMachineData_fBits[kMaxcbmout_0_FatimaTimeMachineDataFolder_FatimaTimeMachineData];   //[cbmout_0.FatimaTimeMachineDataFolder.FatimaTimeMachineData_]
   Double_t        FatimaTimeMachineData_fundelayed_time[kMaxcbmout_0_FatimaTimeMachineDataFolder_FatimaTimeMachineData];   //[cbmout_0.FatimaTimeMachineDataFolder.FatimaTimeMachineData_]
   Double_t        FatimaTimeMachineData_fdelayed_time[kMaxcbmout_0_FatimaTimeMachineDataFolder_FatimaTimeMachineData];   //[cbmout_0.FatimaTimeMachineDataFolder.FatimaTimeMachineData_]
   UInt_t          FatimaTimeMachineData_fwr_subsystem_id[kMaxcbmout_0_FatimaTimeMachineDataFolder_FatimaTimeMachineData];   //[cbmout_0.FatimaTimeMachineDataFolder.FatimaTimeMachineData_]
   ULong_t         FatimaTimeMachineData_fwr_t[kMaxcbmout_0_FatimaTimeMachineDataFolder_FatimaTimeMachineData];   //[cbmout_0.FatimaTimeMachineDataFolder.FatimaTimeMachineData_]

   Int_t           FatimaVmeCalData_;
   UInt_t          FatimaVmeCalData_fUniqueID[kMaxcbmout_0_FatimaVmeCalDataFolder_FatimaVmeCalData];   //[cbmout_0.FatimaVmeCalDataFolder.FatimaVmeCalData_]
   UInt_t          FatimaVmeCalData_fBits[kMaxcbmout_0_FatimaVmeCalDataFolder_FatimaVmeCalData];   //[cbmout_0.FatimaVmeCalDataFolder.FatimaVmeCalData_]
   ULong_t         FatimaVmeCalData_fwr_t[kMaxcbmout_0_FatimaVmeCalDataFolder_FatimaVmeCalData];   //[cbmout_0.FatimaVmeCalDataFolder.FatimaVmeCalData_]
   vector<unsigned int> FatimaVmeCalData_fsingles_e[kMaxcbmout_0_FatimaVmeCalDataFolder_FatimaVmeCalData];
   vector<unsigned int> FatimaVmeCalData_fsingles_e_raw[kMaxcbmout_0_FatimaVmeCalDataFolder_FatimaVmeCalData];
   vector<unsigned int> FatimaVmeCalData_fsingles_qdc_id[kMaxcbmout_0_FatimaVmeCalDataFolder_FatimaVmeCalData];
   vector<unsigned int> FatimaVmeCalData_fsingles_coarse_time[kMaxcbmout_0_FatimaVmeCalDataFolder_FatimaVmeCalData];
   vector<unsigned long> FatimaVmeCalData_fsingles_fine_time[kMaxcbmout_0_FatimaVmeCalDataFolder_FatimaVmeCalData];
   vector<unsigned int> FatimaVmeCalData_fsingles_tdc_timestamp[kMaxcbmout_0_FatimaVmeCalDataFolder_FatimaVmeCalData];
   vector<unsigned int> FatimaVmeCalData_fsingles_tdc_timestamp_raw[kMaxcbmout_0_FatimaVmeCalDataFolder_FatimaVmeCalData];
   vector<unsigned int> FatimaVmeCalData_fsingles_tdc_id[kMaxcbmout_0_FatimaVmeCalDataFolder_FatimaVmeCalData];
   vector<unsigned int> FatimaVmeCalData_fsc41l_hits[kMaxcbmout_0_FatimaVmeCalDataFolder_FatimaVmeCalData];
   vector<unsigned int> FatimaVmeCalData_fsc41r_hits[kMaxcbmout_0_FatimaVmeCalDataFolder_FatimaVmeCalData];
   vector<unsigned int> FatimaVmeCalData_ftm_undelayed_hits[kMaxcbmout_0_FatimaVmeCalDataFolder_FatimaVmeCalData];
   vector<unsigned int> FatimaVmeCalData_ftm_delayed_hits[kMaxcbmout_0_FatimaVmeCalDataFolder_FatimaVmeCalData];
   vector<unsigned int> FatimaVmeCalData_fsc41l_e_hits[kMaxcbmout_0_FatimaVmeCalDataFolder_FatimaVmeCalData];
   vector<unsigned int> FatimaVmeCalData_fsc41r_e_hits[kMaxcbmout_0_FatimaVmeCalDataFolder_FatimaVmeCalData];
   vector<unsigned int> FatimaVmeCalData_ftm_undelayed_e_hits[kMaxcbmout_0_FatimaVmeCalDataFolder_FatimaVmeCalData];
   vector<unsigned int> FatimaVmeCalData_ftm_delayed_e_hits[kMaxcbmout_0_FatimaVmeCalDataFolder_FatimaVmeCalData];
   Int_t           FatimaVmeCalData_ffatvme_mult[kMaxcbmout_0_FatimaVmeCalDataFolder_FatimaVmeCalData];   //[cbmout_0.FatimaVmeCalDataFolder.FatimaVmeCalData_]
   vector<unsigned int> FatimaVmeCalData_fqdc_id[kMaxcbmout_0_FatimaVmeCalDataFolder_FatimaVmeCalData];
   vector<unsigned int> FatimaVmeCalData_fqdc_e[kMaxcbmout_0_FatimaVmeCalDataFolder_FatimaVmeCalData];
   vector<unsigned int> FatimaVmeCalData_fqdc_e_raw[kMaxcbmout_0_FatimaVmeCalDataFolder_FatimaVmeCalData];
   vector<unsigned int> FatimaVmeCalData_fqdc_t_coarse[kMaxcbmout_0_FatimaVmeCalDataFolder_FatimaVmeCalData];
   vector<unsigned long> FatimaVmeCalData_fqdc_t_fine[kMaxcbmout_0_FatimaVmeCalDataFolder_FatimaVmeCalData];
   vector<unsigned int> FatimaVmeCalData_ftdc_id[kMaxcbmout_0_FatimaVmeCalDataFolder_FatimaVmeCalData];
   vector<unsigned int> FatimaVmeCalData_ftdc_time[kMaxcbmout_0_FatimaVmeCalDataFolder_FatimaVmeCalData];
   vector<unsigned int> FatimaVmeCalData_ftdc_time_raw[kMaxcbmout_0_FatimaVmeCalDataFolder_FatimaVmeCalData];

   Int_t           FatimaVmeTimeMachineData_;
   UInt_t          FatimaVmeTimeMachineData_fUniqueID[kMaxcbmout_0_FatimaVmeTimeMachineDataFolder_FatimaVmeTimeMachineData];   //[cbmout_0.FatimaVmeTimeMachineDataFolder.FatimaVmeTimeMachineData_]
   UInt_t          FatimaVmeTimeMachineData_fBits[kMaxcbmout_0_FatimaVmeTimeMachineDataFolder_FatimaVmeTimeMachineData];   //[cbmout_0.FatimaVmeTimeMachineDataFolder.FatimaVmeTimeMachineData_]
   Double_t        FatimaVmeTimeMachineData_fundelayed_time[kMaxcbmout_0_FatimaVmeTimeMachineDataFolder_FatimaVmeTimeMachineData];   //[cbmout_0.FatimaVmeTimeMachineDataFolder.FatimaVmeTimeMachineData_]
   Double_t        FatimaVmeTimeMachineData_fdelayed_time[kMaxcbmout_0_FatimaVmeTimeMachineDataFolder_FatimaVmeTimeMachineData];   //[cbmout_0.FatimaVmeTimeMachineDataFolder.FatimaVmeTimeMachineData_]
   UInt_t          FatimaVmeTimeMachineData_fwr_subsystem_id[kMaxcbmout_0_FatimaVmeTimeMachineDataFolder_FatimaVmeTimeMachineData];   //[cbmout_0.FatimaVmeTimeMachineDataFolder.FatimaVmeTimeMachineData_]
   ULong_t         FatimaVmeTimeMachineData_fwr_t[kMaxcbmout_0_FatimaVmeTimeMachineDataFolder_FatimaVmeTimeMachineData];   //[cbmout_0.FatimaVmeTimeMachineDataFolder.FatimaVmeTimeMachineData_]
   Int_t           GermaniumCalData_;
   UInt_t          GermaniumCalData_fUniqueID[kMaxcbmout_0_GermaniumCalData_GermaniumCalData];   //[cbmout_0.Germanium Cal Data.GermaniumCalData_]
   UInt_t          GermaniumCalData_fBits[kMaxcbmout_0_GermaniumCalData_GermaniumCalData];   //[cbmout_0.Germanium Cal Data.GermaniumCalData_]
   ULong_t         GermaniumCalData_fevent_trigger_time[kMaxcbmout_0_GermaniumCalData_GermaniumCalData];   //[cbmout_0.Germanium Cal Data.GermaniumCalData_]
   UChar_t         GermaniumCalData_fpileup[kMaxcbmout_0_GermaniumCalData_GermaniumCalData];   //[cbmout_0.Germanium Cal Data.GermaniumCalData_]
   UChar_t         GermaniumCalData_foverflow[kMaxcbmout_0_GermaniumCalData_GermaniumCalData];   //[cbmout_0.Germanium Cal Data.GermaniumCalData_]
   ULong_t         GermaniumCalData_fchannel_trigger_time[kMaxcbmout_0_GermaniumCalData_GermaniumCalData];   //[cbmout_0.Germanium Cal Data.GermaniumCalData_]
   Double_t        GermaniumCalData_fchannel_energy[kMaxcbmout_0_GermaniumCalData_GermaniumCalData];   //[cbmout_0.Germanium Cal Data.GermaniumCalData_]
   UInt_t          GermaniumCalData_fcrystal_id[kMaxcbmout_0_GermaniumCalData_GermaniumCalData];   //[cbmout_0.Germanium Cal Data.GermaniumCalData_]
   UInt_t          GermaniumCalData_fdetector_id[kMaxcbmout_0_Germanium Cal Data_GermaniumCalData];   //[cbmout_0.Germanium Cal Data.GermaniumCalData_]
   UInt_t          GermaniumCalData_fwr_subsystem_id[kMaxcbmout_0_Germanium Cal Data_GermaniumCalData];   //[cbmout_0.Germanium Cal Data.GermaniumCalData_]
   ULong_t         GermaniumCalData_fwr_t[kMaxcbmout_0_Germanium Cal Data_GermaniumCalData];   //[cbmout_0.Germanium Cal Data.GermaniumCalData_]
   Long_t          GermaniumCalData_fabsolute_event_time[kMaxcbmout_0_Germanium Cal Data_GermaniumCalData];   //[cbmout_0.Germanium Cal Data.GermaniumCalData_]
   Int_t           GermaniumTimeMachineData_;
   UInt_t          GermaniumTimeMachineData_fUniqueID[kMaxcbmout_0_Time Machine Data_GermaniumTimeMachineData];   //[cbmout_0.Time Machine Data.GermaniumTimeMachineData_]
   UInt_t          GermaniumTimeMachineData_fBits[kMaxcbmout_0_Time Machine Data_GermaniumTimeMachineData];   //[cbmout_0.Time Machine Data.GermaniumTimeMachineData_]
   Double_t        GermaniumTimeMachineData_fundelayed_time[kMaxcbmout_0_Time Machine Data_GermaniumTimeMachineData];   //[cbmout_0.Time Machine Data.GermaniumTimeMachineData_]
   Double_t        GermaniumTimeMachineData_fdelayed_time[kMaxcbmout_0_Time Machine Data_GermaniumTimeMachineData];   //[cbmout_0.Time Machine Data.GermaniumTimeMachineData_]
   UInt_t          GermaniumTimeMachineData_fwr_subsystem_id[kMaxcbmout_0_Time Machine Data_GermaniumTimeMachineData];   //[cbmout_0.Time Machine Data.GermaniumTimeMachineData_]
   ULong_t         GermaniumTimeMachineData_fwr_t[kMaxcbmout_0_Time Machine Data_GermaniumTimeMachineData];   //[cbmout_0.Time Machine Data.GermaniumTimeMachineData_]

   // List of branches
   TBranch        *b_cbmout_0_Event_EventHeader_;   //!
   TBranch        *b_EventHeader_cbmout_0_Event_EventHeader_FairEventHeader;   //!
   TBranch        *b_EventHeader_cbmout_0_Event_EventHeader_fEventno;   //!
   TBranch        *b_EventHeader_cbmout_0_Event_EventHeader_fTrigger;   //!
   TBranch        *b_EventHeader_cbmout_0_Event_EventHeader_fTimeStamp;   //!
   TBranch        *b_EventHeader_cbmout_0_Event_EventHeader_fSpillFlag;   //!
   TBranch        *b_cbmout_0_FatimaTwinpeaksCalDataFolder_FatimaTwinpeaksCalData_;   //!
   TBranch        *b_FatimaTwinpeaksCalData_fUniqueID;   //!
   TBranch        *b_FatimaTwinpeaksCalData_fBits;   //!
   TBranch        *b_FatimaTwinpeaksCalData_fboard_id;   //!
   TBranch        *b_FatimaTwinpeaksCalData_fch_ID;   //!
   TBranch        *b_FatimaTwinpeaksCalData_fdetector_id;   //!
   TBranch        *b_FatimaTwinpeaksCalData_fslow_lead_time;   //!
   TBranch        *b_FatimaTwinpeaksCalData_fslow_trail_time;   //!
   TBranch        *b_FatimaTwinpeaksCalData_ffast_lead_time;   //!
   TBranch        *b_FatimaTwinpeaksCalData_ffast_trail_time;   //!
   TBranch        *b_FatimaTwinpeaksCalData_ffast_ToT;   //!
   TBranch        *b_FatimaTwinpeaksCalData_fslow_ToT;   //!
   TBranch        *b_FatimaTwinpeaksCalData_fenergy;   //!
   TBranch        *b_FatimaTwinpeaksCalData_fwr_subsystem_id;   //!
   TBranch        *b_FatimaTwinpeaksCalData_fwr_t;   //!
   TBranch        *b_FatimaTwinpeaksCalData_fabsolute_event_time;   //!

   TBranch        *b_cbmout_0_FatimaTimeMachineDataFolder_FatimaTimeMachineData_;   //!
   TBranch        *b_FatimaTimeMachineData_fUniqueID;   //!
   TBranch        *b_FatimaTimeMachineData_fBits;   //!
   TBranch        *b_FatimaTimeMachineData_fundelayed_time;   //!
   TBranch        *b_FatimaTimeMachineData_fdelayed_time;   //!
   TBranch        *b_FatimaTimeMachineData_fwr_subsystem_id;   //!
   TBranch        *b_FatimaTimeMachineData_fwr_t;   //!

   TBranch        *b_cbmout_0_FatimaVmeCalDataFolder_FatimaVmeCalData_;   //!
   TBranch        *b_FatimaVmeCalData_fUniqueID;   //!
   TBranch        *b_FatimaVmeCalData_fBits;   //!
   TBranch        *b_FatimaVmeCalData_fwr_t;   //!
   TBranch        *b_FatimaVmeCalData_fsingles_e;   //!
   TBranch        *b_FatimaVmeCalData_fsingles_e_raw;   //!
   TBranch        *b_FatimaVmeCalData_fsingles_qdc_id;   //!
   TBranch        *b_FatimaVmeCalData_fsingles_coarse_time;   //!
   TBranch        *b_FatimaVmeCalData_fsingles_fine_time;   //!
   TBranch        *b_FatimaVmeCalData_fsingles_tdc_timestamp;   //!
   TBranch        *b_FatimaVmeCalData_fsingles_tdc_timestamp_raw;   //!
   TBranch        *b_FatimaVmeCalData_fsingles_tdc_id;   //!
   TBranch        *b_FatimaVmeCalData_fsc41l_hits;   //!
   TBranch        *b_FatimaVmeCalData_fsc41r_hits;   //!
   TBranch        *b_FatimaVmeCalData_ftm_undelayed_hits;   //!
   TBranch        *b_FatimaVmeCalData_ftm_delayed_hits;   //!
   TBranch        *b_FatimaVmeCalData_fsc41l_e_hits;   //!
   TBranch        *b_FatimaVmeCalData_fsc41r_e_hits;   //!
   TBranch        *b_FatimaVmeCalData_ftm_undelayed_e_hits;   //!
   TBranch        *b_FatimaVmeCalData_ftm_delayed_e_hits;   //!
   TBranch        *b_FatimaVmeCalData_ffatvme_mult;   //!
   TBranch        *b_FatimaVmeCalData_fqdc_id;   //!
   TBranch        *b_FatimaVmeCalData_fqdc_e;   //!
   TBranch        *b_FatimaVmeCalData_fqdc_e_raw;   //!
   TBranch        *b_FatimaVmeCalData_fqdc_t_coarse;   //!
   TBranch        *b_FatimaVmeCalData_fqdc_t_fine;   //!
   TBranch        *b_FatimaVmeCalData_ftdc_id;   //!
   TBranch        *b_FatimaVmeCalData_ftdc_time;   //!
   TBranch        *b_FatimaVmeCalData_ftdc_time_raw;   //!

   TBranch        *b_cbmout_0_FatimaVmeTimeMachineDataFolder_FatimaVmeTimeMachineData_;   //!
   TBranch        *b_FatimaVmeTimeMachineData_fUniqueID;   //!
   TBranch        *b_FatimaVmeTimeMachineData_fBits;   //!
   TBranch        *b_FatimaVmeTimeMachineData_fundelayed_time;   //!
   TBranch        *b_FatimaVmeTimeMachineData_fdelayed_time;   //!
   TBranch        *b_FatimaVmeTimeMachineData_fwr_subsystem_id;   //!
   TBranch        *b_FatimaVmeTimeMachineData_fwr_t;   //!

   TBranch        *b_cbmout_0_Germanium Cal Data_GermaniumCalData_;   //!
   TBranch        *b_GermaniumCalData_fUniqueID;   //!
   TBranch        *b_GermaniumCalData_fBits;   //!
   TBranch        *b_GermaniumCalData_fevent_trigger_time;   //!
   TBranch        *b_GermaniumCalData_fpileup;   //!
   TBranch        *b_GermaniumCalData_foverflow;   //!
   TBranch        *b_GermaniumCalData_fchannel_trigger_time;   //!
   TBranch        *b_GermaniumCalData_fchannel_energy;   //!
   TBranch        *b_GermaniumCalData_fcrystal_id;   //!
   TBranch        *b_GermaniumCalData_fdetector_id;   //!
   TBranch        *b_GermaniumCalData_fwr_subsystem_id;   //!
   TBranch        *b_GermaniumCalData_fwr_t;   //!
   TBranch        *b_GermaniumCalData_fabsolute_event_time;   //!
   TBranch        *b_cbmout_0_Time Machine Data_GermaniumTimeMachineData_;   //!
   TBranch        *b_GermaniumTimeMachineData_fUniqueID;   //!
   TBranch        *b_GermaniumTimeMachineData_fBits;   //!
   TBranch        *b_GermaniumTimeMachineData_fundelayed_time;   //!
   TBranch        *b_GermaniumTimeMachineData_fdelayed_time;   //!
   TBranch        *b_GermaniumTimeMachineData_fwr_subsystem_id;   //!
   TBranch        *b_GermaniumTimeMachineData_fwr_t;   //!

   vme_fatima(TTree *tree=0);
   virtual ~vme_fatima();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef vme_fatima_cxx
vme_fatima::vme_fatima(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("Eu152_10kbq_0003_0001_ts_tree.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("Eu152_10kbq_0003_0001_ts_tree.root");
      }
      f->GetObject("evt",tree);

   }
   Init(tree);
}

vme_fatima::~vme_fatima()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t vme_fatima::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t vme_fatima::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void vme_fatima::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   EventHeader_ = 0;
   EventHeader = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("EventHeader.", &EventHeader_, &b_cbmout_0_Event_EventHeader_);
   fChain->SetBranchAddress("EventHeader", &EventHeader, &b_EventHeader_cbmout_0_Event_EventHeader_FairEventHeader);
   fChain->SetBranchAddress("EventHeader.fEventno", &EventHeader_fEventno, &b_EventHeader_cbmout_0_Event_EventHeader_fEventno);
   fChain->SetBranchAddress("EventHeader.fTrigger", &EventHeader_fTrigger, &b_EventHeader_cbmout_0_Event_EventHeader_fTrigger);
   fChain->SetBranchAddress("EventHeader.fTimeStamp", &EventHeader_fTimeStamp, &b_EventHeader_cbmout_0_Event_EventHeader_fTimeStamp);
   fChain->SetBranchAddress("EventHeader.fSpillFlag", &EventHeader_fSpillFlag, &b_EventHeader_cbmout_0_Event_EventHeader_fSpillFlag);

   fChain->SetBranchAddress("FatimaTwinpeaksCalData", &FatimaTwinpeaksCalData_, &b_cbmout_0_FatimaTwinpeaksCalDataFolder_FatimaTwinpeaksCalData_);
   fChain->SetBranchAddress("FatimaTwinpeaksCalData.fUniqueID", FatimaTwinpeaksCalData_fUniqueID, &b_FatimaTwinpeaksCalData_fUniqueID);
   fChain->SetBranchAddress("FatimaTwinpeaksCalData.fBits", FatimaTwinpeaksCalData_fBits, &b_FatimaTwinpeaksCalData_fBits);
   fChain->SetBranchAddress("FatimaTwinpeaksCalData.fboard_id", FatimaTwinpeaksCalData_fboard_id, &b_FatimaTwinpeaksCalData_fboard_id);
   fChain->SetBranchAddress("FatimaTwinpeaksCalData.fch_ID", FatimaTwinpeaksCalData_fch_ID, &b_FatimaTwinpeaksCalData_fch_ID);
   fChain->SetBranchAddress("FatimaTwinpeaksCalData.fdetector_id", FatimaTwinpeaksCalData_fdetector_id, &b_FatimaTwinpeaksCalData_fdetector_id);
   fChain->SetBranchAddress("FatimaTwinpeaksCalData.fslow_lead_time", FatimaTwinpeaksCalData_fslow_lead_time, &b_FatimaTwinpeaksCalData_fslow_lead_time);
   fChain->SetBranchAddress("FatimaTwinpeaksCalData.fslow_trail_time", FatimaTwinpeaksCalData_fslow_trail_time, &b_FatimaTwinpeaksCalData_fslow_trail_time);
   fChain->SetBranchAddress("FatimaTwinpeaksCalData.ffast_lead_time", FatimaTwinpeaksCalData_ffast_lead_time, &b_FatimaTwinpeaksCalData_ffast_lead_time);
   fChain->SetBranchAddress("FatimaTwinpeaksCalData.ffast_trail_time", FatimaTwinpeaksCalData_ffast_trail_time, &b_FatimaTwinpeaksCalData_ffast_trail_time);
   fChain->SetBranchAddress("FatimaTwinpeaksCalData.ffast_ToT", FatimaTwinpeaksCalData_ffast_ToT, &b_FatimaTwinpeaksCalData_ffast_ToT);
   fChain->SetBranchAddress("FatimaTwinpeaksCalData.fslow_ToT", FatimaTwinpeaksCalData_fslow_ToT, &b_FatimaTwinpeaksCalData_fslow_ToT);
   fChain->SetBranchAddress("FatimaTwinpeaksCalData.fenergy", FatimaTwinpeaksCalData_fenergy, &b_FatimaTwinpeaksCalData_fenergy);
   fChain->SetBranchAddress("FatimaTwinpeaksCalData.fwr_subsystem_id", FatimaTwinpeaksCalData_fwr_subsystem_id, &b_FatimaTwinpeaksCalData_fwr_subsystem_id);
   fChain->SetBranchAddress("FatimaTwinpeaksCalData.fwr_t", FatimaTwinpeaksCalData_fwr_t, &b_FatimaTwinpeaksCalData_fwr_t);
   fChain->SetBranchAddress("FatimaTwinpeaksCalData.fabsolute_event_time", FatimaTwinpeaksCalData_fabsolute_event_time, &b_FatimaTwinpeaksCalData_fabsolute_event_time);

   fChain->SetBranchAddress("FatimaTimeMachineData", &FatimaTimeMachineData_, &b_cbmout_0_FatimaTimeMachineDataFolder_FatimaTimeMachineData_);
   fChain->SetBranchAddress("FatimaTimeMachineData.fUniqueID", FatimaTimeMachineData_fUniqueID, &b_FatimaTimeMachineData_fUniqueID);
   fChain->SetBranchAddress("FatimaTimeMachineData.fBits", FatimaTimeMachineData_fBits, &b_FatimaTimeMachineData_fBits);
   fChain->SetBranchAddress("FatimaTimeMachineData.fundelayed_time", FatimaTimeMachineData_fundelayed_time, &b_FatimaTimeMachineData_fundelayed_time);
   fChain->SetBranchAddress("FatimaTimeMachineData.fdelayed_time", FatimaTimeMachineData_fdelayed_time, &b_FatimaTimeMachineData_fdelayed_time);
   fChain->SetBranchAddress("FatimaTimeMachineData.fwr_subsystem_id", FatimaTimeMachineData_fwr_subsystem_id, &b_FatimaTimeMachineData_fwr_subsystem_id);
   fChain->SetBranchAddress("FatimaTimeMachineData.fwr_t", FatimaTimeMachineData_fwr_t, &b_FatimaTimeMachineData_fwr_t);

   fChain->SetBranchAddress("FatimaVmeCalData", &FatimaVmeCalData_, &b_cbmout_0_FatimaVmeCalDataFolder_FatimaVmeCalData_);
   fChain->SetBranchAddress("FatimaVmeCalData.fUniqueID", FatimaVmeCalData_fUniqueID, &b_FatimaVmeCalData_fUniqueID);
   fChain->SetBranchAddress("FatimaVmeCalData.fBits", FatimaVmeCalData_fBits, &b_FatimaVmeCalData_fBits);
   fChain->SetBranchAddress("FatimaVmeCalData.fwr_t", FatimaVmeCalData_fwr_t, &b_FatimaVmeCalData_fwr_t);
   fChain->SetBranchAddress("FatimaVmeCalData.fsingles_e", FatimaVmeCalData_fsingles_e, &b_FatimaVmeCalData_fsingles_e);
   fChain->SetBranchAddress("FatimaVmeCalData.fsingles_e_raw", FatimaVmeCalData_fsingles_e_raw, &b_FatimaVmeCalData_fsingles_e_raw);
   fChain->SetBranchAddress("FatimaVmeCalData.fsingles_qdc_id", FatimaVmeCalData_fsingles_qdc_id, &b_FatimaVmeCalData_fsingles_qdc_id);
   fChain->SetBranchAddress("FatimaVmeCalData.fsingles_coarse_time", FatimaVmeCalData_fsingles_coarse_time, &b_FatimaVmeCalData_fsingles_coarse_time);
   fChain->SetBranchAddress("FatimaVmeCalData.fsingles_fine_time", FatimaVmeCalData_fsingles_fine_time, &b_FatimaVmeCalData_fsingles_fine_time);
   fChain->SetBranchAddress("FatimaVmeCalData.fsingles_tdc_timestamp", FatimaVmeCalData_fsingles_tdc_timestamp, &b_FatimaVmeCalData_fsingles_tdc_timestamp);
   fChain->SetBranchAddress("FatimaVmeCalData.fsingles_tdc_timestamp_raw", FatimaVmeCalData_fsingles_tdc_timestamp_raw, &b_FatimaVmeCalData_fsingles_tdc_timestamp_raw);
   fChain->SetBranchAddress("FatimaVmeCalData.fsingles_tdc_id", FatimaVmeCalData_fsingles_tdc_id, &b_FatimaVmeCalData_fsingles_tdc_id);
   fChain->SetBranchAddress("FatimaVmeCalData.fsc41l_hits", FatimaVmeCalData_fsc41l_hits, &b_FatimaVmeCalData_fsc41l_hits);
   fChain->SetBranchAddress("FatimaVmeCalData.fsc41r_hits", FatimaVmeCalData_fsc41r_hits, &b_FatimaVmeCalData_fsc41r_hits);
   fChain->SetBranchAddress("FatimaVmeCalData.ftm_undelayed_hits", FatimaVmeCalData_ftm_undelayed_hits, &b_FatimaVmeCalData_ftm_undelayed_hits);
   fChain->SetBranchAddress("FatimaVmeCalData.ftm_delayed_hits", FatimaVmeCalData_ftm_delayed_hits, &b_FatimaVmeCalData_ftm_delayed_hits);
   fChain->SetBranchAddress("FatimaVmeCalData.fsc41l_e_hits", FatimaVmeCalData_fsc41l_e_hits, &b_FatimaVmeCalData_fsc41l_e_hits);
   fChain->SetBranchAddress("FatimaVmeCalData.fsc41r_e_hits", FatimaVmeCalData_fsc41r_e_hits, &b_FatimaVmeCalData_fsc41r_e_hits);
   fChain->SetBranchAddress("FatimaVmeCalData.ftm_undelayed_e_hits", FatimaVmeCalData_ftm_undelayed_e_hits, &b_FatimaVmeCalData_ftm_undelayed_e_hits);
   fChain->SetBranchAddress("FatimaVmeCalData.ftm_delayed_e_hits", FatimaVmeCalData_ftm_delayed_e_hits, &b_FatimaVmeCalData_ftm_delayed_e_hits);
   fChain->SetBranchAddress("FatimaVmeCalData.ffatvme_mult", FatimaVmeCalData_ffatvme_mult, &b_FatimaVmeCalData_ffatvme_mult);
   fChain->SetBranchAddress("FatimaVmeCalData.fqdc_id", FatimaVmeCalData_fqdc_id, &b_FatimaVmeCalData_fqdc_id);
   fChain->SetBranchAddress("FatimaVmeCalData.fqdc_e", FatimaVmeCalData_fqdc_e, &b_FatimaVmeCalData_fqdc_e);
   fChain->SetBranchAddress("FatimaVmeCalData.fqdc_e_raw", FatimaVmeCalData_fqdc_e_raw, &b_FatimaVmeCalData_fqdc_e_raw);
   fChain->SetBranchAddress("FatimaVmeCalData.fqdc_t_coarse", FatimaVmeCalData_fqdc_t_coarse, &b_FatimaVmeCalData_fqdc_t_coarse);
   fChain->SetBranchAddress("FatimaVmeCalData.fqdc_t_fine", FatimaVmeCalData_fqdc_t_fine, &b_FatimaVmeCalData_fqdc_t_fine);
   fChain->SetBranchAddress("FatimaVmeCalData.ftdc_id", FatimaVmeCalData_ftdc_id, &b_FatimaVmeCalData_ftdc_id);
   fChain->SetBranchAddress("FatimaVmeCalData.ftdc_time", FatimaVmeCalData_ftdc_time, &b_FatimaVmeCalData_ftdc_time);
   fChain->SetBranchAddress("FatimaVmeCalData.ftdc_time_raw", FatimaVmeCalData_ftdc_time_raw, &b_FatimaVmeCalData_ftdc_time_raw);


   fChain->SetBranchAddress("FatimaVmeTimeMachineData", &FatimaVmeTimeMachineData_, &b_cbmout_0_FatimaVmeTimeMachineDataFolder_FatimaVmeTimeMachineData_);
   fChain->SetBranchAddress("FatimaVmeTimeMachineData.fUniqueID", FatimaVmeTimeMachineData_fUniqueID, &b_FatimaVmeTimeMachineData_fUniqueID);
   fChain->SetBranchAddress("FatimaVmeTimeMachineData.fBits", FatimaVmeTimeMachineData_fBits, &b_FatimaVmeTimeMachineData_fBits);
   fChain->SetBranchAddress("FatimaVmeTimeMachineData.fundelayed_time", FatimaVmeTimeMachineData_fundelayed_time, &b_FatimaVmeTimeMachineData_fundelayed_time);
   fChain->SetBranchAddress("FatimaVmeTimeMachineData.fdelayed_time", FatimaVmeTimeMachineData_fdelayed_time, &b_FatimaVmeTimeMachineData_fdelayed_time);
   fChain->SetBranchAddress("FatimaVmeTimeMachineData.fwr_subsystem_id", FatimaVmeTimeMachineData_fwr_subsystem_id, &b_FatimaVmeTimeMachineData_fwr_subsystem_id);
   fChain->SetBranchAddress("FatimaVmeTimeMachineData.fwr_t", FatimaVmeTimeMachineData_fwr_t, &b_FatimaVmeTimeMachineData_fwr_t);
   fChain->SetBranchAddress("GermaniumCalData", &GermaniumCalData_, &b_cbmout_0_Germanium Cal Data_GermaniumCalData_);
   fChain->SetBranchAddress("GermaniumCalData.fUniqueID", &GermaniumCalData_fUniqueID, &b_GermaniumCalData_fUniqueID);
   fChain->SetBranchAddress("GermaniumCalData.fBits", &GermaniumCalData_fBits, &b_GermaniumCalData_fBits);
   fChain->SetBranchAddress("GermaniumCalData.fevent_trigger_time", &GermaniumCalData_fevent_trigger_time, &b_GermaniumCalData_fevent_trigger_time);
   fChain->SetBranchAddress("GermaniumCalData.fpileup", &GermaniumCalData_fpileup, &b_GermaniumCalData_fpileup);
   fChain->SetBranchAddress("GermaniumCalData.foverflow", &GermaniumCalData_foverflow, &b_GermaniumCalData_foverflow);
   fChain->SetBranchAddress("GermaniumCalData.fchannel_trigger_time", &GermaniumCalData_fchannel_trigger_time, &b_GermaniumCalData_fchannel_trigger_time);
   fChain->SetBranchAddress("GermaniumCalData.fchannel_energy", &GermaniumCalData_fchannel_energy, &b_GermaniumCalData_fchannel_energy);
   fChain->SetBranchAddress("GermaniumCalData.fcrystal_id", &GermaniumCalData_fcrystal_id, &b_GermaniumCalData_fcrystal_id);
   fChain->SetBranchAddress("GermaniumCalData.fdetector_id", &GermaniumCalData_fdetector_id, &b_GermaniumCalData_fdetector_id);
   fChain->SetBranchAddress("GermaniumCalData.fwr_subsystem_id", &GermaniumCalData_fwr_subsystem_id, &b_GermaniumCalData_fwr_subsystem_id);
   fChain->SetBranchAddress("GermaniumCalData.fwr_t", &GermaniumCalData_fwr_t, &b_GermaniumCalData_fwr_t);
   fChain->SetBranchAddress("GermaniumCalData.fabsolute_event_time", &GermaniumCalData_fabsolute_event_time, &b_GermaniumCalData_fabsolute_event_time);
   fChain->SetBranchAddress("GermaniumTimeMachineData", &GermaniumTimeMachineData_, &b_cbmout_0_Time Machine Data_GermaniumTimeMachineData_);
   fChain->SetBranchAddress("GermaniumTimeMachineData.fUniqueID", &GermaniumTimeMachineData_fUniqueID, &b_GermaniumTimeMachineData_fUniqueID);
   fChain->SetBranchAddress("GermaniumTimeMachineData.fBits", &GermaniumTimeMachineData_fBits, &b_GermaniumTimeMachineData_fBits);
   fChain->SetBranchAddress("GermaniumTimeMachineData.fundelayed_time", &GermaniumTimeMachineData_fundelayed_time, &b_GermaniumTimeMachineData_fundelayed_time);
   fChain->SetBranchAddress("GermaniumTimeMachineData.fdelayed_time", &GermaniumTimeMachineData_fdelayed_time, &b_GermaniumTimeMachineData_fdelayed_time);
   fChain->SetBranchAddress("GermaniumTimeMachineData.fwr_subsystem_id", &GermaniumTimeMachineData_fwr_subsystem_id, &b_GermaniumTimeMachineData_fwr_subsystem_id);
   fChain->SetBranchAddress("GermaniumTimeMachineData.fwr_t", &GermaniumTimeMachineData_fwr_t, &b_GermaniumTimeMachineData_fwr_t);
   Notify();
}

Bool_t vme_fatima::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void vme_fatima::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t vme_fatima::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef vme_fatima_cxx
