
#include <iostream>
#include <string>
#include <vector>
#include <TFile.h>
#include <TTree.h>

void test() {
    // Open the ROOT file
    TFile *file = TFile::Open("run_0022_test.root");
    if (!file || file->IsZombie()) {
        std::cerr << "Error opening file" << std::endl;
        return;
    }

    // Get the TTree
    TTree *tree;
    file->GetObject("evt", tree); // Replace "TTreeName" with the actual name of your TTree
    if (!tree) {
        std::cerr << "Error getting tree" << std::endl;
        return;
    }

    // Set up branches
    //Int_t LisaCalData_;
    unsigned int fUniqueID[10];
    unsigned int fBits[10];
    ULong_t wr_t[10];
    Int_t layer_id[10], xposition[10], yposition[10], pileup[10], overflow[10];
    string city[10];
    UInt_t energy[10];
    std::vector<unsigned short> trace[10];
    ULong_t event_no[10];

    //tree->SetBranchAddress("LisaCalData", &LisaCalData_);
    tree->SetBranchAddress("LisaCalData.fUniqueID", fUniqueID);
    tree->SetBranchAddress("LisaCalData.fBits", fBits);
    tree->SetBranchAddress("LisaCalData.wr_t", wr_t);
    tree->SetBranchAddress("LisaCalData.layer_id", layer_id);
    tree->SetBranchAddress("LisaCalData.city", city);
    tree->SetBranchAddress("LisaCalData.xposition", xposition);
    tree->SetBranchAddress("LisaCalData.yposition", yposition);
    tree->SetBranchAddress("LisaCalData.energy", energy);
    tree->SetBranchAddress("LisaCalData.trace", trace);
    tree->SetBranchAddress("LisaCalData.event_no", event_no);
    tree->SetBranchAddress("LisaCalData.pileup", pileup);
    tree->SetBranchAddress("LisaCalData.overflow", overflow);

    // Loop over all entries
    Long64_t nentries = tree->GetEntries();
    for (Long64_t i = 4; i < 5; i++) {
        tree->GetEntry(i);

        // Process the data
        std::cout << "Entry " << i << ":\n";
        //std::cout << " LisaCalData_: " << LisaCalData_ << "\n";
        for (size_t j = 0; j < 2; j++) {
            std::cout << "  fUniqueID[" << j << "]: " << fUniqueID[j] << "\n";
            std::cout << "  fBits[" << j << "]: " << fBits[j] << "\n";
            std::cout << "  wr_t[" << j << "]: " << wr_t[j] << "\n";
            std::cout << "  layer_id[" << j << "]: " << layer_id[j] << "\n";
            std::cout << "  city[" << j << "]: " << city[j] << "\n";
            std::cout << "  xposition[" << j << "]: " << xposition[j] << "\n";
            std::cout << "  yposition[" << j << "]: " << yposition[j] << "\n";
            std::cout << "  energy[" << j << "]: " << energy[j] << "\n";
            std::cout << "  event_no[" << j << "]: " << event_no[j] << "\n";
            std::cout << "  pileup[" << j << "]: " << pileup[j] << "\n";
            std::cout << "  overflow[" << j << "]: " << overflow[j] << "\n";
            std::cout << "  trace[" << j << "]: ";
            for (const auto& val : trace[j]) {
                std::cout << val << " ";
            }
            std::cout << "\n";
			std::cout<<"=================================="<<std::endl;
        }
    }

    // Close the file
    file->Close();
    delete file;
}



