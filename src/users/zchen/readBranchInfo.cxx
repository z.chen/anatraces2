//
//       $  root -l readBranchInfo.cxx
//



#include <iostream>
#include <unordered_set>
#include <TString.h>

// Define a custom hash function for TString
namespace std {
	template<>
		struct hash<TString> {
			std::size_t operator()(const TString& s) const noexcept {
				return std::hash<std::string>()(s.Data());
			}
		};
}

//*** declare ***//
TString iptFile = "run_0022_test.root";
Long64_t entry = 0;
Int_t lenmax = 20;


TString l_br = "LisaCalData";
TString l_UniqueID = "LisaCalData.fUniqueID";
TString l_fBits = "LisaCalData.fBits";
TString l_fwr_t = "LisaCalData.wr_t";
TString l_lid = "LisaCalData.layer_id";
TString l_city = "LisaCalData.city";
TString l_xpos = "LisaCalData.xposition";
TString l_ypos = "LisaCalData.yposition";
TString l_e = "LisaCalData.energy";//Offset:
TString l_trace = "LisaCalData.trace";//Offset:

Int_t fverbos = 2;

std::unordered_set<TString> v_leafToRead;

void loadLeafNames(){
	v_leafToRead.insert(l_br);
	v_leafToRead.insert(l_UniqueID);
	v_leafToRead.insert(l_fBits);
	v_leafToRead.insert(l_fwr_t);
	v_leafToRead.insert(l_lid);
	v_leafToRead.insert(l_city);
	v_leafToRead.insert(l_xpos);
	v_leafToRead.insert(l_ypos);
	v_leafToRead.insert(l_e);
	v_leafToRead.insert(l_trace);
}

void readBranchInfo(){

	// Open the ROOT file
	TFile *file = TFile::Open(iptFile);
	if (!file || file->IsZombie()) {
		std::cerr << "Error opening file!" << std::endl;
		return;
	}

	// Get the TTree
	TTree *tree = nullptr;
	file->GetObject("evt", tree);
	if (!tree) {
		std::cerr << "Tree 'evt' not found!" << std::endl;
		return;
	}

	std::cout<<"...oooOO0OOooo......oooOO0OOooo......oooOO0OOooo......oooOO0OOooo..."<<std::endl;
	std::cout<<"---> ROOT file  : "<<iptFile<<std::endl;
	std::cout<<"---> Total entry: "<<tree->GetEntries()<<std::endl;
	std::cout<<"-->Current entry: "<<entry<<std::endl;

	loadLeafNames();

	if (entry != -1) {
		Int_t ret = tree->LoadTree(entry);
		if (ret == -2) {
			std::cout<<"Cannot read entry(entry does not exist)"<<std::endl;
			return;
		} else if (ret == -1) {
			std::cout<<"Cannot read entry(I/O error)"<<std::endl;;
			return;
		}
		ret = tree->GetEntry(entry);
		if (ret == -1) {
			std::cout<<"Cannot read entry(I/O error)"<<std::endl;
			return;
		} else if (ret == 0) {
			std::cout<<"Cannot read entry(no data read)"<<std::endl;;
			return;
		}
	}
	TObjArray* leaves  = tree->GetListOfLeaves();
	Int_t nleaves = leaves->GetEntriesFast();
	std::cout<<"---> Num of leaves = "<<nleaves<<std::endl;
	if(fverbos==0){
		std::cout<<"---> List of leaves to read: "<<std::endl;
		for ( const TString& value : v_leafToRead ) {
			std::cout << "*** " << value << std::endl;
		}
	}

	int cnt=0;
	for (Int_t i = 0; i < nleaves; i++) {
		TLeaf* leaf = (TLeaf*) leaves->UncheckedAt(i);
		Int_t len = leaf->GetLen();
		if (len <= 0) {
			continue;
		}
		len = TMath::Min(len, lenmax);
		if (leaf->IsA() == TLeafElement::Class()) {

			TString lfullname = (TString)leaf->GetFullName();
			TString lname = (TString)leaf->GetName();
			// compare leaves in the list with loaded leaves // 
			bool fIs_found = (v_leafToRead.find(lfullname) != v_leafToRead.end());
			if( !fIs_found )continue;

			auto lpointer_type = leaf->GetTypeName();
			Int_t lcnt = leaf->GetLen(); 
			if(fverbos==0){
				std::cout<<"...oooOO0OOooo......oooOO0OOooo......oooOO0OOooo......oooOO0OOooo..."<<std::endl;
				std::cout<<"*** Info of Leaf: "<<std::endl;
				std::cout<<"*** Leaf type   : "<<typeid(*leaf).name() << std::endl;
				std::cout<<"*** ValueType   : "<<lpointer_type<<std::endl;
				std::cout<<"*** Full name   : "<<lfullname<<std::endl;
				std::cout<<"*** Leaf name   : "<<lname<<std::endl;
				std::cout<<"*** GetLen      : "<<lcnt<<std::endl;//value counts
			}

			// Check if the leaf is of type TLeafElement
			TLeafElement* leafElement = dynamic_cast<TLeafElement*>(leaf);

			// Get the associated TBranchElement
			TBranchElement* branchElement = dynamic_cast<TBranchElement*>(leafElement->GetBranch());

			TString fbran_name = branchElement->GetFullName();
			if(fverbos == 1)std::cout<<"***branch: full name "<<fbran_name<<std::endl;
			Int_t fbran_ndata = branchElement->GetNdata();
			if(fverbos == 1)std::cout<<"***branch: Ndata() "<<fbran_ndata<<std::endl;
			// Get the object associated with the branch
			char* object = branchElement->GetObject();
			if(cnt==0)std::cout<<"*** Object address: "<< static_cast<void*>(object) << std::endl;
			if(fverbos==0){
				std::cout<<"...oooOO0OOooo......oooOO0OOooo......oooOO0OOooo......oooOO0OOooo..."<<std::endl;
				std::cout<<"*** leaf->PrintVaue()"<<std::endl;
			}
			leaf->PrintValue(len);
			cnt++;
			continue;
		}else{
			std::cout<<"---> Other TLeaf type."<<std::endl;
		}
	}//end of nleaves
}

void addrOffsetCalculator( uintptr_t addr1, uintptr_t addr2 ){

	uintptr_t offset = addr1 > addr2 ? addr1 - addr2 : addr2 - addr1;

	// Print the addresses and the offset
	std::cout<<"...oooOO0OOooo......oooOO0OOooo......oooOO0OOooo......oooOO0OOooo..."<<std::endl;
	std::cout << "Address 1: 0x" << std::hex << addr1 << std::endl;
	std::cout << "Address 2: 0x" << std::hex << addr2 << std::endl;
	std::cout << "Offset: 0x" << std::hex << offset << " (" << std::dec << offset << " bytes)" << std::endl;

}
