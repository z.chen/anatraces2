
//
//last updated@16 Jul,2024
//Z.Chen
//

#include <iostream>
#include <TTree.h>
#include <TFile.h>
#include <vector>
#include <TCanvas.h>
#include <TGraph.h>
#include <TMarker.h>
#include <TAxis.h>
#include <TMultiGraph.h>
#include <TMath.h>
#include <TRandom3.h>
#include "../../../inc/users/zchen/userA.hh"
#include "../../../inc/lisa_vis.hh"

userA_class::userA_class(TTree *tree)
{
    if (tree == NULL) {
        std::cout << "***Error_userA_cc_000:userA_class::userA_class(tree==0)" << std::endl;
        return;
    }
    Init(tree);//from lisa_ucesb
    setTree(tree);//deliver tree to ftree;from lisa_febex
}

userA_class::~userA_class()
{
    std::cout << "userA_class::~userA_class()" << std::endl;
}

userA_class::userA_class()
{
    std::cout << "userA_class::userA_class()" << std::endl;
}

//...oooOO0OOooo......oooOO0OOooo.....oooOO0OOooo......oooOO0OOooo......//
// write your functons below //
//...oooOO0OOooo......oooOO0OOooo.....oooOO0OOooo......oooOO0OOooo......//

void userA_class::anaPileUp(lisa_febex *ipt)
{
    std::cout << "userA_class::anaPileup" << std::endl;

    //...oooOO0OOooo......oooOO0OOooo.....oooOO0OOooo......oooOO0OOooo......//
    // for display //
    //...oooOO0OOooo......oooOO0OOooo.....oooOO0OOooo......oooOO0OOooo......//
    lisa_vis *vis1 = new lisa_vis(ipt);

    /*
       TCanvas *cc = (TCanvas*)vis1->createCanvas(2,2);//this is a 2x2 canvas
                                                       //if you want to use existing functions //
                                                       cc->cd(1);
                                                       vis1->showSingleRawTrace(ipt);
                                                       cc->cd(2);
                                                       vis1->showSingleCorrectTrace(ipt);
                                                       cc->cd(3);
                                                       vis1->showMWDTrace(ipt);
                                                       cc->cd(4);
                                                       vis1->showMWDTrace2(ipt);//overlap 2 and 3
                                                       */
    //std::cout << "Current entry = " << ipt->getJentry() << std::endl;
    //std::cout << "Read from hit "<< ipt->getReadFromHit()<<std::endl;
    //std::cout<<"***Amplitude[i] = "<<ipt->getMWDenergy()[ipt->getReadFromHit()]<<"***"<<std::endl;

    //...oooOO0OOooo......oooOO0OOooo.....oooOO0OOooo......oooOO0OOooo......//
    // for your own analysis //
    //...oooOO0OOooo......oooOO0OOooo.....oooOO0OOooo......oooOO0OOooo......//
    TCanvas *cc2 = (TCanvas*)vis1->createCanvas(2,2);//this is a 2x2 canvas
    cc2->cd(1);
    ipt->loadSingleTrace( ipt->getTree() );//always start from loadSingleTrace()
                                           // to get corrected trace //
    ipt->calcCorrectTrace();
    std::vector<Double_t> fC_xx = ipt->getCorrectTraceI();
    std::vector<Double_t> fC_yy = ipt->getCorrectTraceV();

    TGraph *gr1 = new TGraph( fC_xx.size(), fC_xx.data(), fC_yy.data() );
    gr1->Draw("AP*");
    gr1->SetTitle("Corrected trace");
    gr1->GetXaxis()->SetTitle("Time [ns]");
    gr1->GetYaxis()->SetTitle("Energy [mV]");
    //set format//
    gr1->GetXaxis()->SetTitleOffset(0.85);
    gr1->GetXaxis()->SetLabelOffset(0.001);

    gr1->GetYaxis()->SetTitleOffset(0.65);
    gr1->GetYaxis()->SetLabelOffset(0.001);
    gr1->SetMarkerColor(kBlue);

    cc2->cd(2);
    ipt->calcMWDTrace();//to calculate a MWD trace from corrected trace
    int flen1 = ipt->getMWDTraceLength();
    if(flen1==0) {//for debug
        std::cout<<"Error_: empty graph; trace length = 0."<<std::endl;
        return;
    }
    std::vector<Double_t> fMWD_xx = ipt->getMWDTraceI();
    std::vector<Double_t> fMWD_yy = ipt->getMWDTraceV();
    if( fMWD_xx[0] < 0.5 ){//for debug:to check e-310
        std::cout<<"Error_: e-310 error. fMWD_xx[0] = "<<fMWD_xx[0]<<""<<std::endl;
    }
    TGraph *gr3 = new TGraph( fMWD_xx.size(), fMWD_xx.data(), fMWD_yy.data() );
    gr3->Draw("AP*");
    gr3->SetTitle("MWD trace");
    gr3->GetXaxis()->SetTitle("Time [ns]");
    gr3->GetYaxis()->SetTitle("Energy [mV]");
    //set format//
    gr3->GetXaxis()->SetTitleOffset(0.85);
    gr3->GetXaxis()->SetLabelOffset(0.001);
    gr3->GetYaxis()->SetTitleOffset(0.65);
    gr3->GetYaxis()->SetLabelOffset(0.001);
    gr3->SetMarkerColor(kRed);
    // find some ways to identify pileup automatically //
    // find kink //
    // Calculate slopes between points //
    std::vector<double> slopes;
    std::vector<Double_t> x = getMWDTraceI();
    std::vector<Double_t> y = getMWDTraceV();
    for (UInt_t i = 1; i < x.size(); ++i) {
        double dx = x[i] - x[i - 1];
        double dy = y[i] - y[i - 1];
        slopes.push_back(dy / dx);
    }
    // Set a threshold for detecting kinks //
    double threshold = -0.8;  // Adjust this value based on your data //
                              // Identify the kinks //
    std::vector<Int_t> kinkIndices;
    for (UInt_t i = 1; i < slopes.size(); ++i) {
        //double slopeChange = std::abs(slopes[i] - slopes[i - 1]);
        double slopeChange = slopes[i] - slopes[i - 1];
        //if( TMath::Abs(slopeChange) > 0.5 )std::cout<<"slopeChange(exp) = "<<slopeChange<<" @ x = "<<fMWD_xx[i]<<std::endl;
        if (slopeChange < threshold) {
            kinkIndices.push_back(i);
        }
    }
    // Highlight the kink points on the same TGraph //
    for (Int_t index : kinkIndices) {
        TMarker *marker = new TMarker(fMWD_xx[index], fMWD_yy[index], 29);
        marker->SetMarkerColor(kBlack);
        marker->SetMarkerSize(2);
        marker->Draw();
    }

    //*** simulation ***//
    // Number of points
    const int n = 2000;
    //double x[n], y[n];
    std::vector <Double_t> sim_x;
    std::vector <Double_t> sim_y;
    sim_x.clear();
    sim_y.clear();

    // Define the constants for the functions
    double constant_value = 0.0;
    double exp_constant = 300.0;
    double exp_decay_rate = -1/2050.0;
    double sim_uncertainty = 0.2;

    // Generate the data points
    int cnt=0;
    for (int i = 0; i < n; i++) {
        if (i < 255) {
            // Left part - constant
            sim_x.push_back(0 + i*10);//sim_x[i] = 2000 + i*10;
            sim_y.push_back(gRandom->Gaus(constant_value, sim_uncertainty));//sim_y[i] = constant_value;
        }else if(i < 460 && i >= 255){
            // Middle part - exponential decay
            sim_x.push_back(10 + i*10);//sim_x[i] = 2020 + i*10;
            double ytem = exp_constant * TMath::Exp(exp_decay_rate * (sim_x.at(i)-2560 ));
            sim_y.push_back(gRandom->Gaus(ytem, sim_uncertainty));//sim_y[i] = gRandom->Gaus(ytem, 0.01);
        }
        else if(i < 1840 && i >= 460){
            sim_x.push_back( i*10);//sim_x[i] = 2020 + i*10;
            double ytem = exp_constant * TMath::Exp(exp_decay_rate * (sim_x.at(i)-4600 ));
            sim_y.push_back(gRandom->Gaus(ytem, sim_uncertainty));//sim_y[i] = gRandom->Gaus(ytem, 0.01);
        }
        else {
            // Right part - exponential decay
            sim_x.push_back( i*10);//sim_x[i] = 2020 + i*10;
            double ytem = exp_constant * TMath::Exp(exp_decay_rate * (sim_x.at(i)-18400 ));
            sim_y.push_back(gRandom->Gaus(ytem, sim_uncertainty));//sim_y[i] = gRandom->Gaus(ytem, 0.01);
        }
        cnt++;
    }

    cc2->cd(3);
    ipt->setCorrectTraceI(sim_x);
    ipt->setCorrectTraceV(sim_y);
    std::vector<Double_t> fcorrect_xx_new = ipt->getCorrectTraceI();
    std::vector<Double_t> fcorrect_yy_new = ipt->getCorrectTraceV();
    // Create a TGraph
    TGraph *graph_sim = new TGraph(fcorrect_xx_new.size(), fcorrect_xx_new.data(), fcorrect_yy_new.data());
    graph_sim->SetMarkerColor(kBlue);
    graph_sim->Draw("AP*");

    // Set axis labels
    graph_sim->SetTitle("Simulation");
    graph_sim->GetXaxis()->SetTitle("Sim_Time [ns]");
    graph_sim->GetYaxis()->SetTitle("Amp [mV]");
    graph_sim->GetXaxis()->SetTitleOffset(0.85);
    graph_sim->GetXaxis()->SetLabelOffset(0.001);
    graph_sim->GetYaxis()->SetTitleOffset(0.65);
    graph_sim->GetYaxis()->SetLabelOffset(0.001);

    cc2->cd(4);
    ipt->calcMWDTrace();//to calculate a MWD trace from corrected trace
    int flen2 = ipt->getMWDTraceLength();
    if(flen2==0) {//for debug
        std::cout<<"Error_: empty graph; trace length = 0."<<std::endl;
        return;
    }
    std::vector<Double_t> fsim_MWD_xx = ipt->getMWDTraceI();
    std::vector<Double_t> fsim_MWD_yy = ipt->getMWDTraceV();
    if( fsim_MWD_xx[0] < 0.5 ){//for debug:to check e-310
        std::cout<<"Error_: e-310 error. fsim_MWD_xx[0] = "<<fsim_MWD_xx[0]<<""<<std::endl;
    }
    TGraph *gr4 = new TGraph( fsim_MWD_xx.size(), fsim_MWD_xx.data(), fsim_MWD_yy.data() );
    gr4->Draw("AP*");
    gr4->SetTitle("sim MWD trace");
    gr4->GetXaxis()->SetTitle("Time [ns]");
    gr4->GetYaxis()->SetTitle("Energy [mV]");
    //set format//
    gr4->GetXaxis()->SetTitleOffset(0.85);
    gr4->GetXaxis()->SetLabelOffset(0.001);
    gr4->GetYaxis()->SetTitleOffset(0.65);
    gr4->GetYaxis()->SetLabelOffset(0.001);
    gr4->SetMarkerColor(kRed);

    if(ipt->getIsCalcDMi()) std::cout<<"[Print INFO] Using DM(i)"<<std::endl;
    if(ipt->getIsCalcMWDi())std::cout<<"[Print INFO] Using MWD(i)"<<std::endl;
    //ipt->printSettings();//debug
    // find some ways to identify pileup automatically //
    // find kink //
    // Calculate slopes between points //
    std::vector<double> slopes_sim;
    for (UInt_t i = 1; i < fsim_MWD_xx.size(); ++i) {
        double dx = fsim_MWD_xx[i] - fsim_MWD_xx[i - 1];
        double dy = fsim_MWD_yy[i] - fsim_MWD_yy[i - 1];
        if(dx == 0)continue;
        slopes_sim.push_back(dy / dx);
    }
    // Set a threshold for detecting kinks //
    // Identify the kinks //
    std::vector<Int_t> kinkIndices_sim;
    for (UInt_t i = 1; i < slopes_sim.size(); ++i) {
        double slopeChange = slopes_sim[i] - slopes_sim[i - 1];
        //if(TMath::Abs(slopeChange)>0.5)std::cout<<"slopeChange(sim) = "<<slopeChange<<" @ x = "<<fsim_MWD_xx[i]<<std::endl;
        if (slopeChange < threshold) {
            kinkIndices_sim.push_back(i);
        }
    }
    // Highlight the kink points on the same TGraph //
    for (Int_t index : kinkIndices_sim) {
        TMarker *marker_sim = new TMarker(fsim_MWD_xx[index], fsim_MWD_yy[index], 29);
        marker_sim->SetMarkerColor(kBlack);
        marker_sim->SetMarkerSize(2);
        marker_sim->Draw();
    }


    // calculate energy according to kinks //
    // exp data //
    Int_t length = getMWDTraceLength();
    int points[10];
    double fsum[10];
    for(int i=0;i<10;i++)points[i]=0;
    for(int i=0;i<10;i++)fsum[i]=0;
    for(int i=0;i<length;i++){
        for(UInt_t jj=0;jj<kinkIndices.size()/2;jj++){
            if( x[i]>x[ kinkIndices[jj*2]+2 ] && x[i]< x[ kinkIndices[jj*2+1]-2] ){
                //std::cout<<"current range: "<<fxx_energy[ kink[jj]+10]<<"  "<<fxx_energy[kink[jj+1]-10 ]<<std::endl;
                //if( fxx_energy[i]>fRange[jj] && fxx_energy[i]<fRange[jj+1] )//ns
                fsum[jj]+=y[i];
                points[jj]++;
            }
        }
    }
    for(UInt_t jj=0;jj<kinkIndices.size()/2;jj++){
        //std::cout<<""<<jj<<" sum "<<fsum[jj]<<std::endl;
        std::cout<<"[Print INFO] Calc energy based on kinks: "<<jj<<" "<<fsum[jj]/points[jj]<<std::endl;
    }
    // simulation //
    int points_sim[10];
    double fsum_sim[10];
    for(int i=0;i<10;i++)points_sim[i]=0;
    for(int i=0;i<10;i++)fsum_sim[i]=0;
    for(int i=0;i<length;i++){
        for(UInt_t jj=0;jj<kinkIndices_sim.size()/2;jj++){
            if( fsim_MWD_xx[i]>fsim_MWD_xx[ kinkIndices_sim[jj*2]+2 ] && fsim_MWD_xx[i]< fsim_MWD_xx[ kinkIndices_sim[jj*2+1]-2] ){
                //std::cout<<"current range: "<<fxx_energy[ kink[jj]+10]<<"  "<<fxx_energy[kink[jj+1]-10 ]<<std::endl;
                //if( fxx_energy[i]>fRange[jj] && fxx_energy[i]<fRange[jj+1] )//ns
                fsum_sim[jj]+=fsim_MWD_yy[i];
                points_sim[jj]++;
            }
        }
    }
    for(UInt_t jj=0;jj<kinkIndices_sim.size()/2;jj++){
        //std::cout<<""<<jj<<" sum "<<fsum[jj]<<std::endl;
        std::cout<<"[Print INFO] Calc energy based on kinks: "<<jj<<" "<<fsum_sim[jj]/points_sim[jj]<<std::endl;
    }

}
