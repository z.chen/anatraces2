
#ifndef _ZCHEN_CC_
#define _ZCHEN_CC_

#include <iostream>
#include <TGraph.h>
#include <TVector3.h>
#include <TH2.h>

#include "../../../inc/users/zchen/zchen.hh"
#include "../../../inc/users/zchen/position.hh"
#include "../../../inc/users/zchen/LISA.hh"



zchen::zchen(){

	std::cout<<"tracking..."<<std::endl;

}

zchen::~zchen(){

	std::cout<<"end of tracking..."<<std::endl;

}

void zchen::hitPattern(){

	//*** data reading ***//
	Long64_t nentries = iptTree->GetEntries();
	std::cout<<"===================================="<<std::endl;
	std::cout<<"--->Total event number: "<<nentries<<std::endl;

	if(USE_CENTER)std::cout<<"Center position is used."<<std::endl;
	if(USE_FAKE)std::cout<<"Center position + random values(fake results) is used."<<std::endl;
	if(SINGLE_MODE)std::cout<<"Running at single mode."<<std::endl;
	if(HOLDON_MODE)std::cout<<"Running at Hold-on mode."<<std::endl;

	//*** tracking ***//
	setEnt();
	loadPosition();
	setDisplay();

	//*** loop ***//
	//for (Long64_t jentry=0; jentry<10000;jentry++)
	for (Long64_t jentry=0; jentry<nentries;jentry++)
	{
		if( jentry%SPEED !=0 )continue;
		offlineLoop(hit,jentry,nentries);
	}
	std::cout<<"===================================="<<std::endl;
	std::cout<<"The end."<<std::endl;



}

void zchen::offlineLoop(UInt_t fhit, Int_t* channel){

	//iptTree->GetEntry(jentry);
	//*** protection ***//
	if(fhit>8){
		std::cout<<"Error404!!!Illegal hit number: "<<fhit<<std::endl;
		return;
	}
	UInt_t fpos[1];
	if(IF_DEBUG)std::cout<<"in test1"<<std::endl;
	double x1[1] ={ 0};
	double y1[1] ={ 0};
	double x2[1] ={ 0};
	double y2[1] ={ 0};
	if(IF_DEBUG)std::cout<<"in test2"<<std::endl;
	for( UInt_t hh = 0; hh < fhit; hh++ )
	{
		if(IF_DEBUG)std::cout<<"in test3"<<std::endl;
		if(IF_DEBUG)std::cout<<"hit = "<<fhit<<std::endl;
		getPosition(channel[hh],fpos);
		if(USE_CENTER){
			std::cout<<"here.using center position."<<std::endl;
			if(channel[hh]<8 && channel[hh]>0){
				x1[0] = coor_LISA_center_x[fpos[0]];
				y1[0] = coor_LISA_center_y[fpos[0]];
				h_eris->Fill(x1[0],y1[0]);
				std::cout<<"febch = "<<channel[hh]<<std::endl;
			}
			if(IF_DEBUG)std::cout<<"in test4"<<std::endl;
			if(channel[hh]>8 && channel[hh]<16){
				x2[0] = coor_LISA_center_x[fpos[0]];
				y2[0] = coor_LISA_center_y[fpos[0]];
				h_sparrow->Fill(x2[0],y2[0]);
				std::cout<<"febch = "<<channel[hh]<<std::endl;
			}
		}else if(USE_FAKE){
			if(IF_DEBUG)std::cout<<"in test5"<<std::endl;
			if(channel[hh]<8){
				x1[0] = gRandom->Uniform(coor_LISA_x1[fpos[0]],coor_LISA_x2[fpos[0]]);
				y1[0] = gRandom->Uniform(coor_LISA_y1[fpos[0]],coor_LISA_y2[fpos[0]]);
				h_eris->Fill(x1[0],y1[0]);
			}
			if(channel[hh]>8){
				x2[0] = gRandom->Uniform(coor_LISA_x1[fpos[0]],coor_LISA_x2[fpos[0]]);
				y2[0] = gRandom->Uniform(coor_LISA_y1[fpos[0]],coor_LISA_y2[fpos[0]]);
				h_sparrow->Fill(x2[0],y2[0]);
			}
		}else{
			if(channel[hh]<8){
				x1[0] = gRandom->Uniform(coor_LISA_x1[fpos[0]],coor_LISA_x2[fpos[0]]);
				y1[0] = gRandom->Uniform(coor_LISA_y1[fpos[0]],coor_LISA_y2[fpos[0]]);
				h_eris->Fill(x1[0],y1[0]);
			}
			if(channel[hh]>8){
				x2[0] = gRandom->Uniform(coor_LISA_x1[fpos[0]],coor_LISA_x2[fpos[0]]);
				y2[0] = gRandom->Uniform(coor_LISA_y1[fpos[0]],coor_LISA_y2[fpos[0]]);
				h_sparrow->Fill(x2[0],y2[0]);
			}
		}
	}//end of loop for hit
	if(IF_DEBUG)std::cout<<"in test6"<<std::endl;
	if( fhit == 2 && channel[0] < 8 && channel[1] > 8 )filltrack(x1[0],y1[0],positionZ_eris,x2[0],y2[0],positionZ_sparrow,positionZ_degrader,offline_hx,offline_hy,offline_hpz);
	//*** debug-begin ***//
	//int nbinsx = offline_hx->GetNbinsX();
	//std::cout<<"nbinsx after filling the histos = "<<nbinsx<<std::endl;
	//for(int ii=1;ii<nbinsx;ii++)
	//{
	//	double bcenterx = offline_hx->GetXaxis()->GetBinCenter(ii);
	//	int ibin = offline_hx->FindBin(bcenterx);
	//	std::cout<<"bin centerx = "<<bcenterx<<std::endl;
	//	std::cout<<"ibin = "<<ibin<<std::endl;
	//}
	//*** debug-end ***//

	//*** display ***//
	//if(jentry%SPEED==0)
	//if(jentry == 20)
	//{
	//*** tracking ***//
	offline_c1->cd(1);
	//if(if_rebin_x)offline_hx->RebinX(4);if_rebin_x=0;
	offline_hx->Draw("colz");
	p_eris1->Draw();
	p_eris2->Draw();
	p_sparrow1->Draw();
	p_sparrow2->Draw();
	pzline->Draw();
	offline_c1->cd(2);
	//if(if_rebin_y)offline_hy->RebinX(4);if_rebin_y=0;
	offline_hy->Draw("colz");
	p_eris1->Draw();
	p_eris2->Draw();
	p_sparrow1->Draw();
	p_sparrow2->Draw();
	pzline->Draw();
	offline_c1->Update();

	offline_c2->cd();
	offline_hpz->Draw("colz");
	offline_c2->Update();
	//*** hit pattern ***//
	c_hit_pattern->cd(1);
	h_eris->Draw("colz");
	for(UInt_t ii=0;ii<hit_eris.fbox.size();ii++)hit_eris.fbox[ii]->Draw();
	gPad->Modified();
	gPad->Update();

	c_hit_pattern->cd(2);
	h_sparrow->Draw("colz");
	for(UInt_t ii=0;ii<hit_sparrow.fbox.size();ii++)hit_sparrow.fbox[ii]->Draw();
	gPad->Modified();
	gPad->Update();
	//}//end if jentry
	//if(jentry%SPEED==0)std::cout<<"--->Running...: "<<jentry<<"/"<<nentries;
	std::cout<<"--->Running...: "<<jentry<<"/"<<nentries;
	fflush(stdout);
	printf("\r");


}

void zchen::filltrack(double x1,double y1,double z1,double x2, double y2,double z2, double pz,TH2D *offline_hx,TH2D *offline_hy,TH2D *offline_hpz){

	if(SINGLE_MODE){
		offline_hx->Reset();
		offline_hy->Reset();
		offline_hpz->Reset();
	}
	double px,py;
	px=x1+(x2-x1)/(z2-z1)*(pz-z1);
	py=y1+(y2-y1)/(z2-z1)*(pz-z1);

	offline_hpz->Fill(px,py);

	int nbinxx=offline_hx->GetNbinsX();
	//std::cout<<"offline_hx_NBINS = "<<nbinxx<<std::endl;
	double zmin=offline_hx->GetXaxis()->GetXmin();
	double zmax=offline_hx->GetXaxis()->GetXmax();
	for(int ii=0;ii<nbinxx;ii++)
	{
		double nowz=zmin+(zmax-zmin)*ii/(nbinxx*1.0);
		double nowf=x1+(x2-x1)/(z2-z1)*(nowz-z1);
		//std::cout<<"nowz = "<<nowz<<std::endl;
		//std::cout<<"nowf = "<<nowf<<std::endl;
		offline_hx->Fill(nowz,nowf);
	}

	nbinxx=offline_hy->GetNbinsX();
	//std::cout<<"offline_hy_NBINS = "<<nbinxx<<std::endl;
	zmin=offline_hy->GetXaxis()->GetXmin();
	zmax=offline_hy->GetXaxis()->GetXmax();
	for(int ii=0;ii<nbinxx;ii++)
	{
		double nowz=zmin+(zmax-zmin)*ii/(nbinxx*1.0);
		double nowf=y1+(y2-y1)/(z2-z1)*(nowz-z1);
		offline_hy->Fill(nowz,nowf);
	}

}

void zchen::setEnt(){

	gROOT->SetStyle("Modern");
	//gStyle->SetHistFillColor(7);
	//gStyle->SetHistFillStyle(3002);
	//gStyle->SetHistLineColor(kBlue);
	//*** FAIR style ***//
	gStyle->SetHistFillColor(796);
	gStyle->SetHistFillStyle(1001);
	gStyle->SetHistLineColor(kBlack);
	gStyle->SetFuncColor(kRed);
	gStyle->SetFrameLineWidth(2);
	gStyle->SetCanvasColor(0);
	gStyle->SetTitleFillColor(0);
	gStyle->SetTitleStyle(0);
	gStyle->SetStatColor(0);
	gStyle->SetStatStyle(0);
	gStyle->SetStatX(0.9);  
	gStyle->SetStatY(0.9);  
	gStyle->SetPalette(1);
	//gStyle->SetOptLogz(1);
	//  gStyle->SetOptTitle(0);
	//gStyle->SetOptFit(1);
	gStyle->SetOptStat(1111111);
	//gStyle->SetOptStat(0);
	//gStyle->SetPadBorderMode(1);
	//  gStyle->SetOptDate(1);
	gStyle->SetLabelFont(132,"XYZ");
	gStyle->SetLabelSize(0.05,"X");
	gStyle->SetLabelSize(0.05,"Y");
	gStyle->SetLabelOffset(0.004);
	//gStyle->SetTitleOffset(0.1);
	gStyle->SetTitleFont(132,"XYZ");
	gStyle->SetTitleSize(0.045,"X");
	gStyle->SetTitleSize(0.045,"Y");
	//gStyle->SetTitleFont(132,"");
	gStyle->SetTextFont(132);
	gStyle->SetStatFont(132);


}

void zchen::loadPosition(){

	loadPositionInfo();

}


void zchen::setDisplay(){

	p_eris1=new TLine(positionZ_eris,coor_LISA_y1[3],positionZ_eris,coor_LISA_y2[3]);
	p_eris2=new TLine(positionZ_eris,coor_LISA_y1[0],positionZ_eris,coor_LISA_y2[0]);
	p_eris1->SetLineWidth(5);
	p_eris1->SetLineColor(kRed);
	p_eris2->SetLineWidth(5);
	p_eris2->SetLineColor(kRed);

	p_sparrow1=new TLine(positionZ_sparrow,coor_LISA_y1[3],positionZ_sparrow,coor_LISA_y2[3]);
	p_sparrow2=new TLine(positionZ_sparrow,coor_LISA_y1[0],positionZ_sparrow,coor_LISA_y2[0]);
	p_sparrow1->SetLineWidth(5);
	p_sparrow1->SetLineColor(kRed);
	p_sparrow2->SetLineWidth(5);
	p_sparrow2->SetLineColor(kRed);

	pzline=new TLine(positionZ_degrader,-1,positionZ_degrader,1);//z==0;degrader
	pzline->SetLineWidth(5);
	pzline->SetLineColor(kBlack);//6,purple

	offline_hx=new TH2D("offline_hx","Projection of X-Z (TOP VIEW)",20,-10,10,100,-10,10);
	offline_hy=new TH2D("offline_hy","Projection of Y-Z (SIDE VIEW)",20,-10,10,100,-10,10);
	offline_hpz=new TH2D("offline_hpz","Distribution at Z = 0mm",36,-6,6,36,-6,6);
	offline_hx->GetXaxis()->SetTitle("PositionZ[mm]");
	offline_hx->GetYaxis()->SetTitle("PositionX[mm]");
	offline_hy->GetXaxis()->SetTitle("PositionZ[mm]");
	offline_hy->GetYaxis()->SetTitle("PositionY[mm]");
	offline_hpz->GetXaxis()->SetTitle("PositionX[mm]");
	offline_hpz->GetYaxis()->SetTitle("PositionY[mm]");
	offline_hpz->GetYaxis()->SetTitleOffset(.8);
	offline_hx->SetStats(kFALSE);
	offline_hy->SetStats(kFALSE);
	offline_hpz->SetStats(kFALSE);

	offline_c1=new TCanvas("offline_c1","Projection",0,0,900,800);
	offline_c1->Divide(1,2);
	offline_c1->Draw();
	offline_c1->cd(1);
	//offline_c1->cd(1)->SetGrid();
	offline_hx->Draw();
	p_eris1->Draw();
	p_eris2->Draw();
	p_sparrow1->Draw();
	p_sparrow2->Draw();
	pzline->Draw();
	offline_c1->cd(2);
	//offline_c1->cd(2)->SetGrid();
	offline_hy->Draw();
	p_eris1->Draw();
	p_eris2->Draw();
	p_sparrow1->Draw();
	p_sparrow2->Draw();
	pzline->Draw();
	offline_c1->Modified();
	offline_c1->Update();

	offline_c2=new TCanvas("offline_c2","Z=0",910,0,800,800);
	offline_c2->cd();
	//offline_c2->cd()->SetGrid();
	offline_hpz->Draw();
	offline_c2->Modified();
	offline_c2->Update();

	//*** hit pattern ***//
	h_eris = new TH2F("Eris","Eris",30,-7,7,30,-7,7);//mm
	h_eris->GetXaxis()->SetTitle("PositionX[mm]");
	h_eris->GetYaxis()->SetTitle("PositionY[mm]");
	h_sparrow = new TH2F("Sparrow","Sparrow",30,-7,7,30,-7,7);
	h_sparrow->GetXaxis()->SetTitle("PositionX[mm]");
	h_sparrow->GetYaxis()->SetTitle("PositionY[mm]");
	h_eris->SetStats(kFALSE);
	h_sparrow->SetStats(kFALSE);
	c_hit_pattern = new TCanvas("hit_pattern","hit_pattern",0,850,900,400);
	c_hit_pattern->Divide(2,1);
	for(int ii=0;ii<4;ii++){//2x2

		//*** eris ***//
		hit_eris.fbox.push_back(new TBox(coor_LISA_x1[ii],coor_LISA_y1[ii],coor_LISA_x2[ii],coor_LISA_y2[ii]));
		hit_eris.fbox[ii]->SetLineWidth(5);
		hit_eris.fbox[ii]->SetLineColor(kRed);
		hit_eris.fbox[ii]->SetFillStyle(0);
		//*** sparrow ***//
		hit_sparrow.fbox.push_back(new TBox(coor_LISA_x1[ii],coor_LISA_y1[ii],coor_LISA_x2[ii],coor_LISA_y2[ii]));
		hit_sparrow.fbox[ii]->SetLineWidth(5);
		hit_sparrow.fbox[ii]->SetLineColor(6);//6, purple
		hit_sparrow.fbox[ii]->SetFillStyle(0);
	}


}
