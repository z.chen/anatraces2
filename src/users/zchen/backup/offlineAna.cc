//for HiMAC exp
//=======================================================================
//root [0] .L offlineAna.cc
//root [1] offlineAna tt("ch0-7_random_gaus1.root")
//root [2] tt.hitPattern()
//               OR
//root [1] offlineAna *tt = new offlineAna("ch0-7_random_gaus1.root")
//root [2] tt->hitPattern()
//=======================================================================
//change display mode: see offlineAna.hh

#include <TGraph.h>
#include <TVector3.h>
#include "offlineAna.hh"
#include "position.hh"
#include "LISA.hh"
//#include "mycut.cxx"
#include "peaks.hh"
#include <TH2.h>

//...oooOO0OOooo......oooOO0OOooo......oooOO0OOooo......oooOO0OOooo...//
void offlineAna::hitPattern()
{
	//*** data reading ***//
	Long64_t nentries = iptTree->GetEntries();
	std::cout<<"===================================="<<std::endl;
	std::cout<<"--->Total event number: "<<nentries<<std::endl;

	if(USE_CENTER)std::cout<<"Center position is used."<<std::endl;
	if(USE_FAKE)std::cout<<"Center position + random values(fake results) is used."<<std::endl;
	if(SINGLE_MODE)std::cout<<"Running at single mode."<<std::endl;
	if(HOLDON_MODE)std::cout<<"Running at Hold-on mode."<<std::endl;

	//*** tracking ***//
	setEnt();
	loadPosition();
	setDisplay();

	//*** loop ***//
	//for (Long64_t jentry=0; jentry<10000;jentry++)
	for (Long64_t jentry=0; jentry<nentries;jentry++)
	{
		offlineLoop(hit,jentry,nentries);
	}
	std::cout<<"===================================="<<std::endl;
	std::cout<<"The end."<<std::endl;
	return;
}

//...oooOO0OOooo......oooOO0OOooo......oooOO0OOooo......oooOO0OOooo...//
void offlineAna::energyCalib()
{
	TH1F* fdh[24];//8*3
	double fmeans[24];
	int npeaks = 1;
	Double_t *fpar = NULL;

	setEnt();

	TFile *ff1 = new TFile("himac_anaroot_run6_all_thre200.root");
	std::cout<<"***start to analyze run6..."<<std::endl;
	TTree *fftree = (TTree*)ff1->Get("tree");

	for(int ii=0;ii<4;ii++)fftree->Draw(Form("Eraw>>fch%d(1000,0,1000)",ii),Form("!if_overflow&&febCh==%d",ii));
	for(int ii=12;ii<16;ii++)fftree->Draw(Form("Eraw>>fch%d(1000,0,1000)",ii),Form("!if_overflow&&febCh==%d",ii));
	for(int ii=0;ii<4;ii++)fdh[ii] = (TH1F*)gROOT->FindObject(Form("fch%d",ii))->Clone(Form("run6_ch%d",ii));
	for(int ii=12;ii<16;ii++)fdh[ii-8] = (TH1F*)gROOT->FindObject(Form("fch%d",ii))->Clone(Form("run6_ch%d",ii));

	//*** display ***//
	TCanvas *mycan = new TCanvas("mycan","mycan",0,0,800,800);
	for(int ii=0;ii<8;ii++){
		fdh[ii]->GetXaxis()->SetRangeUser(10,1000);
		mycan->Update();
		mycan->Modified();
		fpar = peaks(fdh[ii],npeaks);
		fmeans[ii] = fpar[3*(npeaks-1)+3];
		fdh[ii]->GetXaxis()->SetRangeUser(fmeans[ii]-500,fmeans[ii]+500);
		peaks(fdh[ii],npeaks);
		mycan->Print(Form("./figs/id_%02d.png",ii));
		//std::cout<<"--->amplitude: "<<fpar[3*(npeaks-1)+2]<<std::endl;
		std::cout<<"--->mean: "<<fmeans[ii]<<std::endl;
		//std::cout<<"--->sigma: "<<fpar[3*(npeaks-1)+4]<<std::endl;
	}
	std::cout<<"run6, done."<<std::endl;
	//return;

	TFile *ff2 = new TFile("himac_anaroot_run7_all_thre200.root");
	fftree = (TTree*)ff2->Get("tree");
	std::cout<<"***start to analyze run7..."<<std::endl;

	for(int ii=0;ii<4;ii++)fftree->Draw(Form("Eraw>>fch%d(1000,0,1000)",ii),Form("!if_overflow&&febCh==%d",ii));
	for(int ii=12;ii<16;ii++)fftree->Draw(Form("Eraw>>fch%d(1000,0,1000)",ii),Form("!if_overflow&&febCh==%d",ii));
	for(int ii=0;ii<4;ii++)fdh[ii+8] = (TH1F*)gROOT->FindObject(Form("fch%d",ii))->Clone(Form("run7_ch%d",ii));
	for(int ii=12;ii<16;ii++)fdh[ii] = (TH1F*)gROOT->FindObject(Form("fch%d",ii))->Clone(Form("run7_ch%d",ii));
	for(int ii=8;ii<16;ii++){
		fdh[ii]->GetXaxis()->SetRangeUser(10,1000);
		mycan->Update();
		mycan->Modified();
		fpar = peaks(fdh[ii],npeaks);
		fmeans[ii] = fpar[3*(npeaks-1)+3];
		fdh[ii]->GetXaxis()->SetRangeUser(fmeans[ii]-500,fmeans[ii]+500);
		peaks(fdh[ii],npeaks);
		mycan->Print(Form("./figs/id_%02d.png",ii));
		//std::cout<<"--->amplitude: "<<fpar[3*(npeaks-1)+2]<<std::endl;
		//std::cout<<"--->mean: "<<fpar[3*(npeaks-1)+3]<<std::endl;
		//std::cout<<"--->sigma: "<<fpar[3*(npeaks-1)+4]<<std::endl;
	}


	TFile *ff3 = new TFile("himac_anaroot_run9_all_thre200.root");
	fftree = (TTree*)ff3->Get("tree");
	std::cout<<"***start to analyze run9..."<<std::endl;

	for(int ii=0;ii<4;ii++)fftree->Draw(Form("Eraw>>fch%d(1000,0,1000)",ii),Form("!if_overflow&&febCh==%d",ii));
	for(int ii=12;ii<16;ii++)fftree->Draw(Form("Eraw>>fch%d(1000,0,1000)",ii),Form("!if_overflow&&febCh==%d",ii));
	for(int ii=0;ii<4;ii++)fdh[ii+16] = (TH1F*)gROOT->FindObject(Form("fch%d",ii))->Clone(Form("run9_ch%d",ii));
	for(int ii=12;ii<16;ii++)fdh[ii+8] = (TH1F*)gROOT->FindObject(Form("fch%d",ii))->Clone(Form("run9_ch%d",ii));
	for(int ii=16;ii<24;ii++){
		fdh[ii]->GetXaxis()->SetRangeUser(10,1000);
		mycan->Update();
		mycan->Modified();
		fpar = peaks(fdh[ii],npeaks);
		fmeans[ii] = fpar[3*(npeaks-1)+3];
		fdh[ii]->GetXaxis()->SetRangeUser(fmeans[ii]-500,fmeans[ii]+500);
		peaks(fdh[ii],npeaks);
		mycan->Print(Form("./figs/id_%02d.png",ii));
		//std::cout<<"--->amplitude: "<<fpar[3*(npeaks-1)+2]<<std::endl;
		//std::cout<<"--->mean: "<<fpar[3*(npeaks-1)+3]<<std::endl;
		//std::cout<<"--->sigma: "<<fpar[3*(npeaks-1)+4]<<std::endl;
	}

	for(int ii=0;ii<24;ii++)std::cout<<"--->collected means: "<<"id = "<<ii<<"  "<<fmeans[ii]<<std::endl;
	//mycan->Divide(2,2);
	//for(int ii=4;ii<8;ii++){
	//	mycan->cd(ii+1-4);
	//	fdh[ii]->Draw();
	//}
	
	//*** calib ***/
	double det0[3];//ref
	double det1[3];
	double det2[3];
	double det3[3];
	double det12[3];//ref
	//double det12_2[2];//kick out one point
	double det13[3];
	//double det13_2[2];
	double det14[3];
	double det15[3];
	//double det15_2[2];
	double fitPars[2];
	//*** eris ***//
	for(int ii=0;ii<3;ii++)det0[ii] = fmeans[8*ii];
	for(int ii=0;ii<3;ii++)det1[ii] = fmeans[8*ii+1];
	for(int ii=0;ii<3;ii++)det2[ii] = fmeans[8*ii+2];
	for(int ii=0;ii<3;ii++)det3[ii] = fmeans[8*ii+3];
	TCanvas *fit_Can = new TCanvas("fit_Can","fit_Can",0,0,1000,800);
	fit_Can->Divide(3,2);
	fit_Can->cd(1);
	//***test***//
	for(int ii=0;ii<3;ii++)std::cout<<"---> det0: "<<det0[ii]<<std::endl;
	for(int ii=0;ii<3;ii++)std::cout<<"---> det1: "<<det1[ii]<<std::endl;
	for(int ii=0;ii<3;ii++)std::cout<<"---> det2: "<<det2[ii]<<std::endl;
	for(int ii=0;ii<3;ii++)std::cout<<"---> det3: "<<det3[ii]<<std::endl;
	TGraph *fcalib1 = new TGraph(3,det1,det0);
	TGraph *fcalib2 = new TGraph(3,det2,det0);
	TGraph *fcalib3 = new TGraph(3,det3,det0);
	fcalib1->Draw("AP*");
	TF1 *calib_fit = new TF1("calib_fit","pol1",200,1000);
	fcalib1->Fit("calib_fit","q");
	fcalib1->SetTitle("calib1");
	calib_fit->GetParameters(fitPars);
	std::cout<<"--->bias = "<<fitPars[0]<<", slop = "<<fitPars[1]<<std::endl;
	fit_Can->cd(2);

	fcalib2->Draw("AP*");
	fcalib2->Fit("calib_fit","q");
	fcalib2->SetTitle("calib2");
	calib_fit->GetParameters(fitPars);
	std::cout<<"--->bias = "<<fitPars[0]<<", slop = "<<fitPars[1]<<std::endl;
	fit_Can->cd(3);

	fcalib3->Draw("AP*");
	fcalib3->Fit("calib_fit","q");
	fcalib3->SetTitle("calib3");
	calib_fit->GetParameters(fitPars);
	std::cout<<"--->bias = "<<fitPars[0]<<", slop = "<<fitPars[1]<<std::endl;
	//*** sparrow ***//
	for(int ii=0;ii<3;ii++)det12[ii] = fmeans[8*(ii+1)-4];
	for(int ii=0;ii<3;ii++)det13[ii] = fmeans[8*(ii+1)-3];
	for(int ii=0;ii<3;ii++)det14[ii] = fmeans[8*(ii+1)-2];
	for(int ii=0;ii<3;ii++)det15[ii] = fmeans[8*(ii+1)-1];
	fit_Can->cd(4);
	//kick out middle point
	//det12_2[0]=det12[0];
	//det12_2[1]=det12[2];
	//det13_2[0]=det13[0];
	//det13_2[1]=det13[2];
	//det15_2[0]=det15[0];
	//det15_2[1]=det15[2];
	//***test***//
	for(int ii=0;ii<3;ii++)std::cout<<"---> det12: "<<det12[ii]<<std::endl;
	for(int ii=0;ii<2;ii++)std::cout<<"---> det13: "<<det13[ii]<<std::endl;
	for(int ii=0;ii<3;ii++)std::cout<<"---> det14: "<<det14[ii]<<std::endl;
	for(int ii=0;ii<2;ii++)std::cout<<"---> det15: "<<det15[ii]<<std::endl;
	TGraph *fcalib4 = new TGraph(3,det13,det12);
	TGraph *fcalib5 = new TGraph(3,det14,det12);
	TGraph *fcalib6 = new TGraph(3,det15,det12);
	fcalib4->Draw("AP*");
	fcalib4->Fit("calib_fit","q");
	fcalib4->SetTitle("calib13");
	calib_fit->GetParameters(fitPars);
	std::cout<<"--->bias = "<<fitPars[0]<<", slop = "<<fitPars[1]<<std::endl;
	fit_Can->cd(5);

	fcalib5->Draw("AP*");
	fcalib5->Fit("calib_fit","q");
	fcalib5->SetTitle("calib14");
	calib_fit->GetParameters(fitPars);
	std::cout<<"--->bias = "<<fitPars[0]<<", slop = "<<fitPars[1]<<std::endl;
	fit_Can->cd(6);

	fcalib6->Draw("AP*");
	fcalib6->Fit("calib_fit","q");
	fcalib6->SetTitle("calib15");
	calib_fit->GetParameters(fitPars);
	std::cout<<"--->bias = "<<fitPars[0]<<", slop = "<<fitPars[1]<<std::endl;
	fit_Can->Print("./figs/linear_fit.png");
}

//...oooOO0OOooo......oooOO0OOooo......oooOO0OOooo......oooOO0OOooo...//
void offlineAna::getPID(int fch1 = 0, int fch2 = 12)
{
	char fnamex[30] = "NULL";
	char fnamey[30] = "NULL";
	setEnt();
	//pidcut();
	iptTree->SetBranchStatus("Eraw",1);  // activate branchname
	iptTree->SetBranchStatus("Ecal",1);  // activate branchname

	//*** data reading ***//
	Long64_t nentries = iptTree->GetEntries();
	//Long64_t nentries = iptTree->GetEntriesFast();
	std::cout<<"===================================="<<std::endl;
	std::cout<<"--->Total event number: "<<nentries<<std::endl;
	TH2F *fPID = new TH2F("fPID","fPID",2200,0,1100,2200,0,1100);
	fPID->SetTitle("PID");
	fPID->SetName("PID");
	for(Long64_t ii=0;ii<nentries;ii++){
		iptTree->GetEntry(ii);
		if( hit != 2 )continue;
		double e1=0;
		double e2=0;
		//double eQuito=0;
		//double eHava=0;
		for(UInt_t jhit = 0;jhit<hit;jhit++){
			//*** all to all ***//
			/*
			if( febCh[jhit] >=0 && febCh[jhit]<=8  )
			{
				e1 = Ecal[jhit];//Eris
				//e1 = Eraw[jhit];//Eris
				strcpy(fnamex,"Eris");
			}
			if( febCh[jhit] >=8 && febCh[jhit] <=15 )
			{
				e2 = Ecal[jhit];//Sparrow
				//e2 = Eraw[jhit];//Sparrow
				strcpy(fnamey,"Sparrow");
			}
			*/
			//*** single to single ***//
			if( febCh[jhit] == fch1  )
			{
				//e1 = Ecal[jhit];//Eris
				e1 = Eraw[jhit];//Eris
				getMapping(febCh[jhit],fnamex);
			}
			if( febCh[jhit] == fch2 )
			{
			//	e2 = Ecal[jhit];//Sparrow
				e2 = Eraw[jhit];//Sparrow
				getMapping(febCh[jhit],fnamey);
			}
		}
		fPID->Fill(e1,e2);
		//if(cutg->IsInside(e1,e2))fPID->Fill(e1,e2);
	}
	fPID->GetXaxis()->SetTitle(fnamex);
	fPID->GetYaxis()->SetTitle(fnamey);
	fPID->GetYaxis()->SetTitleOffset(1.2);
	TCanvas *fcan = new TCanvas("PID","PID",0,0,900,700);
	//TCanvas *fcan = new TCanvas("PID","PID",0,0,1100,900);
	//fcan->Divide(1,2);
	//fcan->cd(1);
	gPad->SetLogz();
	fPID->Draw("colz");
	return;
	//*** 2 layer calibration ***//
	TH2D *calib = (TH2D*)fPID->Clone("calib");
	fcan->cd(2);
	calib->Draw("colz");
	TF1 *flin = new TF1("flin","pol1",1000,5000);
	flin->SetParameter(1,1);

	calib->Fit("flin","rob=0.5");
	flin->Draw("same");
	return;
	TH1F *py = (TH1F*)fPID->ProjectionY("py",1,1000);
	py->Draw();
	py->GetXaxis()->SetRangeUser(10,1000);
	py->GetXaxis()->SetTitleOffset(1.0);
	//TH1F *ftest=0;
	//TH1F *ftest = new TH1F("ftest","ftest",1000,0,1000);
	double famp[1]  = {2200};
	double fmean[1] = {5000};
	double fsig[1]  = {40};
	singleFit(py,famp,fmean,fsig);
	//std::cout<<"par0 = "<<famp[0]<<"; par1 = "<<fmean[0]<<"; par2 = "<<fsig[0]<<std::endl;
	std::cout<<"--->Fitting results:"<<std::endl;
	std::cout<<"mean = "<<fmean[0]<<"; sigma = "<<fsig[0]<<std::endl;
	std::cout<<"dE/E(FWHM) = "<<fsig[0]*2.355/fmean[0]*100<<"\%"<<std::endl;
}


//...oooOO0OOooo......oooOO0OOooo......oooOO0OOooo......oooOO0OOooo...//
//theta_phi plot
void offlineAna::hitPattern_2()
{
	Long64_t nentries = iptTree->GetEntries();
	std::cout<<"--->Total event number: "<<nentries<<std::endl;

	TH2F *h_thetaPhi = new TH2F("theta_phi","theta_phi",180,-180,180,180,0,180);//deg
	h_thetaPhi->GetXaxis()->SetTitle("Phi[deg]");
	h_thetaPhi->GetYaxis()->SetTitle("Theta[deg]");
	TCanvas *c_tracking = new TCanvas("tracking","tracking",10,10,800,400);
	c_tracking->cd();
	//*** defination ***//
	//*** ---***--- *** //
	//*** |0|***|1| *** //
	//*** ---***--- *** //
	//*** |3|***|2| *** //
	//*** ---***--- *** //
	//**X--->********** //
	//******^********** //
	//**Y**/ \********* //
	//******|********** //
	//0,(-2.5,2.5);1,(2.5,2.5)//
	//3,(-2.5,-2.5);2,(2.5,-2.5) //
	//eris, z=-5; sparrow, z=+5 //mm
	//***************** //
	for (Long64_t jentry=0; jentry<1000;jentry++)
		//for (Long64_t jentry=0; jentry<nentries;jentry++)
	{
		iptTree->GetEntry(jentry);
		TVector3 v3;//
		std::vector<double>ftheta;
		std::vector<double>fphi;
		ftheta.clear();
		fphi.clear();
		//TVector3 v_sparrow;//z=+5mm
		//if( febCh[0] == 0 && hit == 2 ){
		for( UInt_t hh = 0; hh < hit; hh++ )
		{
			v3.SetXYZ(0,0,0);
			switch(febCh[hh]) {//compatible with ch0-7
				case 0:
					v3.SetXYZ(gRandom->Uniform(-4.5,-0.5),gRandom->Uniform(0.5,4.5),-5);
					break;
				case 1:
					v3.SetXYZ(gRandom->Uniform(0.5,4.5),gRandom->Uniform(0.5,4.5),-5);
					break;
				case 2:
					v3.SetXYZ(gRandom->Uniform(0.5,4.5),gRandom->Uniform(-4.5,-0.5),-5);
					break;
				case 3:
					v3.SetXYZ(gRandom->Uniform(-4.5,-0.5),gRandom->Uniform(-4.5,-0.5),-5);
					break;
				case 4:
					v3.SetXYZ(gRandom->Uniform(-4.5,-0.5),gRandom->Uniform(0.5,4.5),5);
					break;
				case 5:
					v3.SetXYZ(gRandom->Uniform(0.5,4.5),gRandom->Uniform(0.5,4.5),5);
					break;
				case 6:
					v3.SetXYZ(gRandom->Uniform(0.5,4.5),gRandom->Uniform(-4.5,-0.5),5);
					break;
				case 7:
					v3.SetXYZ(gRandom->Uniform(-4.5,-0.5),gRandom->Uniform(-4.5,-0.5),5);
					break;
				default:
					std::cout<<"--->Error501. Incorrect channel ID. It's only compatible with ch0-7."<<std::endl;
					return;
			}
			ftheta.push_back(v3.Theta()/TMath::Pi()*180.);
			fphi.push_back(v3.Phi()/TMath::Pi()*180.);
			h_thetaPhi->Fill(fphi.back(),ftheta.back());
		}
		//}
		if(ftheta.size()!=0 &&  ftheta.size() != hit)
		{
			std::cout<<"Error502. ftheta.size != hit"<<std::endl;
			std::cout<<"ftheta.size = "<<ftheta.size()<<std::endl;
			std::cout<<"hit = "<<hit<<std::endl;
			return;
		}
		if(jentry%100==0){
			h_thetaPhi->Draw("colz");
			gPad->Modified();
			gPad->Update();
		}

		std::cout<<"--->Running...: "<<jentry<<"/"<<nentries;
		//if(jentry%100==0)std::cout<<"--->Running...: "<<jentry<<"/"<<nentries;
		fflush(stdout);
		printf("\r");
	}//end loop
	/* 
	   c_hit_pattern->cd(1);
	   h_eris->Draw();
	   gPad->Modified();
	   gPad->Update();

	   c_hit_pattern->cd(2);
	   h_sparrow->Draw();
	   gPad->Modified();
	   gPad->Update();
	   */
	std::cout<<"===================================="<<std::endl;
	std::cout<<"The end."<<std::endl;
	return;
}

//...oooOO0OOooo......oooOO0OOooo......oooOO0OOooo......oooOO0OOooo...//
offlineAna::offlineAna()
{
    //setFileName(sIptFileName);
    fiptFile = new TFile(getFileName(),"READ");

   /* 
       fiptChain = new TChain("tree");
       fiptChain->Add("../anaroot/run_0077_0001_ana.root");
       fiptChain->Add("../anaroot/run_0077_0002_ana.root");
       fiptChain->Add("../anaroot/run_0077_0003_ana.root");
       fiptChain->Add("../anaroot/run_0077_0004_ana.root");
       fiptChain->Add("../anaroot/run_0077_0005_ana.root");
       fiptChain->Add("../anaroot/run_0079_0001_ana.root");
       fiptChain->Add("../anaroot/run_0079_0002_ana.root");
       fiptChain->Add("../anaroot/run_0079_0003_ana.root");
       fiptChain->Add("../anaroot/run_0080_0001_ana.root");
       fiptChain->Add("../anaroot/run_0080_0002_ana.root");
     */  
    if( !fiptFile->IsOpen() ){
        std::cout<<"--->Error401.Can not open root file "<<getFileName()<<std::endl;
        return;
    }
    fiptFile->GetObject("tree",iptTree);
    //iptTree = fiptChain;
    InitRoot(iptTree);

}
//...oooOO0OOooo......oooOO0OOooo......oooOO0OOooo......oooOO0OOooo...//
offlineAna::~offlineAna()
{
    std::cout<<"--->The end."<<std::endl;
}

//...oooOO0OOooo......oooOO0OOooo......oooOO0OOooo......oooOO0OOooo...//
void offlineAna::loadPosition(){
    loadPositionInfo();
}

//...oooOO0OOooo......oooOO0OOooo......oooOO0OOooo......oooOO0OOooo...//
void offlineAna::singleFit(TH1F *fhh, double *famp, double *fmean, double *fsigma){
    if(!fhh || fhh->GetEntries()==0 ){
        std::cout<<"Error402!!! Empty histo. Please check."<<std::endl;
        return;
    }
    TF1 *fgaus = new TF1("fgaus","pol0(0)+gaus(1)",0,10000);
    fgaus->SetParameter(1,famp[0]);
    fgaus->SetParameter(2,fmean[0]);
    fgaus->SetParameter(3,fsigma[0]);
    fgaus->SetParLimits(1,famp[0]-famp[0]/10.,famp[0]+famp[0]/10.);
    fgaus->SetParLimits(2,fmean[0]-100.,fmean[0]+100.);
    fgaus->SetParLimits(3,fsigma[0]-10.,fsigma[0]+10.);
    fhh->Fit("fgaus","R");
    double fpar[6];
    fgaus->GetParameters(fpar);
    famp[0]   = fpar[1];
    fmean[0]  = fpar[2];
    fsigma[0] = fpar[3];

}

//...oooOO0OOooo......oooOO0OOooo......oooOO0OOooo......oooOO0OOooo...//
void offlineAna::filltrack(double x1,double y1,double z1,double x2, double y2,double z2, double pz,TH2D *offline_hx,TH2D *offline_hy,TH2D *offline_hpz)
{
    if(SINGLE_MODE){
        offline_hx->Reset();
        offline_hy->Reset();
        offline_hpz->Reset();
    }
    double px,py;
    px=x1+(x2-x1)/(z2-z1)*(pz-z1);
    py=y1+(y2-y1)/(z2-z1)*(pz-z1);

    offline_hpz->Fill(px,py);

    int nbinxx=offline_hx->GetNbinsX();
    //std::cout<<"offline_hx_NBINS = "<<nbinxx<<std::endl;
    double zmin=offline_hx->GetXaxis()->GetXmin();
    double zmax=offline_hx->GetXaxis()->GetXmax();
    for(int ii=0;ii<nbinxx;ii++)
    {
        double nowz=zmin+(zmax-zmin)*ii/(nbinxx*1.0);
        double nowf=x1+(x2-x1)/(z2-z1)*(nowz-z1);
        //std::cout<<"nowz = "<<nowz<<std::endl;
        //std::cout<<"nowf = "<<nowf<<std::endl;
        offline_hx->Fill(nowz,nowf);
    }

    nbinxx=offline_hy->GetNbinsX();
    //std::cout<<"offline_hy_NBINS = "<<nbinxx<<std::endl;
    zmin=offline_hy->GetXaxis()->GetXmin();
    zmax=offline_hy->GetXaxis()->GetXmax();
    for(int ii=0;ii<nbinxx;ii++)
    {
        double nowz=zmin+(zmax-zmin)*ii/(nbinxx*1.0);
        double nowf=y1+(y2-y1)/(z2-z1)*(nowz-z1);
        offline_hy->Fill(nowz,nowf);
    }
    return;
}

//...oooOO0OOooo......oooOO0OOooo......oooOO0OOooo......oooOO0OOooo...//
void offlineAna::InitRoot(TTree *ftree){

    // Set branch addresses and branch pointers
    if (!ftree) {
        std::cout<<"--->Error403!!! Failed to init root file. Couln't find tree."<<std::endl;
        return;
    }
    ftree->SetBranchAddress("TRIGGER_c4", &TRIGGER_c4, &b_TRIGGER_c4);
    ftree->SetBranchAddress("EVENTNO_c4", &EVENTNO_c4, &b_EVEVTNO_c4);
    ftree->SetBranchAddress("hit", &hit, &b_hit);
    ftree->SetBranchAddress("febCh", febCh, &b_febCh);
    ftree->SetBranchAddress("entry", &entry, &b_entry);
    ftree->SetBranchAddress("entry_empty", entry_empty, &b_entry_empty);
    ftree->SetBranchAddress("ts", ts, &b_ts);
    ftree->SetBranchAddress("tsS", tsS, &b_tsS);
    ftree->SetBranchAddress("Eraw", Eraw, &b_Eraw);
    ftree->SetBranchAddress("Ecal", Ecal, &b_Ecal);
    ftree->SetBranchAddress("T_cfd", T_cfd, &b_T_cfd);
    ftree->SetBranchAddress("traceLength", traceLength, &b_traceLength);
    ftree->SetBranchAddress("tracesX", tracesX, &b_tracesX);
    ftree->SetBranchAddress("tracesY", tracesY, &b_tracesY);

    ftree->SetBranchStatus("*",0);  // disable all branches
    ftree->SetBranchStatus("hit",1);  // activate branchname
    ftree->SetBranchStatus("febCh",1);  // activate branchname
    std::cout<<"--->init complete."<<std::endl;
    std::cout<<"--->read data from "<<getFileName()<<std::endl;

}

//...oooOO0OOooo......oooOO0OOooo......oooOO0OOooo......oooOO0OOooo...//
//define histograms, canvas, ...//
void offlineAna::setDisplay(){

    p_eris1=new TLine(positionZ_eris,coor_LISA_y1[3],positionZ_eris,coor_LISA_y2[3]);
    p_eris2=new TLine(positionZ_eris,coor_LISA_y1[0],positionZ_eris,coor_LISA_y2[0]);
    p_eris1->SetLineWidth(5);
    p_eris1->SetLineColor(kRed);
    p_eris2->SetLineWidth(5);
    p_eris2->SetLineColor(kRed);

    p_sparrow1=new TLine(positionZ_sparrow,coor_LISA_y1[3],positionZ_sparrow,coor_LISA_y2[3]);
    p_sparrow2=new TLine(positionZ_sparrow,coor_LISA_y1[0],positionZ_sparrow,coor_LISA_y2[0]);
    p_sparrow1->SetLineWidth(5);
    p_sparrow1->SetLineColor(kRed);
    p_sparrow2->SetLineWidth(5);
    p_sparrow2->SetLineColor(kRed);

    pzline=new TLine(positionZ_degrader,-1,positionZ_degrader,1);//z==0;degrader
    pzline->SetLineWidth(5);
    pzline->SetLineColor(kBlack);//6,purple

    offline_hx=new TH2D("offline_hx","Projection of X-Z (TOP VIEW)",20,-10,10,100,-10,10);
    offline_hy=new TH2D("offline_hy","Projection of Y-Z (SIDE VIEW)",20,-10,10,100,-10,10);
    offline_hpz=new TH2D("offline_hpz","Distribution at Z = 0mm",36,-6,6,36,-6,6);
    offline_hx->GetXaxis()->SetTitle("PositionZ[mm]");
    offline_hx->GetYaxis()->SetTitle("PositionX[mm]");
    offline_hy->GetXaxis()->SetTitle("PositionZ[mm]");
    offline_hy->GetYaxis()->SetTitle("PositionY[mm]");
    offline_hpz->GetXaxis()->SetTitle("PositionX[mm]");
    offline_hpz->GetYaxis()->SetTitle("PositionY[mm]");
    offline_hpz->GetYaxis()->SetTitleOffset(.8);
    offline_hx->SetStats(kFALSE);
    offline_hy->SetStats(kFALSE);
    offline_hpz->SetStats(kFALSE);

    offline_c1=new TCanvas("offline_c1","Projection",0,0,900,800);
    offline_c1->Divide(1,2);
    offline_c1->Draw();
    offline_c1->cd(1);
    //offline_c1->cd(1)->SetGrid();
    offline_hx->Draw();
    p_eris1->Draw();
    p_eris2->Draw();
    p_sparrow1->Draw();
    p_sparrow2->Draw();
    pzline->Draw();
    offline_c1->cd(2);
    //offline_c1->cd(2)->SetGrid();
    offline_hy->Draw();
    p_eris1->Draw();
    p_eris2->Draw();
    p_sparrow1->Draw();
    p_sparrow2->Draw();
    pzline->Draw();
    offline_c1->Modified();
    offline_c1->Update();

    offline_c2=new TCanvas("offline_c2","Z=0",910,0,800,800);
    offline_c2->cd();
    //offline_c2->cd()->SetGrid();
    offline_hpz->Draw();
    offline_c2->Modified();
    offline_c2->Update();

    //*** hit pattern ***//
    h_eris = new TH2F("Eris","Eris",30,-7,7,30,-7,7);//mm
    h_eris->GetXaxis()->SetTitle("PositionX[mm]");
    h_eris->GetYaxis()->SetTitle("PositionY[mm]");
    h_sparrow = new TH2F("Sparrow","Sparrow",30,-7,7,30,-7,7);
    h_sparrow->GetXaxis()->SetTitle("PositionX[mm]");
    h_sparrow->GetYaxis()->SetTitle("PositionY[mm]");
    h_eris->SetStats(kFALSE);
    h_sparrow->SetStats(kFALSE);
    c_hit_pattern = new TCanvas("hit_pattern","hit_pattern",0,850,900,400);
    c_hit_pattern->Divide(2,1);
    for(int ii=0;ii<4;ii++){//2x2

        //*** eris ***//
        hit_eris.fbox.push_back(new TBox(coor_LISA_x1[ii],coor_LISA_y1[ii],coor_LISA_x2[ii],coor_LISA_y2[ii]));
        hit_eris.fbox[ii]->SetLineWidth(5);
        hit_eris.fbox[ii]->SetLineColor(kRed);
        hit_eris.fbox[ii]->SetFillStyle(0);
        //*** sparrow ***//
        hit_sparrow.fbox.push_back(new TBox(coor_LISA_x1[ii],coor_LISA_y1[ii],coor_LISA_x2[ii],coor_LISA_y2[ii]));
        hit_sparrow.fbox[ii]->SetLineWidth(5);
        hit_sparrow.fbox[ii]->SetLineColor(6);//6, purple
        hit_sparrow.fbox[ii]->SetFillStyle(0);
    }

}

//...oooOO0OOooo......oooOO0OOooo......oooOO0OOooo......oooOO0OOooo...//
//fhit, number of fired detectors in each event//
void offlineAna::offlineLoop(UInt_t fhit, Long64_t jentry, Long64_t nentries){

    if( jentry%SPEED !=0 )return;
    iptTree->GetEntry(jentry);
    //*** protection ***//
    if(fhit>8){
        std::cout<<"Error404!!!Illegal hit number: "<<fhit<<std::endl;
        return;
    }
    UInt_t fpos[1];
    if(IF_DEBUG)std::cout<<"in test1"<<std::endl;
    double x1[1] ={ 0};
    double y1[1] ={ 0};
    double x2[1] ={ 0};
    double y2[1] ={ 0};
    if(IF_DEBUG)std::cout<<"in test2"<<std::endl;
    for( UInt_t hh = 0; hh < fhit; hh++ )
    {
        if(IF_DEBUG)std::cout<<"in test3"<<std::endl;
        if(IF_DEBUG)std::cout<<"hit = "<<fhit<<std::endl;
        getPosition(febCh[hh],fpos);
        if(USE_CENTER){
            std::cout<<"here.using center position."<<std::endl;
            if(febCh[hh]<8 && febCh[hh]>0){
                x1[0] = coor_LISA_center_x[fpos[0]];
                y1[0] = coor_LISA_center_y[fpos[0]];
                h_eris->Fill(x1[0],y1[0]);
                std::cout<<"febch = "<<febCh[hh]<<std::endl;
            }
            if(IF_DEBUG)std::cout<<"in test4"<<std::endl;
            if(febCh[hh]>8 && febCh[hh]<16){
                x2[0] = coor_LISA_center_x[fpos[0]];
                y2[0] = coor_LISA_center_y[fpos[0]];
                h_sparrow->Fill(x2[0],y2[0]);
                std::cout<<"febch = "<<febCh[hh]<<std::endl;
            }
        }else if(USE_FAKE){
            if(IF_DEBUG)std::cout<<"in test5"<<std::endl;
            if(febCh[hh]<8){
                x1[0] = gRandom->Uniform(coor_LISA_x1[fpos[0]],coor_LISA_x2[fpos[0]]);
                y1[0] = gRandom->Uniform(coor_LISA_y1[fpos[0]],coor_LISA_y2[fpos[0]]);
                h_eris->Fill(x1[0],y1[0]);
            }
            if(febCh[hh]>8){
                x2[0] = gRandom->Uniform(coor_LISA_x1[fpos[0]],coor_LISA_x2[fpos[0]]);
                y2[0] = gRandom->Uniform(coor_LISA_y1[fpos[0]],coor_LISA_y2[fpos[0]]);
                h_sparrow->Fill(x2[0],y2[0]);
            }
        }else{
            if(febCh[hh]<8){
                x1[0] = gRandom->Uniform(coor_LISA_x1[fpos[0]],coor_LISA_x2[fpos[0]]);
                y1[0] = gRandom->Uniform(coor_LISA_y1[fpos[0]],coor_LISA_y2[fpos[0]]);
                h_eris->Fill(x1[0],y1[0]);
            }
            if(febCh[hh]>8){
                x2[0] = gRandom->Uniform(coor_LISA_x1[fpos[0]],coor_LISA_x2[fpos[0]]);
                y2[0] = gRandom->Uniform(coor_LISA_y1[fpos[0]],coor_LISA_y2[fpos[0]]);
                h_sparrow->Fill(x2[0],y2[0]);
            }
        }
    }//end of loop for hit
    if(IF_DEBUG)std::cout<<"in test6"<<std::endl;
    if( fhit == 2 && febCh[0] < 8 && febCh[1] > 8 )filltrack(x1[0],y1[0],positionZ_eris,x2[0],y2[0],positionZ_sparrow,positionZ_degrader,offline_hx,offline_hy,offline_hpz);
    //*** debug-begin ***//
    //int nbinsx = offline_hx->GetNbinsX();
    //std::cout<<"nbinsx after filling the histos = "<<nbinsx<<std::endl;
    //for(int ii=1;ii<nbinsx;ii++)
    //{
    //	double bcenterx = offline_hx->GetXaxis()->GetBinCenter(ii);
    //	int ibin = offline_hx->FindBin(bcenterx);
    //	std::cout<<"bin centerx = "<<bcenterx<<std::endl;
    //	std::cout<<"ibin = "<<ibin<<std::endl;
    //}
    //*** debug-end ***//

    //*** display ***//
    //if(jentry%SPEED==0)
    //if(jentry == 20)
    //{
    //*** tracking ***//
    offline_c1->cd(1);
    //if(if_rebin_x)offline_hx->RebinX(4);if_rebin_x=0;
    offline_hx->Draw("colz");
    p_eris1->Draw();
    p_eris2->Draw();
    p_sparrow1->Draw();
    p_sparrow2->Draw();
    pzline->Draw();
    offline_c1->cd(2);
    //if(if_rebin_y)offline_hy->RebinX(4);if_rebin_y=0;
    offline_hy->Draw("colz");
    p_eris1->Draw();
    p_eris2->Draw();
    p_sparrow1->Draw();
    p_sparrow2->Draw();
    pzline->Draw();
    offline_c1->Update();

    offline_c2->cd();
    offline_hpz->Draw("colz");
    offline_c2->Update();
    //*** hit pattern ***//
    c_hit_pattern->cd(1);
    h_eris->Draw("colz");
    for(UInt_t ii=0;ii<hit_eris.fbox.size();ii++)hit_eris.fbox[ii]->Draw();
    gPad->Modified();
    gPad->Update();

    c_hit_pattern->cd(2);
    h_sparrow->Draw("colz");
    for(UInt_t ii=0;ii<hit_sparrow.fbox.size();ii++)hit_sparrow.fbox[ii]->Draw();
    gPad->Modified();
    gPad->Update();
    //}//end if jentry
    //if(jentry%SPEED==0)std::cout<<"--->Running...: "<<jentry<<"/"<<nentries;
    std::cout<<"--->Running...: "<<jentry<<"/"<<nentries;
    fflush(stdout);
    printf("\r");
}

//...oooOO0OOooo......oooOO0OOooo......oooOO0OOooo......oooOO0OOooo...//
void offlineAna::setEnt(){
    gROOT->SetStyle("Modern");
    //gStyle->SetHistFillColor(7);
    //gStyle->SetHistFillStyle(3002);
    //gStyle->SetHistLineColor(kBlue);
    //*** FAIR style ***//
    gStyle->SetHistFillColor(796);
    gStyle->SetHistFillStyle(1001);
    gStyle->SetHistLineColor(kBlack);
    gStyle->SetFuncColor(kRed);
    gStyle->SetFrameLineWidth(2);
    gStyle->SetCanvasColor(0);
    gStyle->SetTitleFillColor(0);
    gStyle->SetTitleStyle(0);
    gStyle->SetStatColor(0);
    gStyle->SetStatStyle(0);
    gStyle->SetStatX(0.9);  
    gStyle->SetStatY(0.9);  
    gStyle->SetPalette(1);
    //gStyle->SetOptLogz(1);
    //  gStyle->SetOptTitle(0);
    //gStyle->SetOptFit(1);
    gStyle->SetOptStat(1111111);
    //gStyle->SetOptStat(0);
    //gStyle->SetPadBorderMode(1);
    //  gStyle->SetOptDate(1);
    gStyle->SetLabelFont(132,"XYZ");
    gStyle->SetLabelSize(0.05,"X");
    gStyle->SetLabelSize(0.05,"Y");
    gStyle->SetLabelOffset(0.004);
    //gStyle->SetTitleOffset(0.1);
    gStyle->SetTitleFont(132,"XYZ");
    gStyle->SetTitleSize(0.045,"X");
    gStyle->SetTitleSize(0.045,"Y");
    //gStyle->SetTitleFont(132,"");
    gStyle->SetTextFont(132);
    gStyle->SetStatFont(132);
}

void offlineAna::convert(float fk1, float fb1, float fk2, float fb2){

    std::cout<<"New Slop = "<<fk1*fk2<<std::endl;
    std::cout<<"New bias = "<<fk2*fb1+fb2<<std::endl;

}

