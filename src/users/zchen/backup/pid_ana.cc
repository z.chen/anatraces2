#include "Mo100.hh"
#include "Nb.hh"

void pid_ana()
{
    TChain *fch = new TChain("tree");
    fch->Add("run_0077_0001_ana.root");
    fch->Add("run_0077_0002_ana.root");
    fch->Add("run_0077_0003_ana.root");
    fch->Add("run_0077_0004_ana.root");
    fch->Add("run_0077_0005_ana.root");
    fch->Add("run_0079_0001_ana.root");
    fch->Add("run_0079_0002_ana.root");
    fch->Add("run_0079_0003_ana.root");
    fch->Add("run_0080_0001_ana.root");
    fch->Add("run_0080_0002_ana.root");

    pidcut();
    getNb();


    fch->Draw("Eraw[0]:Eraw[1]>>caravshava(400,200,360,400,200,360)","febCh[0]==2&&febCh[1]==10&&hit==2","colz");
    fch->Draw("Eraw[0]:Eraw[1]>>caravshava_cut(100,200,360,100,200,360)","febCh[0]==2&&febCh[1]==10&&hit==2&&Mo100","colz");
    fch->Draw("Eraw[0]:Eraw[1]>>caravshava_cut2(100,200,360,100,200,360)","febCh[0]==2&&febCh[1]==10&&hit==2&&Nb_cut","colz");

    fch->Draw("Eraw[0]:Eraw[1]>>quitovsnovi(400,200,360,400,200,360)","febCh[0]==1&&febCh[1]==9&&hit==2","colz");


    TH2F *hquitovsnovi = (TH2F*)gROOT->FindObject("quitovsnovi");
    TH2F *hcaravshava = (TH2F*)gROOT->FindObject("caravshava");
    TH2F *hcaravshava_cut = (TH2F*)gROOT->FindObject("caravshava_cut");
    TH2F *hcaravshava_cut2 = (TH2F*)gROOT->FindObject("caravshava_cut2");

    gStyle->SetOptStat(0);
    auto *mycan = new TCanvas("mycan","mycan",10,10,900,900);
    mycan->Divide(2,2);
    mycan->cd(1);
    gPad->SetLogz();
    hquitovsnovi->Draw("colz");
    hquitovsnovi->GetXaxis()->SetTitle("Quito");
    hquitovsnovi->GetYaxis()->SetTitle("Novi Sad");
    cutg->Draw("same");
    cutg_Nb->Draw("same");
    mycan->cd(2);
    gPad->SetLogz();
    hcaravshava->Draw("colz");
    hcaravshava->GetXaxis()->SetTitle("Caracas");
    hcaravshava->GetYaxis()->SetTitle("Havana");
    mycan->cd(3);
    hcaravshava_cut->Draw("colz");
    hcaravshava_cut->GetXaxis()->SetTitle("Caracas");
    hcaravshava_cut->GetYaxis()->SetTitle("Havana");
    mycan->cd(4);
    hcaravshava_cut2->Draw("colz");
    hcaravshava_cut2->GetXaxis()->SetTitle("Caracas");
    hcaravshava_cut2->GetYaxis()->SetTitle("Havana");


}

