# Define the old and new names
NAME_OLD=calcCFDTraceLength
NAME_NEW=getCFDTraceLength

# Replace NAME_OLD with NAME_NEW in all matching files
for file in $(grep -rl "$NAME_OLD" inc/ src/); do
    sed -i "s/$NAME_OLD/$NAME_NEW/g" "$file"
done

